CREATE TABLE USER_SESSION_CAF_TBL
(ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1),
SESSION_ID VARCHAR(225) NOT NULL,
CAF_TBL_ID BIGINT NOT NULL,
MOBILE_NUMBER VARCHAR(15),
USER_ID BIGINT,
STATUS_ID BIGINT,
CREATED_DATE TIMESTAMP,
PRIMARY KEY (ID),
UNIQUE (CAF_TBL_ID));