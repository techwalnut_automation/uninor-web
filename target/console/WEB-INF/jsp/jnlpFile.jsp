<?xml version="1.0" encoding="UTF-8"?>
<jnlp spec="6.0+" codebase="${url}resources/jars/" 
	href="${hostUrl}getJnlp?key=${regKey}&uvId=${userId}">
    <information>
        <title>Scan Client</title>
        <vendor>Scan Client Demo</vendor>
         <offline-allowed/>        

    </information>
   
    <security>
		<all-permissions/>
   </security>

  <update check="always" policy="always"/>

   <resources>
       <!-- Application Resources -->
               <j2se version="1.6+" initial-heap-size="512m" max-heap-size="1024m"/>
       <jar href="scanclient-signed.jar" main="false" download="eager"/>
      
       
   </resources>
    <application-desc
         name="ScanClient"
         main-class="com.vistasoftwares.speedoc.scanning.client.views.ScanningView">
         <argument>${userId}</argument>  
		 <argument>${regKey}</argument>
		 <argument>${hostUrl}</argument>  
    </application-desc>
    
</jnlp>        
