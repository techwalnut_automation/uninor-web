<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
<meta http-equiv="Content-path" content="text/html; charset=ISO-8859-1">
<title>Audit</title>
<script type="text/javascript" src="<c:url value='/resources/scripts/slc/moment-with-langs.js'/>"></script>
<script src="<c:url value="/resources/scripts/audit/auditPipe.js?v=1.2" />" type="text/javascript"></script>
<%-- <script src="<c:url value="/resources/scripts/horizontal/cafElementListHorizontal.js?v=1.2" />" type="text/javascript"></script>	 --%>
<script type="text/javascript" src="<c:url value='/resources/scripts/audit/audit.js?v=1.2'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/scripts/uninor/PageRotation.js' />" ></script>
<c:url var="imageUrl" value="/resources/images/uninor/" />


<script type="text/javascript">
	$(document).ajaxStop($.unblockUI);
	$(document).ready(function() {
		$("#cancelBtn1").button();
		$("#auditsave").button();
		
		$("#photoComplianceBtn1").button();
		$("#documentComBtn1").button();
		$("#cafComBtn1").button();
		$("#customerComBtn1").button();
		$("#caoCombtn1").button();
		$("#retailerComBtn1").button();
		$("#distributorComBtn1").button();
		$("#overwritingBtn1").button();
		$("#outstationCombtn1").button();
		$("#mnpCombtn1").button();
		$("#databaseCombtn1").button();
		$("#auditImageRotateBtn").button();
		
		$("#okRejectBtn").button();
		$("#rejectSpan").val();
		$("#usernameSpan").val();
		$("#createdDateSpan").val();
	});
	
	function bigImg(x) {
	    x.style.width = "180%";
	}
	function normalImg(x) {
	    x.style.width = "100%";
	}
</script>

<style>
#bg_image_photo {
	width: 250px;
	height: 300px;
	margin: 5px 5px 5px 5px;
}

#bg_image_signature {
	width: 350px;
	height: 150px;
	margin: 5px 5px 5px 5px;
}

#basicDetailsTbl td {
	width: 7%;
	text-align: left;
}

#basicDetailsTbl input {
	width: 100px;
	font-size: 13px;
}

#basicDetailsTbl label {
	font-size: 12px;
}

#addDetailsTbl td {
	width: 7%;
	text-align: left;
}

#addDetailsTbl input {
	font-size: 13px;
    width: 120px;
}

#addDetailsTbl label {
	font-size: 12px;
}

#passportDetailsTbl td {
	width: 7%;
	text-align: left;
}

#passportDetailsTbl input {
	width: 100px;
}

#passportDetailsTbl label {
	font-size: 12px;
}

#ui-tabs-5 #auditDiv {
	margin: 0;
	padding: 0;
	width: 100%;
}

.ui-tabs .ui-tabs-panel {
	padding: 0 !important;
}

#cafImages1 {
	overflow-y: scroll;
	overflow-x: hidden;
	float: left;
	width: 100%;
	height: 50%;
	border-bottom: 4px solid black;
}
</style>
</head>
<body>
	<div>						
			<form id="rotateCafImageAudit" name="rotateCafImageAudit" style=" margin-left: 2%;">											
				<input id="fileName3" type="hidden" name="fileName"/>
				<input type="hidden" id="lblNoOfPage3" /> 
				<label >Page :</label> <select id="pageNo3" name="pageNo"></select>								   	
			</form>																
			<button title="rotate" style=" margin-left: 10%; position:relative; bottom: 34px; float: left;" id="auditImageRotateBtn">Rotate</button>
		</div>
		<div id="" style="width: 100%; height: 100%">
			<div id="" style="position: relative; height: 100%">
				<div style="position: relative; width: 100%; margin-bottom: 5px">
					
					<div id="cafImages1"></div>
					<div id="hiddenImageDiv1" style="display: none;"></div>
					<div id="rotateAuditHiddendiv" style="display: none; height: 0px"></div>
				</div>
				<div
					style="position: relative; width: 100%; height: 50%; float: right; vertical-align: top; overflow-y: scroll; overflow-x: hidden;">
					<div id="smbox">
						<div class="smbox-cont">
							<div id="form1">
								
								<table border="0" cellspacing="0" cellpadding="0" width="100%">
									<tr>
										<td>
											<form id="auditForm" name="auditForm">
												<input type="hidden"  id="emailAddress1" name="email"/>
												<input type="hidden"  id="vasApplied1" name="vasApplied"/>
												<input type="hidden"  id="tarrifPlanApplied1" name="tarrifPlanApplied"/>
												<input type="hidden"  id="existingNumber1" name="existingNumber"/>
												<input type="hidden"  id="prevServiceProviderName1" name="prevServiceProviderName"/>
												<input type="hidden"  id="existingConnection1" name="existingConnection"/>
												<input type="hidden"  id="callerBackRingTone1" name="callerBackRingTone"/>
												<input type="hidden"  id="friendsAndFamily1" name="friendsAndFamily"/>
												
												
												<table width="100%" cellpadding="0" cellspacing="0"
													style="margin-top: 10px">		
													<tr>										
														<td><label style="font-size: 12px;">Reasons :</label><input class="focusH" id="rejectSpan" name="rejectReason" tabindex="1" readonly="readonly" style="width: 93%; background-color: #CCCCCC;  color: #FF0000;  border: 0 none; font-size: 13px;"></td>
													</tr>
													<tr>
														<td><label style="font-size: 12px;">User :</label><input class="focusH" id="usernameSpan" name="username" tabindex="2" readonly="readonly" style="background-color: #CCCCCC;  border: 0 none; font-size: 12px; width: 113px;">
														<label style="font-size: 12px;">Date :</label><input class="focusH" id="createdDateSpan" name="createdDate" tabindex="3" readonly="readonly" style="background-color: #CCCCCC;  border: 0 none; width: 113px; font-size: 12px;"></td>
													</tr>							
													<tr>
														<td>
															<table width="88%" id="basicDetailsTbl">
																<tr>
																	<td><label>Speedoc Id</label>
																	</td>
																	<td><input class="focusH" id="speedocId2"
																		name="speedocId" maxlength="9" tabindex="1"
																		style="width:140px;" readonly="readonly"></td>
																	<td><label>Mobile Number</label>
																	</td>
																	<td><input class="focusH" id="mobileNumber1"
																		name="mobileNumber" maxlength="10" tabindex="2"
																		style="width:140px;" readonly="readonly" /></td>
																	<td><label>Caf Number</label>
																	</td>
																	<td><input class="focusH" id="cafNumber1"
																		name="cafNumber" readonly="readonly" maxlength="20"
																		style="width:140px;" tabindex="4"></td>
																	<td><label>DOB</label>
																	</td>
																	<td>
																		<div>
																			<input class="focusH" id="dob1" name="dob"
																				maxlength="10" readonly="readonly"
																				style="width:140px;"
																				class="validateDateOfBirth" tabindex="8" />
																		</div>
																		<div>
																			<label>(dd-mm-yyyy)</label>
																		</div></td>
																</tr>
																<tr>
																	<td id="fn"><label>First Name</label>
																	</td>
																	<td><input tabindex="5" class="focusH"
																		id="firstName1" name="firstName" maxlength="50"
																		style="width:140px;"
																		readonly="readonly" /></td>
																	<td><label>Middle Name</label>
																	</td>
																	<td><input tabindex="6" class="focusH"
																		id="middleName1" name="middleName" readonly="readonly"
																		style="width:140px;" maxlength="50" /></td>
																	<td><label>Last Name</label>
																	</td>
																	<td><input tabindex="7" class="focusH"
																		id="lastName1" name="lastName" readonly="readonly"
																		style="width:140px;" onkeyup="fillLastName()" maxlength="50" /></td>
																	<td><label class="generalLable">Nationality</label>
																	</td>
																	<td><input class="focusH" id="nationality1"
																		style="width:140px;" name="nationality" tabindex="9" readonly="readonly">
																	</td>
																</tr>
																<tr>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
															</table></td>
													</tr>
													<tr>
														<td>
															<table width="85%" id="addDetailsTbl">
																<tr>
																	<td align="right"><label>Father's/Husband's:</label>
																	</td>
																</tr>
																<tr>
																	<td><label>First Name</label>
																	</td>
																	<td><input class="focusH" id="fatherFirstName1"
																		name="fatherFirstName" readonly="readonly"
																		tabindex="10" maxlength="50" /></td>
																	<td><label class="generalLable">Middle
																			Name</label>
																	</td>
																	<td><input class="focusH" id="fatherMiddleName1"
																		name="fatherMiddleName" readonly="readonly"
																		tabindex="11" maxlength="50" /></td>
																	<td><label>Last Name</label>
																	</td>
																	<td><input class="focusH" id="fatherLastName1"
																		name="fatherLastName" readonly="readonly"
																		tabindex="12" maxlength="50" /></td>
																	
																	<td><label class="generalLable">Landmark</label></td>
																	<td><input class="focusH"
																		id="localAddressLandmark1" name="localAddressLandmark"
																		maxlength="30" readonly="readonly" tabindex="14" /></td>
																</tr>
																<tr>
																	<td><label>Permanent Address</label>
																	</td>
																	<td colspan="3"><textarea class="focusH" 
																			style="font-size: 13px; width: 386px; height: 85px;"
																			id="permanentAddress1" name="permanentAddress"
																			readonly="readonly" tabindex="13"></textarea></td>
																	
																	<td><label>Local Address</label></td>
																	<td colspan="3"><textarea class="focusH"
																			id="localAddress1" name="localAddress" 
																			style="font-size: 13px; width: 386px; height: 85px;"
																			readonly="readonly" tabindex="13"></textarea></td>
																	
																</tr>
																<tr>
																	<td><label>State</label></td>
																	<td><input class="focusH" id="localState1"
																		name="localState" class="generalLable"
																		readonly="readonly" tabindex="15" readonly="readonly">
																	</td>
																	<td><label>City</label></td>
																	<td><input class="focusH" id="localCity1"
																		name="localCity" class="generalLable" tabindex="16"
																		readonly="readonly"></td>
																		
																	<td><label>District</label></td>
																	<td><input class="focusH" id="localDistrict1"
																		name="localDistrict" class="generalLable"
																		tabindex="17" readonly="readonly"></td>
																	
																	<td><label>Pin Code</label></td>
																	<td><input class="focusH" id="localPincode1"
																		name="localPincode" maxlength="6" readonly="readonly"
																		tabindex="18" /></td>
																</tr>
																<tr>
																	<td><label>Alternate No</label></td>
																	<td><input class="focusH"
																		id="alternateMobileNumber1"
																		name="alternateMobileNumber" maxlength="10"
																		readonly="readonly" tabindex="19" /></td>
																	<td><label class="generalLable">PAN/GIR No.</label></td>
																	<td><input class="focusH" id="panGirNumber1"
																		name="panGirNumber" readonly="readonly" tabindex="27" />
																	</td>
																</tr>
																<tr>
																	<td><label>POI</label>
																	</td>
																	<td><input class="focusH" id="poi1" name="poi"
																		style="width: 155px;"
																		readonly="readonly" tabindex="29"></td>
																	<td><label class="generalLable">POI Ref#</label>
																	</td>
																	<td><input class="focusH" id="poiref1"
																		name="poiref" maxlength="30" tabindex="30"
																		readonly="readonly" style="width: 155px;"/></td>
																	<td><label>POA</label>
																	</td>
																	<td><input class="focusH" id="poa1" name="poa"
																		style="width: 155px;"
																		tabindex="31" readonly="readonly"></td>
																	<td><label class="generalLable">POA Ref#</label>
																	</td>
																	<td><input class="focusH" id="poaref1"
																		name="poaref" maxlength="30" tabindex="32"
																		readonly="readonly" style="width: 155px;"/></td>
																</tr>
																<tr>
																	<td><label class="generalLable">POI Issue
																			Place</label>
																	</td>
																	<td><input id="poiIssuePlace1" class="focusH"
																		tabindex="33" maxlength="50" readonly="readonly"
																		style="width: 155px;" name="poiIssuePlace"></td>
																	<td><label class="generalLable">POI
																			Issuing Authority</label>
																	</td>
																	<td><input id="poiIssuingAuthority1" class="focusH"
																		tabindex="34" readonly="readonly"
																		style="width: 155px;"
																		name="poiIssuingAuthority"></td>
																	<td><label class="generalLable">POI Issue
																			Date</label>
																	</td>
																	<td><input id="poiIssueDate1" class="focusH"
																		maxlength="19" tabindex="35" readonly="readonly"
																		style="width: 155px;" 
																		name="poiIssueDate"></td>
																	<td><label class="generalLable">POA Issue
																			Place</label>
																	</td>
																	<td><input id="poaIssuePlace1" class="focusH"
																		readonly="readonly" maxlength="50"
																		style="width: 155px;"
																		name="poaIssuePlace"></td>
																</tr>
																<tr>
																	<td><label class="generalLable">POA
																			Issuing Authority</label>
																	</td>
																	<td><input id="poaIssuingAuthority1" class="focusH"
																		style="width: 155px;"
																		readonly="readonly" name="poaIssuingAuthority">
																	</td>
																	<td><label class="generalLable">POA Issue
																			Date</label>
																	</td>
																	<td><input id="poaIssueDate1" class="focusH"
																		style="width: 155px;"
																		maxlength="19" readonly="readonly" name="poaIssueDate">
																	</td>
																	<td><label>Status Of Subscriber</label>
																	</td>
																	<td><input id="statusOfSubscriber1" class="focusH"
																		style="width: 155px;"
																		tabindex="44" readonly="readonly"
																		name="statusOfSubscriber"></td>
																	<td><label>Profession of the Subscriber</label>
																	</td>
																	<td><input id="subscriberProfession1"
																		style="width: 155px;"
																		class="focusH" tabindex="45" readonly="readonly"
																		name="subscriberProfession"></td>
																</tr>
																<tr>
																	<td><label>IMSI No.</label>
																	</td>
																	<td><input id="imsiNumber1" class="focusH"
																		style="width: 155px;"
																		maxlength="10" tabindex="46" readonly="readonly"
																		name="imsiNumber"></td>
																	<td><label class="generalLable">UID No. </label>
																	</td>
																	<td><input id="uidNumber1" class="focusH"
																		style="width: 155px;"
																		tabindex="50" maxlength="50" readonly="readonly"
																		name="uidNumber"></td>
																	<td><label>MNP</label>
																	</td>
																	<td>
																		<div id="mnpCheckBox1">
																			<input type="checkbox" style="width: 20px;"
																				tabindex="51" name="mnp" id="mnp1" class="focusH"
																				readOnly="readOnly">

																		</div></td>
																</tr>
																<tr>
																	<td><input type="hidden" id="connectionType1"
																		name="connectionType"></input>
																	</td>

																	<td><input type="hidden" id="id1" name="id" /> <input
																		type="hidden" id="onhold1" name="onhold" /></td>
																	<td><input type="hidden" id="cafStatusId2"
																		name="cafStatusId" /></td>
																	<td><input type="hidden" id="images2"
																		name="images1" /></td>
																	<td><input type="hidden" id="cafType1"
																		name="cafType" />
																	</td>
																	<td><input type="hidden" id="cafFileLocation1" name="cafFileLocation" />
																	</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
																<!-- <tr id="portingReason1">
																	<td nowrap="nowrap"><label>Porting Reason:</label>
																	</td>
																</tr> -->
																<tr id="mnpManditory1" style="width:50%">
																	<td><label class="generalLable">UPC Code </label>
																	</td>
																	<td><input id="upcCode1" class="focusH"
																		style="width: 155px;"
																		tabindex="2" maxlength="10" readonly="readonly"
																		name="upcCode"></td>
																	<td><label>CAF Tracking Status</label>
																	</td>
																	<td><input class="focusH" id="cafTrackingStatus1" name="cafTrackingStatus"
																		style="width: 155px;"
																		readonly="readonly" tabindex="57"></td>
																	<td><label>RTR Code</label>
																	</td>
																	<td><input class="focusH" id="wap1" name="wap"
																		style="width: 155px;"
																		readonly="readonly" style="width: 130px;" tabindex="58"></td>
																	<td><label>RTR Name</label></td>
																	<td><input class="focusH" id="mms1" name="mms"
																		style="width: 190px;"
																		readonly="readonly" style="width: 130px;" tabindex="59"></td>
																</tr>
																<tr>
																	<td><label class="generalLable">Foreign
																			National</label>
																	</td>
																	<td><input type="checkbox" class="focusH"
																		style="width: 20px;" id="foreignNational1"
																		name="foreignNational" value="REQUIRED" tabindex="60" />
																	</td>
																	<td><label class="generalLable">Outstation</label>
																	</td>
																	<td><input type="checkbox" class="focusH"
																		style="width: 20px;" id="outstation1" name="outstation"
																		value="REQUIRED" tabindex="61" /></td>
																	<td colspan="2" nowrap="nowrap">
																		<div id="copyCommonFieldsCheckboxTr">
																			<label class="generalLable">Copy common
																				fields from above</label>
																		</div></td>
																	<td>
																		<div id="copyCommonFieldsCheckboxTr1" align="left">
																			<input type="checkbox" style="width: 20px;"
																				id="copyCommonFieldsCheckbox1" tabindex="62">
																		</div></td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																</tr>
															</table>
														</td>
													</tr>
													<tr>
														<td>
															<table width="75%">
																<!-- PASSPORT & VISA DETAILS OF FOREIGN NATIONALS -->
																<tr style="height:9px"></tr>
																<tr id="foreignNationalTr1">
																	<td><label style="font-size: 12px;"
																		class="generalLable"> <b><u>Passport & Visa Details</u> :</b></label>
																	</td>
																</tr>
															</table></td>
													</tr>
													<tr>
														<td>
															<table width="99%" id="passportDetailsTbl">
																<tr id="foreignNationalTr2">
																	<td><label class="generalLable">PASSPORT
																			No.</label>
																	</td>
																	<td><input class="focusH" id="passportNumber1"
																		class="generalLable" name="passportNumber"
																		tabindex="63" readonly="readonly" /></td>
																	<td><label class="generalLable">Type of
																			Visa</label>
																	</td>
																	<td><input class="focusH" id="typeOfVisa1"
																		name="typeOfVisa" readonly="readonly" tabindex="64" />
																	</td>
																	<td><label class="generalLable">Visa valid
																			till</label>
																	</td>
																	<td>
																		<div>
																			<input class="focusH" id="visaValidTill1"
																				name="visaValidTill" class="validateVisaValidTill"
																				readonly="readonly" maxlength="10" tabindex="65">
																		</div>
																		<div>
																			<label>(dd/mm/yy)</label>
																		</div></td>
																	<td><label class="generalLable">Visa No.</label>
																	</td>
																	<td colspan="5" align="left"><input class="focusH"
																		id="visaNumber1" name="visaNumber" readonly="readonly"
																		tabindex="66"></td>
																	<td width=100%><div>&nbsp;</div>
																	</td>
																	<td>&nbsp;</td>
																</tr>
																<!-- 			IN CASE OF OUTSTATION APPLICANT / FOREIGN NATIONALS	 -->
																<tr id="outstationTr1">
																	<td align="left" colspan="10"><label
																		style="width: 150px;" class="generalLable">
																			<b><u>Local Reference</u> :</b></label></td>
																</tr>
																<tr id="outstationTr2">
																	<td><label class="generalLable">First Name</label>
																	</td>
																	<td><input class="focusH" id="localReferenceName1"
																		name="localReferenceName" maxlength="50"
																		readonly="readonly" tabindex="67"></td>
																	<td><label class="generalLable">Address</label>
																	</td>
																	<td><input class="focusH"
																		id="localReferenceAddress1"
																		name="localReferenceAddress" maxlength="200"
																		readonly="readonly" tabindex="68"></td>
																	<td><label class="generalLable">State</label>
																	</td>
																	<td><input class="focusH" id="permanentState1"
																		class="generalLable" name="permanentState"
																		tabindex="69" readonly="readonly"></td>
																	<td><label class="generalLable">City/Town</label>
																	</td>
																	<td><input class="focusH" id="localReferenceCity1"
																		name="localReferenceCity" class="generalLable"
																		tabindex="70" readonly="readonly" /></td>
																	<td><label class="generalLable">Pin Code</label>
																	</td>
																	<td><input class="focusH"
																		id="localReferencePincode1"
																		name="localReferencePincode" maxlength="6"
																		readonly="readonly" tabindex="71" /></td>
																</tr>
																<tr id="outstationTr3">
																	<td><label class="generalLable">Local
																			contact number</label>
																	</td>
																	<td><input class="focusH"
																		id="localReferenceContactNumber1"
																		name="localReferenceContactNumber" maxlength="10"
																		tabindex="72" readonly="readonly" /></td>
																	<td><label class="generalLable">Calling
																			Party Number</label>
																	</td>
																	<td><input id="partyNumber1" class="focusH"
																		tabindex="73" maxlength="10" readonly="readonly"
																		name="partyNumber"></td>
																	<td><label class="generalLable">Activation
																			Date</label>
																	</td>
																	<td><input id="activationDateTxt1" class="focusH"
																		maxlength="19" tabindex="74" readonly="readonly"
																		name="activationDate"></td>
																</tr>
																<tr>
																	<td><label class="generalLable">Cao Name</label>
																	</td>
																	<td><input class="focusH" id="caoName1"
																		name="caoName" maxlength="6" readonly="readonly"
																		style="width: 100px;" tabindex="71" /></td>
																	<td><label class="generalLable">Cao Code</label>
																	</td>
																	<td><input class="focusH" id="caoCode1"
																		name="caoCode" maxlength="6" readonly="readonly"
																		style="width: 200px;"
																		tabindex="72" /></td>
																	<td><label class="generalLable">Cao
																			Signature</label>
																	</td>
																	<td><div id="imagediv1"></div></td>
																</tr>
															</table>
														</td>
													</tr>
<!-- 													election commission  25-05-2015-->
													<tr>
														<td>
															<table width="75%">
																<!-- Election Commission DETAILS -->
																<tr id="electionCommissionTr1">
																	<td><label style="font-size: 12px;"
																		class="generalLable"><b><u>Election Commission Details</u> :</b></label>
																	</td>
																</tr>
															</table></td>
													</tr>
													<tr>
														<td>
															<table width="68%" id="electionDetails1">
																<tr>
																	<td style="width: 10%;"><label style="font-size: 12px;">Name</label></td>
																	<td><input class="focusH" id="voterName1"
																		name="voterName" readonly="readonly" maxlength="20"
																		tabindex="74"></td>
																	<td><label style="font-size: 12px;">Father's Name</label></td>
																	<td><input class="focusH"
																			id="voterFatherName1" name="voterFatherName"
																			readonly="readonly" tabindex="75"></td>
																	<td><label style="font-size: 12px;">District</label></td>
																	<td><input class="focusH"
																			id="voterCity1" name="voterCity"
																			readonly="readonly" tabindex="76"></td>
																	<td><label style="font-size: 12px;">State</label></td>
																	<td><input class="focusH"
																			id="voterState1" name="voterState"
																			readonly="readonly" tabindex="77"></td>																																
																</tr>																													
															</table>
														</td>
													</tr>
												</table>
											</form>
										</td>
									</tr>
									<tr>
										<td align="left">
											<table border="0" width="92%" cellpadding="0" cellspacing="0">
												<tr>
													<td align="left">
														<ul>
															<li style="display: inline; float: left; padding-right: 10px;">
																<button type="button" id="auditsave" tabindex="5">Accept</button>
															</li>
														</ul></td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="photoComplianceBtn1" value="Photo_Compliance" tabindex="7">Photo Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="overwritingBtn1" tabindex="14" value="Overwriting">Overwriting</button>
															</li>
														</ul>
													</td>
													
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="cafComBtn1" tabindex="9" value="CAF_Compliance">CAF Comp</button>
															</li>
														</ul>
													</td>
													
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="caoCombtn1" tabindex="11" value="CAO_Compliance">CAO Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="mnpCombtn1" tabindex="16" value="MNP_Compliance">MNP Comp</button>
															</li>
														</ul>
													</td>
																									
												</tr>
												<tr>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="customerComBtn1" tabindex="10" value="Customer_Compliance">Customer Comp</button>
															</li>
														</ul>
													</td>	
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="distributorComBtn1" tabindex="13" value="Distributor_Compliance">Distributor Comp</button>
															</li>
														</ul>
													</td>												
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="documentComBtn1" tabindex="8" value="Document_Compliance">Document Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="outstationCombtn1" tabindex="15" value="Outstation_Compliance">Outstation Comp</button>
															</li>
														</ul>
													</td>
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="retailerComBtn1" tabindex="12" value="Retailer_Compliance">Retailer Comp</button>
															</li>
														</ul>
													</td>	
													<td align="left">
														<ul>
															<li style="display: inline; float: left;">
																<button type="button" id="databaseCombtn1" tabindex="17" value="Database_Compliance">Database Comp</button>
															</li>
														</ul>
													</td>
<!-- 													<td align="left"> -->
<!-- 														<ul> -->
<!-- 															<li style="display: inline; float: left;"> -->
<!-- 																<button type="button" id="cancelBtn1" tabindex="6">Cancel</button> -->
<!-- 															</li> -->
<!-- 														</ul> -->
<!-- 													</td> -->
												</tr>
											</table>
										</td>
									</tr>
									
									<tr>
										<td style="float: left; margin-left: 34px;">
											<div id="auditRejectOpt1" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt2" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt3" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt4" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt5" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt6" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt7" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt8" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt9" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt10" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt11" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
											<div id="auditRejectOpt12" style="width: 100%; height: auto;">
													<jsp:include page="audit-reason-page.jsp"></jsp:include>
											</div>
										</td>
									</tr>
									<tr>
										<td style="float: left; margin-left: 40px;"><button id="okRejectBtn" type="button">OK</button></td>
									</tr>
								</table>
							</div>
						</div>
						<!-- div class="smbox-bottom" ends -->
					</div>
				</div>
			</div>
		</div>
		<!--wrapper ends -->
	</div>
	<div id="int-bottom">
		<span></span>
	</div>
	<!-- End: Content Bottom -->
	<c:url var="homeLoadUrl" value="/loginController/loginSuccess" />
	<form id="loadHome" action="${homeLoadUrl}"></form>
</div>
</body>
</html>