<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>   
<script type="text/javascript">
$(document).ready(function() {
	 $('input[type="text"]').addClass('input');
	 $("#btnDownload").button();
	 
	 $("#fromDate").datepicker({
	      changeMonth: true,
	      numberOfMonths: 1,	
	      dateFormat: 'dd/mm/yy' ,    
	      onClose: function(selectedDate) {
	        $("#toDate").datepicker("option", "minDate", selectedDate);
	      }
	 });
	 
	 $("#toDate").datepicker({  
	     changeMonth: true,
	     numberOfMonths: 1,
	     dateFormat: 'dd/mm/yy', 	  
	     onClose: function(selectedDate) {
	       $("#fromDate").datepicker("option", "maxDate", selectedDate+"+1D");
	     }
	  });				
});

	function validation(){
		var fromDate = $("#fromDate").val();		
			if(fromDate == ""){
				jAlert("Enter From Date.");
				$("#fromDate").focus();
				return false;
			}
	
		var toDate = $("#toDate").val();
			if(toDate == ""){
				jAlert("Enter To Date.");
				$("#toDate").focus();
				return false;
			}
			
		var status=$("#statusId").val();
			if(status == "SELECT"){
				jAlert("Select Valid Status.");
				$("#statusId").focus();
				return false;
			}	
		return true;	
	}
			
</script>
 <div class="content">
         <div id="form">
            <c:url value="/jasper/downloadreport" var="url"></c:url>
            	<form:form id="frmSearchOption" modelAttribute="jasperReportVo" 
            			   action="${url}" method="post" onsubmit="return validation();"> 
	             	<table>
						<tr><td style="color: #000000;"><label style="font-size: 12px;">From Date :</label></td>
							<td><form:input path="fromDate" type="text" id="fromDate" name="fromDate" style="font-size: 12px;"/></td>
							<td></td>
							<td style="color: #000000;"> &nbsp;<label style="font-size: 12px;">To Date :</label></td>
							<td><form:input type="text" id="toDate" path="toDate" name="toDate" style="font-size: 12px;"/> </td>
							<td>&nbsp;&nbsp;
 								<form:select id="statusId" path ="statusId" name="statusId" style="font-size: 12px;"> 
									<option style="font-size: 12px;">SELECT</option>
									<option value="SCAN_COMPLETE" style="font-size: 12px;">SCAN_COMPLETE</option>
									<option value="AVAM_DATA" style="font-size: 12px;">AVAM_DATA</option>
									<option value="AVAM_DONE" style="font-size: 12px;">AVAM_SUCCESS</option>
									<option value="AVAM_FAILED" style="font-size: 12px;">AVAM_FAILED</option>
									<option value="DE_UPLOAD_DATA" style="font-size: 12px;">DE_UPLOAD_DATA</option>							
									<option value="FLC_READY" style="font-size: 12px;">FLC_READY</option>									
									<option value="FLC_ACCEPT" style="font-size: 12px;">FLC_ACCEPT</option>	
									<option value="FLC_REJECT" style="font-size: 12px;">FLC_REJECT</option>
									<option value="FLC_SKIP" style="font-size: 12px;">FLC_SKIP</option>
									<option value="DE_COMPLETE" style="font-size: 12px;">DE_COMPLETE</option>
									<option value="SLC_UPLOAD_DATA" style="font-size: 12px;">SLC_UPLOAD_DATA</option>
									<option value="SCRUTINY_READY" style="font-size: 12px;">SCRUTINY_READY</option>
									<option value="SCRUTINY_COMPLETE" style="font-size: 12px;">SCRUTINY_COMPLETE</option>
									<option value="SCRUTINY_REJECT" style="font-size: 12px;">SCRUTINY_REJECT</option>
									<option value="AUDIT_READY" style="font-size: 12px;">AUDIT_READY</option>
									<option value="AUDIT_DONE" style="font-size: 12px;">AUDIT_DONE</option>
									<option value="AUDIT_REJECT" style="font-size: 12px;">AUDIT_REJECT</option>
									<option value="INDEXING_DATA" style="font-size: 12px;">INDEXING_DATA</option>									
									<option value="MASTER_MIS_REPORT" style="font-size: 12px;">MASTER_MIS_REPORT</option>	
									<option value="FTP_PROCESS_REPORT" style="font-size: 12px;">FTP_PROCESS_REPORT</option>																	
								</form:select> 						
							</td>
							<td colspan="3" style="padding-left: 15px">								
								<input type="submit" id="btnDownload" value="Download"/>
							</td>						
							<td>						   
							</td>
						</tr>
					</table>
			</form:form>	
		</div>
</div>
