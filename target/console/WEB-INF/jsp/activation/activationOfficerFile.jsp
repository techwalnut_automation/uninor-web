<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:url var="url" value="/activationController/saveform" />
<script type="text/javascript">
	$(document).ready(function(){
		$("#uploadActBtn").button();
		$("#resetBtn").button();	
		
		$.get("<c:url value='/activationController/getactivationgrid' />",function(data){
			 $('#result').html(data);   
		});
		
		$("#imageName").change(function(){
				jQuery.ajaxSetup({async:false});
				var url = "<c:url value='/activationController/imageValidation' />" ;
				var fd = new FormData(document.getElementById("activationForm"));	
					$.ajax({
			 	    	url: url,
			 	    	data: fd,
			 	   	 	dataType: 'text',
			 	    	processData: false,
			 	    	contentType: false,
			 	    	type: 'POST',
			 	    	success: function(data){
			 	    		if(data == "SIZE_ERROR"){
					 	    	$("#imgerror").css("color","red");
					 	    	$("#imgerror").html("Signature Not Uploaded. Signature size 2kb to 50kb.");
					 	    	$("#hideTxt").val("FAIL");					 	    								   
						 	}else if(data == "RESOLUTION_ERROR") {
						 		$("#imgerror").css("color","red");
					 	    	$("#imgerror").html("Signature Not Uploaded. Signature resolution should be 420 * 120 pixels.");
					 	    	$("#hideTxt").val("FAIL");	
							}else if(data == "TYPE_ERROR"){
								$("#imgerror").css("color","red");
					 	    	$("#imgerror").html("Signature Not Uploaded. Signature type must be .jpeg or .jpg format.");
					 	    	$("#hideTxt").val("FAIL");				 	    		
							}else if(data == "DUPLICATE_CODE_ERROR"){
								$("#imgerror").css("color","red");
					 	    	$("#imgerror").html("Cao Code Already Exist.");
					 	    	$("#hideTxt").val("FAIL");	
							}else if(data == "SUCCESS"){								  
								$("#hideTxt").val("SUCCESS");
								$("#imgerror").html("");								
							}  	 
			 	    	}	 	    	  						 	    	
			 	  	});							
					jQuery.ajaxSetup({async:true});											
		});
		
		$("#uploadActBtn").click(function(){
			if(validateForm()){
				$("#imgerror").html("");
				var url = "<c:url value='/activationController/saveform' />" ;
				var fd = new FormData(document.getElementById("activationForm"));	
					$.ajax({
			 	    	url: url,
			 	    	data: fd,
			 	   	 	dataType: 'text',
			 	    	processData: false,
			 	    	contentType: false,
			 	    	type: 'POST',
			 	    	success: function(data){
			 	       	    $('#result').html(data); 	 	          		
		 	          		jAlert("Signature Upload Successfully.");
		 	          		$("#popup_content").css("background","white");
		 	          		$('#activationForm')[0].reset();				                                
			 	    	}	    	
			 	  	});	
			}		 
		});						
	});
		function validateForm() {
			var trimCaoName = $.trim($('#caoName').val());
			var trimCaoCode = $.trim($('#caoCode').val());
			var fileName=$("#imageName").val();       
			
			if (trimCaoName == null || trimCaoName == "") {
					$("#imgerror").css("color", "red");
					$("#imgerror").html("Please Enter Cao Name.");
					return false;
				}
				if (trimCaoCode == null || trimCaoCode == "") {
					$("#imgerror").css("color", "red");
					$("#imgerror").html("Please Enter Cao Code.");
					return false;
				}
				if(fileName == ""){
					$("#imgerror").css("color", "red");
					$("#imgerror").html("Please Select Signature File.");
					return false;
				}
				if($('#caoCode').val() != (fileName.substring(0,fileName.lastIndexOf('.')))){
					$("#imgerror").css("color", "red");
					$("#imgerror").html("Signature Not Uploaded. Cao Code and Signature name not same.");
					return false;
				}				
				if($("#hideTxt").val() == "FAIL"){
					$("#imgerror").css("color", "red");
					$("#imgerror").html("Upload Valid Signature.");
					return false;
				}
			return true;
		}

	
</script>
<style>
#mainActivation {
	float: left;
    height: 80%;
    position: relative;
    width: 45%;
}
#noteDiv{   
    border: 1px solid;      
    font-size: 11px;
    width:40%;
    height: 79%;
    position:relative;
    float: right;
    padding: 10px;
    right: 80px;
}

</style>
<div class="content" >
	<input type="hidden" id="hideTxt" value="SUCCESS"/>
	<div style="height:44%; width: 100%;">
	<div id="mainActivation">
		<form:form id="activationForm" method="post" action="${url}"
			modelAttribute="signatureFile" enctype="multipart/form-data"
			onload="clearForm()">
			<table height="242" style="border: 1px solid; padding: 15px;">
				<tr>
					<td><b><label style="font-size: 12px;">CAO Name :</label></b></td>
					<td><form:input type="text" path="caoName" size="22" id="caoName" tabindex="1" style="font-size: 12px;"></form:input></td>
				</tr>
				<tr>
					<td><b><label style="font-size: 12px;">CAO Code :</label></b></td>
					<td><form:input type="text" path="caoCode" size="22" id="caoCode" tabindex="2" style="font-size: 12px;"></form:input></td>
				</tr>
				<tr>
					<td><b><label style="font-size: 12px;">Signature File :</label></b></td>
					<td><form:input type="file" path="imageName" id="imageName" size="22" name="imageName" tabindex="3" style="font-size: 12px;"></form:input></td>				
					<td><form:input type="hidden" path="id" id="id" size="25" name="id" tabindex="4" style="font-size: 12px;"></form:input></td>
				</tr>
				<tr>
					<td colspan="2" style="font-size: 12px;"><label id="imgerror"></label></td>									
				</tr>
			
			   <tr>
			   		<td></td>
			  	 	<td> 
			     		<button type="button" id="uploadActBtn" tabindex="4">Upload</button> 
			     		<button type="reset" id="resetBtn" onClick="this.form.reset()" tabindex="5">Reset</button>
			   		</td>
			 </tr>
			</table>	
		</form:form>
	</div>
	<div id="noteDiv">
	    <label><b>Instruction For Signature Image:</b></label>
	    <ul>	       
	    	<li>The signature should be on white paper with Black Ball Point pen. </li>
	    	<li>Image Dimensions <font style="color: red;">420 x 120 pixel </font>will preferred. </li>
	    	<li>Image should be of type <font style="color: red;"> JPG or JPEG</font>.</li>
	    	<li>Image Size should in between <font style="color: red;">2 - 50 KB. </font></li>
	    	<li>Ensure that the size of the scanned image is not more than <font style="color: red;"> 50 KB </font></li>
	    	<li>Signature image name should be <font style="color: red;"> CAOCODE.jpg </font></li>
	    	 </br> Ex. if cao code is 123456ABC78 then signature image name should </br>be 123456ABC78.jpg or 123456ABC78.jpeg 
	    </ul>
	</div>	
	</div>
	

</div>
<div id="result" style="width: 800px;"></div>
