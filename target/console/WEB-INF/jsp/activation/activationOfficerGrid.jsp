<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<c:url value="/resources/images" var="imageUrl" />
<c:url value="/activationController/getactivationmonthwiselist" var="gridUrl" />
<script type="text/javascript">
  function editSignature(id){	
	$( "#editSignatureDialogDiv" ).dialog({
		   open: function(event, ui) {
		     $(this).load("../activationController/geteditsignaturepage",$.param({id:id}), function() {
		    	 $("#imageFileName").val();
			 });
		   },
		  modal: true,
		  width:615,
		  title: 'Edit Activation Form',
		  position: 'top',
		  buttons : {
			  'Update' : function(){				  
				  jConfirm("Do You Want To Update?", "Confirm Update", function(r){
		        		 if (r){
			        		if(validateActivatonForm()){			        				        			 
		        			  var url = "<c:url value='/activationController/saveform' />" ;
		     				  var fd = new FormData(document.getElementById("editSignForm"));	
		     					$.ajax({
		     			 	    	url: url,
		     			 	    	data: fd,
		     			 	   	 	dataType: 'text',
		     			 	    	processData: false,
		     			 	    	contentType: false,
		     			 	    	type: 'POST',
		     			 	    	success: function(data){		     			 	     	 	          		
		     		 	          		jAlert("Update Successfully.");
		     		 	          		$("#popup_content").css("background","white");
		     		 	          		$("#editSignForm").remove();
										$("#editSignatureDialogDiv").dialog("close");
										$( ".ui-dialog" ).remove(); 
										$('#tabs').tabs('load', 1);				                                
		     			 	    	}	    	
		     			 	  	});	
			        		 }        			 			        			 		        	
			        	}
				  });				  				  				       			        		             
			  },
			  'Cancel': function(){					  
				  $("#editSignForm").remove();
				  $("#editSignatureDialogDiv").dialog("close");
				  $( ".ui-dialog" ).remove();
			  }
		  }
    });		
  }
  function disableOrEnableAssignedUser(caoId,msg,status){  
	
      $("#dialog-confirm").html("Are you sure you want to "+msg+" CAO ?");
       $("#dialog-confirm").dialog({
              resizable: false,
              modal: true,
              title: "Confirm Update",
              height: 130,              
              width: 300,
              buttons: {
                  "Yes": function () {
                	  
                      $(this).dialog('close');
                      $.get("<c:url value='/activationController/changecaostatus'/>",$.param({id:caoId,status:status}),function(data){
                          if(data=='success'){
                        	 
                              jAlert("CAO "+msg+" Successfully...!!!","CAO Disable/Enable",function() {
                            	  $('#tabs').tabs('load', 1);  
                              });
                               
                          }
//                           var pageno = $('#output').val();
//                           loadAssignUserTable(pageno);
                         
                      });
                  },
                      "No": function () {
                      $(this).dialog('close');
                  }
              }
          });
  }
  
  function validateActivatonForm(){
	if($("#caoNameTxt").val() == null || $("#caoNameTxt").val() == ""){
		jAlert("Please Enter Cao Name.");
		return false;
	}
	if($("#imageFileName").val() == ""){
		jAlert("Please Select Signature File.");
		return false;
	}
	if($("#hideTxtBox").val() == "FAIL"){
		jAlert("Upload Valid Signature.");
		return false;
	}
	return true;
  }
$(document).ready(function() {
		$("#btnEdit").button(); 				
		
	$("#grid1").jqGrid({
     	url:'${gridUrl}',
		datatype : 'json',
		mtype : 'GET',
		colNames : ['Id', 'Activation Officer Name', 'Activation Officer Code', 'Action','status'],
		colModel : [ {
			name : 'id',
			index : 'id',
			width : 6,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			},
			hidden : true
		}, {
			name : 'caoName',
			index : 'caoName',
			width : 25,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		}, {
			name : 'caoCode',
			index : 'caoCode',
			width : 25,
			editable : false,
			hidden : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		},{
			name : 'action',
			index : 'action',
			width : 20,
			editable : false,
			hidden : false,
			sortable : false,
			editoptions : {
				readonly : true,
			}
		},{
			name : 'status',
			index : 'status',
			width : 6,
			editable : false,
			sortable : false,
			editoptions : {
				readonly : true,
			},
			hidden : true
		}],
		postData :{month:$("#month").val()},
		rowNum :30,
		rowList : [ 30, 60, 90, 120, 150, 250, 400, 600, 800, 900, 1000 ],
		height : 'auto',
		autowidth : true,
		rownumbers : true,
		toppager : true,
		pager: '#pager',
		refresh : true,
		sortname : 'id',
		viewrecords : true,
		sortorder : "desc",
		emptyrecords : "Empty records",
		loadonce : false,
		subGrid: false,
		hidegrid: false,
		footerrow: true,
		userDataOnFooter: true,		
		subGridOptions: { 
              "plusicon" : "ui-icon-info",
			  "minusicon" : "ui-icon-triangle-1-s", 
			  "openicon" : "ui-icon-arrowreturn-1-e" 
			},				
          jsonReader : {
			root : "rows",
			page : "page",
			total : "total",
			records : "records",
			repeatitems : false,
			cell : "cell",
			id : "id"
		},gridComplete: function(){				
			var ids = jQuery("#grid1").jqGrid('getDataIDs');
			for(var i=0; i < ids.length; i++){
				var cl = ids[i]; 
				var rowData=jQuery('#grid1').jqGrid ('getRowData', cl);
				var status = jQuery('#grid1').jqGrid ('getCell', cl, 'status');
				
				//alert("Status : "+(status==0));
				if(status == 0){
					editAction = "<button id='btnEdit' type='button' onclick=\"editSignature("+cl+")\" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' title='Edit'><img src='${imageUrl}/Text-Edit-icon.png' width='20' height='15'/></button>";
					editAction = editAction + " <button id='btnEdit' type='button' onclick=\"disableOrEnableAssignedUser("+cl+",'Disable',1)\" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' title='Disable'><img src='${imageUrl}/active.png' width='20' height='15'/></button>";
				}
				else
					editAction = "&nbsp;&nbsp;&nbsp;&nbsp; <button id='btnEdit' type='button' onclick=\"disableOrEnableAssignedUser("+cl+",'Enable',0)\" class='ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only' title='Enable'><img src='${imageUrl}/deactive.png' width='20' height='15'/></button>";
				var avamId=rowData.id;
				jQuery("#grid1").jqGrid('setRowData',ids[i],{action : editAction});
			}

		},
		loadComplete: function(){
// 			jQuery("#grid1").jqGrid('setCaption',"Records found : " + data.records );
		 }
	 });

	$("#grid1").jqGrid('navGrid','#pager', {
		edit : false,
		add : false,
		del : false,
		search : false
	}, {}, {}, {}, { // search
		sopt : [ 'cn', 'eq', 'ne', 'lt', 'gt', 'bw', 'ew' ],
		closeOnEscape : true,
		multipleSearch : true,
		closeAfterSearch : true
	});			
});


</script>
<div>
	<div id='jqgrid'>	
		<table id='grid1' style="text-align: center; font-size: 12px;"></table>	
	</div>
</div>
<div id="editSignatureDialogDiv"></div>
<div id="dialog-confirm"></div>