<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<script type="text/javascript">
$(document).ready(function() {
	$("#imageFileName").change(function(){
		jQuery.ajaxSetup({async:false});
		var url = "<c:url value='/activationController/imageFileValidation' />" ;
		var fd = new FormData(document.getElementById("editSignForm"));	
			$.ajax({
	 	    	url: url,
	 	    	data: fd,
	 	   	 	dataType: 'text',
	 	    	processData: false,
	 	    	contentType: false,
	 	    	type: 'POST',
	 	    	success: function(data){
	 	    		if(data == "SIZE_ERROR"){
	 	    			$("#imgeror").css("color", "red");
	 	    			$("#imgeror").html("Signature Not Uploaded. Signature size 2kb to 50kb.");
			 	    	$("#hideTxtBox").val("FAIL");				 	    		 	    								   
				 	}else if(data == "RESOLUTION_ERROR") {
				 		$("#imgeror").css("color", "red");
				 		$("#imgeror").html("Signature Not Uploaded. Signature resolution should be 420 * 120 pixels.");
			 	    	$("#hideTxtBox").val("FAIL");			 	    		
					}else if(data == "TYPE_ERROR"){
						$("#imgeror").css("color", "red");
						$("#imgeror").html("Signature Not Uploaded. Signature type must be .jpeg or .jpg format.");
			 	    	$("#hideTxtBox").val("FAIL");			 	    						 	    		
					}else if(data == "SUCCESS"){								  
						$("#hideTxtBox").val("SUCCESS");
						$("#imgeror").html("");	
					}  	 
	 	    	}	 	    	  						 	    	
	 	  	});							
			jQuery.ajaxSetup({async:true});		
	});	
});

</script>
<div id="editSignature">
<input type="hidden" id="hideTxtBox" value="SUCCESS"/>
  <c:url var="url" value="/activationController/saveform" />
	<form id="editSignForm" name="editSignForm" method="post" action="${url}" enctype="multipart/form-data">
		<fieldset>
			<legend style="margin-left: 10px;"> Update Activation Officer</legend>
			<table cellspacing="5" cellpadding="5">
				<tr>
					<td><label style="font-weight: bold; font-size: 12px;">CAO Name :</label></td>
					<td><input type="text" name="caoName" id="caoNameTxt" size="22" value="${caoVo.caoName}" style="font-size: 12px;"/></td>
					<td><label style="font-weight: bold; font-size: 12px;">CAO Code :</label></td>
					<td><input type="text" name="caoCode" id="caoCodeTxt" size="22" value="${caoVo.caoCode}" readonly="true" style="font-size: 12px;"/>
					</td>
				</tr>
				<tr>
					<td><label style="font-weight: bold; font-size: 12px;">Signature File :</label></td>
					<td><input type="file" name="imageName" id="imageFileName" size="25" value="${caoVo.imageName}" style="font-size: 12px;"/></td>
					<td></td>
					<td><input type="hidden" name="id" id="id" size="22" value="${caoVo.id}" style="font-size: 12px;"/></td>
				</tr>
				<tr>
					<td colspan="2" style="font-size: 12px;"><label id="imgeror"></label></td>				
				</tr>
			</table>
		</fieldset>
	</form>
</div>