//var slcremak = "NOT_SELECTED"; 

var states;
var cities;
var districts;
var cityresponse = 0;
var distresponse = 0;
var noOfPages = 0;
var mdnbeforeChange = null;
var activationdate = null;
var oldDataEntryVO = null;
var placeholder = "" ;
var rejectReasons=[];

$(document).ready(function(){
	
	$("#dob").mask("99/99/9999", {
		placeholder : "_"
	});
	$("#visaValidTill").mask("99/99/9999", {
		placeholder : "_"
	});

	$("#existingConnection").mask("9999999999", {
		placeholder : "_"
	});
	$("#localReferenceContactNumber").mask("9999999999", {
		placeholder : "_"
	});

	// / $("#simNumber").mask("9999999999999999999",{placeholder:"_"});
	$("#mobileNumber").mask("9999999999", {
		placeholder : "_"
	});
	$("#localPincode").mask("999999", {
		placeholder : "_"
	});
	// $("#permanentPincode").mask("999999",{placeholder:"_"});
	$("#localReferencePincode").mask("999999", {
		placeholder : "_"
	});
	$("#poiIssueDate").mask("99/99/9999", {
		placeholder : "_"
	});
	$("#cafNumber").mask("999999999999", {
		placeholder : "_"
	});
	$("#poaIssueDate").mask("99/99/9999", {
		placeholder : "_"
	});
	$("#activationDateTxt").mask("99/99/9999", {
		placeholder : "_"
	});
	
//	$("#rejectOpt1").hide();
	$("#rejectOpt2").hide();
	$("#rejectOpt3").hide();
	$("#rejectOpt4").hide();
	$("#rejectOpt5").hide();
	$("#rejectOpt6").hide();
	$("#rejectOpt7").hide();
	$("#rejectOpt8").hide();
	$("#rejectOpt9").hide();
	$("#rejectOpt10").hide();
	$("#rejectOpt11").hide();
	$("#rejectOpt12").hide();
	
	$("#speedocId").focus();
	
	$("#rejectDone").hide();
	$("#rejectDone").button();
//	$("#rejectBut").click(function(){
//		$("#rejectOpt1").slideToggle();
//	});
	
	$("#photoComplianceBtn").click(function(){
		var photoCom = $("#photoComplianceBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt1").html(data);
			setSelected("#rejectOpt1");
			$("#rejectOpt1").show();
			$("#rejectOpt2").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#documentComBtn").click(function(){
		var docu = $("#documentComBtn").val();
		$.post("../slc/getrejectreason?header="+docu,function(data){
			$("#rejectOpt2").html(data);
			setSelected("#rejectOpt2");
			$("#rejectOpt2").show();
			$("#rejectOpt1").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#cafComBtn").click(function(){
		var photoCom = $("#cafComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt3").html(data);
			setSelected("#rejectOpt3");
			$("#rejectOpt3").show();
			$("#rejectOpt1").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#customerComBtn").click(function(){
		var photoCom = $("#customerComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt4").html(data);
			setSelected("#rejectOpt4");
			$("#rejectOpt4").show();		
			$("#rejectOpt1").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#caoCombtn").click(function(){
		var photoCom = $("#caoCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt5").html(data);
			setSelected("#rejectOpt5");
			$("#rejectOpt5").show();	
			$("#rejectOpt1").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#retailerComBtn").click(function(){
		var photoCom = $("#retailerComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt6").html(data);
			setSelected("#rejectOpt6");
			$("#rejectOpt6").show();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});	
	});
	
	$("#distributorComBtn").click(function(){
		var photoCom = $("#distributorComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt7").html(data);
			setSelected("#rejectOpt7");
			$("#rejectOpt7").show();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});	
	});
	
	$("#overwritingBtn").click(function(){
		var photoCom = $("#overwritingBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt8").html(data);
			setSelected("#rejectOpt8");
			$("#rejectOpt8").show();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#outstationCombtn").click(function(){
		var photoCom = $("#outstationCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt9").html(data);
			setSelected("#rejectOpt9");
			$("#rejectOpt9").show();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#mnpCombtn").click(function(){
		var photoCom = $("#mnpCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt10").html(data);
			setSelected("#rejectOpt10");
			$("#rejectOpt10").show();
			$("#rejectOpt9").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#databaseCombtn").click(function(){
		var photoCom = $("#databaseCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){		
			$("#rejectOpt11").html(data);
			setSelected("#rejectOpt11");
			$("#rejectOpt11").show();
			$("#rejectOpt10").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#otherbtn").click(function(){
		var photoCom = $("#otherbtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt12").html(data);
			setSelected("#rejectOpt12");
			$("#rejectOpt12").show();
			$("#rejectOpt11").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#slcsave").click(function(){
		var formObject = $("#myForm").serializeObject();
		formObject.dob = saveDateFormate(formObject.dob);
		formObject.poaIssueDate = saveDateFormate(formObject.poaIssueDate);
		formObject.poiIssueDate = saveDateFormate(formObject.poiIssueDate);	
		formObject.activationDate = saveDateFormate(formObject.activationDate);	
		formObject.visaValidTill = saveDateFormate(formObject.visaValidTill);	
		
		var speedoc = formObject.speedocId;
//		alert("speedoc :: " + speedoc);
		
		//commented by @Kisshor 28-08-2015
		//$.post("../slc/alreadyexistspeedoc",{speedocId:speedoc}, function(data){		
		//if(data == true){
		//	reset();
		//	rePipeNextCaf();
		//}else{
		//end
			
				$.post("../slc/save",formObject,function(data) {
					var sr = data; 
					if(sr.successful){
						reset();
						rePipeNextCaf();				
					}else{
						reset();
						rePipeNextCaf();
					}
				}) ;	
			//}			
		//});		
	});
		
	$("#slcImageRotateBtn").click(function(){	// added on 15-01-2015			
		rotateSlcCafImage();
	});
	
	
	$("#rejectDone").click(function(){		
		if( rejectReasons.length != 0){
			var formObject = $("#myForm").serializeObject();
			formObject.dob = saveDateFormate(formObject.dob);
			formObject.poaIssueDate = saveDateFormate(formObject.poaIssueDate);
			formObject.poiIssueDate = saveDateFormate(formObject.poiIssueDate);
			formObject.visaValidTill = saveDateFormate(formObject.visaValidTill);
			formObject.activationDate = saveDateFormate(formObject.activationDate);
			
			var speedoc = formObject.speedocId;
//			alert("speedoc :: " + speedoc);
			
			//commented by @Kisshor 28-08-2015
			
			//$.post("../slc/alreadyexistspeedoc",{speedocId:speedoc}, function(data){		
			//	if(data == true){
			//		reset();
			//		rePipeNextCaf();
			//	}else{
			
			//end
					$.post("../slc/renamereject?slcremak="+rejectReasons, formObject , function(data){
						var response = data;
						if(response.successful){
							reset();
							$("#rejectOpt1").hide();
							$("#rejectOpt2").hide();
							$("#rejectOpt3").hide();
							$("#rejectOpt4").hide();
							$("#rejectOpt5").hide();
							$("#rejectOpt6").hide();
							$("#rejectOpt7").hide();
							$("#rejectOpt8").hide();
							$("#rejectOpt9").hide();
							$("#rejectOpt10").hide();
							$("#rejectOpt11").hide();
							$("#rejectOpt12").hide();
							$("#rejectDone").hide();
							rejectReasons.length = 0;
							rePipeNextCaf();
						}else{
							jAlert("Rejection Failed.","Data Entry");
							reset();
							$("#rejectOpt1").hide();
							$("#rejectOpt2").hide();
							$("#rejectOpt3").hide();
							$("#rejectOpt4").hide();
							$("#rejectOpt5").hide();
							$("#rejectOpt6").hide();
							$("#rejectOpt7").hide();
							$("#rejectOpt8").hide();
							$("#rejectOpt9").hide();
							$("#rejectOpt10").hide();
							$("#rejectOpt11").hide();
							$("#rejectOpt12").hide();
							$("#rejectDone").hide();
							rejectReasons.length = 0;
							rePipeNextCaf();
						}
				 	});	
				//}
			//});
		}else{
			$("#rejectDone").blur();
			jAlert("Reject reason mandatory. Please select whichever is applicable.","Data Entry",function(){
				$("#rejectDone").focus();
			});
		}			
	 });
   rePipeNextCaf();
}) ;

	var rejectReasons=[];
	function addRejectReasons(value, ele){ 
		if (ele.checked) {
			rejectReasons.push(value);
		 }else{
			 var reasons1 = [];
			 for(var i=0; i < rejectReasons.length; i++){
				 if(rejectReasons[i] != value){
					 reasons1.push(rejectReasons[i]);
				 } 
			 }
			 rejectReasons = reasons1;
		 }
	}
	
	function setSelected(id){ 
		$(id + " input[type=checkbox]").each(
			    function() {
			    	 for(var i=0; i < rejectReasons.length; i++){
						 if(rejectReasons[i] == this.value){
							 this.checked = true;
						 } 
					 }
			    }
			);
	} 

function reset(){
	$("#myForm :input").val("");
//	$("#rejectOpt1").hide();
	$("#imagediv").html("");
	
	$("#rejectOpt1").html("");
	$("#rejectOpt2").html("");
	$("#rejectOpt3").html("");
	$("#rejectOpt4").html("");
	$("#rejectOpt5").html("");
	$("#rejectOpt6").html("");
	$("#rejectOpt7").html("");
	$("#rejectOpt8").html("");
	$("#rejectOpt9").html("");
	$("#rejectOpt10").html("");
	$("#rejectOpt11").html("");
	$("#rejectOpt12").html("");
	rejectReasons.length =0;
	
	 $("#connectionOptingFor").val('Personal');
		// $("#localReferenceCity").val('1');
		$("#cafNumber").val("");
		$("#cafNumber").focus();
		$("#cafImages").html("");

		$("#monthlyExpenditure").val(0);
	//	$("#localState").change();

		if (document.getElementById("foreignNational").checked) {

			$("#foreignNational").attr('checked', false);
			$("#outstation").attr('checked', false);
			$("#copyCommonFieldsCheckbox").attr('checked', false);
//			foreignNationalDisabled(true);
//			displayForeignNationalRow(false);
//			outstationDisabled(true);
//			displayOutStationRow(false);
			$('#copyCommonFieldsCheckboxTr').hide();
			$('#copyCommonFieldsCheckboxTr1').hide();
			$("#foreignNational").val("N");
		}

		if (document.getElementById("outstation").checked) {

			$('#copyCommonFieldsCheckboxTr').hide();
			$('#copyCommonFieldsCheckboxTr1').hide();
			$('#outstationTr2').hide();
			$("#outstation").attr('checked', false);
			$("#copyCommonFieldsCheckbox").attr('checked', false);
//			outstationDisabled(true);
//			displayOutStationRow(false);
			$("#outstation").val("N")
		}

		if (document.getElementById("mnp").checked) {
			$("#mnp").attr('checked', false);
			$("#mnp").val("N");
			$("#monthlyExpenditure").val(0);
			$("#existingConnection").val("");
			$("#mnpCheckBox").hide();
//			mnpHide();
		}

		$("#mnpCheckBox").hide();
		$("#pageNo").html("");		
}


function fillForm1(dataEntryVO) {
	// checkedCheckBoxes(true);
	
	jQuery.ajaxSetup({
		async : false
	});
	oldDataEntryVO = dataEntryVO;
	
	$("#speedocId1").val(dataEntryVO.speedocId);	
	$("#mobileNumber1").val(dataEntryVO.mobileNumber);
	if (dataEntryVO.cafNumber == 'null') {
		$("#cafNumber").val("");
	} else {
		$("#cafNumber").val(dataEntryVO.cafNumber);
	}
	$("#id").val(dataEntryVO.id);
	$("#cafStatusId1").val(dataEntryVO.cafStatusId);
	$("#maritalStatus").val(dataEntryVO.maritalStatus);	
	$("#images1").val(dataEntryVO.images);
	$("#cafFileLocation").val(dataEntryVO.cafFileLocation);
	$("#cafType").val(dataEntryVO.cafType);
	$("#onhold").val(dataEntryVO.onhold);
	// for rotate Page
	$("#fileName2").val(dataEntryVO.cafFileLocation);
	$("#pageNo2").html("");
	noOfPages = 0;
	noOfPages = dataEntryVO.pages;
//	alert("pages : "+dataEntryVO.pages);
	for (var i = 0; i < noOfPages; i++) {
		$("#pageNo2").append(
				"<option  value='" + (i + 1) + "'>" + (i + 1) + "</option>");	

	}	

	$.trim($('#firstName1').val(dataEntryVO.firstName));
	$.trim($('#middleName1').val(dataEntryVO.middleName));
	$.trim($('#lastName1').val(dataEntryVO.lastName));
//	$("#firstName").val(dataEntryVO.firstName);
//	$("#middleName").val(dataEntryVO.middleName);
//	$("#lastName").val(dataEntryVO.lastName);
	$.trim($('#fatherFirstName').val(dataEntryVO.fatherFirstName));
	$.trim($('#fatherMiddleName').val(dataEntryVO.fatherMiddleName));
	$.trim($('#fatherLastName').val(dataEntryVO.fatherLastName));
//	$("#fatherFirstName").val(dataEntryVO.fatherFirstName);
//	$("#fatherMiddleName").val(dataEntryVO.fatherMiddleName);
//	$("#fatherLastName").val(dataEntryVO.fatherLastName);
	$("#localAddress").val(dataEntryVO.localAddress);
	$("#permanentAddress").val(dataEntryVO.permanentAddress);
	$("#localAddressLandmark").val(dataEntryVO.localAddressLandmark);

	if (!(dataEntryVO.localPincode == null)) {
		$("#localPincode").val(dataEntryVO.localPincode);
	}

	$("#alternateMobileNumber").val(dataEntryVO.alternateMobileNumber);

	$("#panGirNumber").val(dataEntryVO.panGirNumber);
	$("#poiref").val(dataEntryVO.poiref);
	$("#poaref").val(dataEntryVO.poaref);

	$("#photograph").val(dataEntryVO.photograph);
	$("#gender").val(dataEntryVO.gender);
	$("#title").val(dataEntryVO.title);

	$("#nationality").val(dataEntryVO.nationality);

	$("#localState").val(dataEntryVO.localState);

	$("#localCity").val(dataEntryVO.localCity);

	$("#localDistrict").val(dataEntryVO.localDistrict);

	$("#form6061").val(dataEntryVO.form6061);
	$("#poi").val(dataEntryVO.poi);
	$("#poa").val(dataEntryVO.poa);
	$("#poiIssuingAuthority").val(dataEntryVO.poiIssuingAuthority);
	$("#poaIssuingAuthority").val(dataEntryVO.poaIssuingAuthority);
	$("#category").val(dataEntryVO.category);

	// new
	$("#poiIssuePlace").val(dataEntryVO.poiIssuePlace);

	$("#poaIssuePlace").val(dataEntryVO.poaIssuePlace);

	$("#partyNumber").val(dataEntryVO.callingPartyNumber);
	
	$("#permanentState").val(dataEntryVO.permanentState);
	$("#activationOfficerName").val(dataEntryVO.activationOfficerName);
	$("#activationOfficerDesignation").val(dataEntryVO.activationOfficerDesignation);
	$("#statusOfSubscriber").val(dataEntryVO.statusOfSubscriber);
	$("#subscriberProfession").val(dataEntryVO.subscriberProfession);
	$("#imsiNumber").val(dataEntryVO.imsiNumber);
	$("#vasApplied").val(dataEntryVO.vasApplied);
	$("#category").val(dataEntryVO.category);
	$("#emailAddress").val(dataEntryVO.email);
	$("#uidNumber").val(dataEntryVO.uidNumber);
	$("#tarrifPlanApplied").val(dataEntryVO.tarrifPlanApplied);
	$("#existingNumber").val(dataEntryVO.existingNumber);
	$("#upcCode").val(dataEntryVO.upcCode);
	$("#prevServiceProviderName").val(dataEntryVO.prevServiceProviderName);
	$("#cafTrackingStatus").val(dataEntryVO.cafTrackingStatus);
	$("#wap").val(dataEntryVO.rtrCode);
	$("#mms").val(dataEntryVO.rtrName);
	
	if (dataEntryVO.dob != null) {		
		var date = dateformate(dataEntryVO.dob);
		$("#dob").val(date);
	}else{
		$("#dob").val("");
	}

	if (dataEntryVO.poiIssueDate != null) {
		var date = dateformate(dataEntryVO.poiIssueDate);
		$("#poiIssueDate").val(date);
	}else{
		$("#poiIssueDate").val("");
	}

	if (dataEntryVO.poaIssueDate != null) {
		var date = dateformate(dataEntryVO.poaIssueDate);
		$("#poaIssueDate").val(date);
	}else{
		$("#poaIssueDate").val("");
	}
	
	if (dataEntryVO.activationDate != null) {
		var date = dateformate(dataEntryVO.activationDate);
		$("#activationDateTxt").val(date);
	}else{
		$("#activationDateTxt").val("");
	}
	
	localState: {
		if (dataEntryVO.foreignNational != null) {
			if (dataEntryVO.foreignNational.toUpperCase() == 'Y') {

				$("#foreignNational").attr('checked', true);

				//showForeignNational();

				fillPassportVistDetailsFields(dataEntryVO);

				// fillCommonFieldsCheckbox();
				$("#copyCommonFieldsCheckbox").attr('checked', true);
				$('#localReferenceName').val(dataEntryVO.localReferenceName);
				$('#localReferenceAddress').val(dataEntryVO.localAddress);
				$("#localReferenceCity").val(dataEntryVO.localReferenceCity);
				$('#localReferenceContactNumber').val(dataEntryVO.mobileNumber);
				$('#localReferencePincode').val(dataEntryVO.localPincode);
			}
		}

		if (dataEntryVO.outstation != null) {
			if (dataEntryVO.outstation.toUpperCase() == "Y") {

				$("#outstation").attr('checked', true);
				showOutstation();
				// fillCommonFieldsCheckbox();
				$("#copyCommonFieldsCheckbox").attr('checked', true);
				$('#localReferenceName').val(dataEntryVO.localReferenceName);
				$('#localReferenceAddress').val(dataEntryVO.localAddress);
				$("#localReferenceCity").val(dataEntryVO.localReferenceCity);
				$('#localReferenceContactNumber').val(dataEntryVO.mobileNumber);
				$('#localReferencePincode').val(dataEntryVO.localPincode);
			}
		}

		if (dataEntryVO.outstation != null && dataEntryVO.foreignNational != null) {
			if (dataEntryVO.outstation.toUpperCase() != "Y"
					&& dataEntryVO.foreignNational.toUpperCase() != "Y") {
				$('#copyCommonFieldsCheckboxTr').hide();
				$('#copyCommonFieldsCheckboxTr1').hide();
			}
		}

		if (dataEntryVO.connectionType != 'null') {
			$("#connectionType").val(dataEntryVO.connectionType);
		}

		jQuery.ajaxSetup({
			async : true
		});
	}
	
	// Cao Image 
	$.post("../slc/getcaoimage", $.param({caoCode:dataEntryVO.caoCode}), function(data){
		var response = data;
		if(response.successful)
		{
			var caoVo =  response.result;
			var htmlImage = "";
			var timestamp = new Date().getTime();
			htmlImage += "<img onmouseover=\"bigImg(this)\" onmouseout=\"normalImg(this)\" id=\"ImageDiv\" alt=\"\" src=\"../slc/getcaoimage?fileName="
				+ caoVo.imageName
				+ "&timestamp="
				+ timestamp
				+ "\" style='width:100%'  > <br>";
			htmlImage += "</div>";
						
			$("#caoName").val(caoVo.caoName);
			$("#caoCode").val(caoVo.caoCode);
			$("#imagediv").html(htmlImage);
		}
	 });
	
	// election commission 26-05-2015
	$("#voterId").val(dataEntryVO.voterId);
	$("#voterName").val(dataEntryVO.voterName);
	$("#pollingStation").val(dataEntryVO.pollingStation);
	
}

function dateformate(timestamp){
	if(timestamp != ""){
		var strDate = timestamp.split(" ");
		var date = strDate[0].split("-");
		return date[2] + "-" + date[1] + "-"+ date[0];
	}
}

function formateDate(data){
	var formatted = "" ;
	if(data != null || data != ""){
	 var mydate = moment.utc(data).toDate() ;
	 if(mydate == "Invalid Date"){
		 var dateArray =  data.split(":") ;
		 var dateSplit = dateArray[0].split("-") ;
		 var mm = ""; 
		 var dd = "" ;
		 if(dateSplit[1].length == 1){
			 mm = "0"+dateSplit[1].charAt(0) ;
		 }else{
			 mm = dateSplit[1] ; 
		 }		 
		 if(dateSplit[2].charAt(0).length == 1){
			 dd = dateSplit[2].charAt(0)+dateSplit[2].charAt(1) ;
		 }		 
		 var newDate = moment.utc(dateSplit[0]+"-"+mm+"-"+dd+" 00"+":00"+":00").toDate() ;		
		 var d =  new Date(newDate) ;
         formatted =  d.getDate()+"-"+(d.getMonth()+1)+"-"+d.getFullYear() ;
	   }
	return formatted ;
 }
}

function formateDateToTimestamp(data){
	if(data === null || data === ""){
		data = new Date() ;
		var mydate = moment.utc(data).toDate() ;
		var d =  new  Date(mydate) ;
		var formateDate = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+"00:"+"00:"+"00";
		return formateDate ;
	}else{
		var d = data.split("-") ;
		var formateDate = d[0]+"-"+d[1]+"-"+d[2]+" "+"00:"+"00:"+"00";
		return formateDate ;
	}
}

function saveDateFormate(date){
	if(date != null && date != ""){ 
		var d = date.split("-") ;
		var formateDate = d[2]+"-"+d[1]+"-"+d[0]+" "+"00:"+"00:"+"00";
		return formateDate ;
		
	}
}

function fillPassportVistDetailsFields(dataEntryVO) {

	$("#passportNumber1").val(dataEntryVO.passportNumber);

	$("#typeOfVisa").val(dataEntryVO.typeOfVisa);

	if (dataEntryVO.visaValidTill != null) {
		var visaValidTill = dataEntryVO.visaValidTill.split('-');
		$("#visaValidTill").val(visaValidTill[2] + "/" + visaValidTill[1] + "/"+ visaValidTill[0]);
	}
	$("#visaNumber").val(dataEntryVO.visaNumber);

}