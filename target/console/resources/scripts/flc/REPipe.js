(function($) {
$.REPipe = {
		currentCaf : null,
		pipedCaf : null,
		imageCount : 1,
		init : function() {
			$.REPipe.imageCount = 1;
		},
		getNextCaf : function() {
			if ($.REPipe.currentCaf == null) {
//				alert("Load Next Caf : ");
				$.REPipe.loadNextCafAndImage();
			} else {
				$.REPipe.currentCaf = null;
//				alert("Show piped caf : "+$.REPipe.pipedCaf.fileLocation);
				$.REPipe.pipedImagesLoaded();
			}
		},
		getCafImages : function(cafFlcVo) {
			$.post("../flc/getcafimagenames",cafFlcVo,function(data) {
								var sr = data;
								if (sr.successful) {
									var imageNames = null;
									imageNames = sr.result.split(";");
									$.REPipe.pipedCaf.images = imageNames;
									var htmlStr = "";
									var timestamp = new Date().getTime();

									htmlStr = "";
									for (var i = 0; i < imageNames.length; i++) {
										if (imageNames[i] != "") {
											htmlStr += "<img class=\"image_class\" id=\"hiddenImageDiv\" alt=\"\" src=\"../flc/getcaffile?fileName="
													+ imageNames[i]
													+ "&timestamp="
													+ timestamp
													+ "\"> <br>";										
										}
									}
//									htmlStr += "</div>";

									$("#hdiv").html(htmlStr);
									$(".image_class").bind("load", function() {
//										$("#currentImgDiv").html($("#hdiv").html());
//										$("#hdiv").html("");
//										alert("pipedImagesLoaded  : ");
										$.REPipe.pipedImagesLoaded();
//										alert("pipedImagesLoaded  : ");
									});
								}
							});
		},
		pipedImagesLoaded : function() {
			//$("#msgDiv").html("Image Count : " + $.REPipe.imageCount );			
			if ($.REPipe.pipedCaf != null) {
//				if ($.REPipe.imageCount == $.REPipe.pipedCaf.pages) {
					$.REPipe.imageCount = 0;
					 if ($.REPipe.currentCaf == null) { 
						if ($.REPipe.pipedCaf != null) {
							$("#currentImgDiv").html($("#hdiv").html());
//							$("#hdiv").html("");
//							alert("pipedImagesLoaded  : "+$("#hdiv").html());
							$.REPipe.currentCaf = $.REPipe.pipedCaf;
							fillForm($.REPipe.currentCaf);
							$.REPipe.loadNextCafAndImage();
						} else {
							$.REPipe.cafNotFound();
						}
					}
//				} else {
//					$.REPipe.imageCount = $.REPipe.imageCount + 1;
//				}
			} else {
				if($.REPipe.currentCaf == null){
					$.REPipe.cafNotFound();
				}
			}
		},
		loadNextCafAndImage : function() {
			$.post("../flc/getflcdata", null,function(data) {
				var serviceResponse = data;

				$.REPipe.pipedCaf = serviceResponse.result;
				if (serviceResponse.successful) {
					$.REPipe.getCafImages($.REPipe.pipedCaf);
				} else {
					$.REPipe.pipedCaf = null;
					if($.REPipe.currentCaf == null){
						$.REPipe.cafNotFound();
					}
				}
			});
		},
		cafNotFound : function(cafId) {
			$("#currentImgDiv").html("");
			jAlert("CAF is not available for verification.","Data Entry",function(){
				$("#speedocId").focus();
			});
		},
		reCancel : function() {

			var ccafId = "";
			var cnextCafId = "";
			if($.REPipe.pipedCaf != null){
				cnextCafId = $.REPipe.pipedCaf.cafId;
			}
			if ($.REPipe.currentCaf != null) {
				ccafId = $.REPipe.currentCaf.cafId;
			}

			var url = "../flc/cancelremcaf?cafId=" + ccafId
					+ "&nextCafId=" + cnextCafId;
			jQuery.ajaxSetup({async:false});
			$.post(url, null, function(data) {
				
			});
			window.location="../flc/cancelvrcaf";
			jQuery.ajaxSetup({async:true});
		},
	} ;

	rePipeNextCaf = function() {
		$.REPipe.getNextCaf();
	} ;

	rePipeCancel = function() {
		$.REPipe.reCancel();
	} ;	
})(jQuery);
