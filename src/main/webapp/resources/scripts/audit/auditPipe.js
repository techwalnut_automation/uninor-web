$(document).ready(function(){
$.REPipe = {
		currentCaf : null,
		pipedCaf : null,
		imageCount : 1,
		dob : null,
		activationDate : null,
		poaIssueDate : null,
		poiIssueDate : null,
		init : function() {
			$.REPipe.imageCount = 1;
		},
		getNextCaf : function() {

			if ($.REPipe.currentCaf == null) {
				$.REPipe.loadNextCafAndImage();
			} else {
				$.REPipe.currentCaf = null;
				$.REPipe.pipedImagesLoaded();
			}
		},
		getCafImages : function(cafFlcVo) {
			cafFlcVo.cafDate = $.REPipe.stringToTimestamp(cafFlcVo.cafDate) ;
			cafFlcVo.createdDate = $.REPipe.stringToTimestamp(cafFlcVo.createdDate) ;
			
			$.REPipe.dob = cafFlcVo.dob;
			cafFlcVo.dob = $.REPipe.stringToTimestamp(cafFlcVo.dob) ;
			
			cafFlcVo.modifiedDate = $.REPipe.stringToTimestamp(cafFlcVo.modifiedDate) ;
			
			$.REPipe.poaIssueDate = cafFlcVo.poaIssueDate;
			cafFlcVo.poaIssueDate = $.REPipe.stringToTimestamp(cafFlcVo.poaIssueDate) ;
			
			$.REPipe.poiIssueDate = cafFlcVo.poiIssueDate;
			cafFlcVo.poiIssueDate = $.REPipe.stringToTimestamp(cafFlcVo.poiIssueDate) ;
			
			cafFlcVo.scannedDate = $.REPipe.stringToTimestamp(cafFlcVo.scannedDate) ;
			cafFlcVo.visaValidTill = $.REPipe.stringToTimestamp(cafFlcVo.visaValidTill) ;
			
			$.REPipe.activationDate = cafFlcVo.activationDate;
			cafFlcVo.activationDate = $.REPipe.stringToTimestamp(cafFlcVo.activationDate) ;
			
			$.post("../audit/getcafimagenames",cafFlcVo,function(data) {
					var sr = data;
					if (sr.successful) {
						var imageNames = null;
						imageNames = sr.result.split(";");
						$.REPipe.pipedCaf.images = imageNames;
						var htmlStr = "";
						var timestamp = new Date().getTime();

						htmlStr = "";
						for (var i = 0; i < imageNames.length; i++) {
							if (imageNames[i] != "") {
								htmlStr += "<img class=\"image_class\" id=\"hiddenImageDiv\" alt=\"\" src=\"../flc/getcaffile?fileName="
										+ imageNames[i]
										+ "&timestamp="
										+ timestamp
										+ "\" style='width:100%'> <br>";										
							}
						}
//						htmlStr += "</div>";

						$("#hiddenImageDiv1").html(htmlStr);
						$(".image_class").bind("load", function() {
						$.REPipe.pipedImagesLoaded();
					});
				}
			});
		},
		pipedImagesLoaded : function() {
			
			//$("#msgDiv").html("Image Count : " + $.REPipe.imageCount );
			if ($.REPipe.pipedCaf != null) {
//				if ($.REPipe.imageCount == $.REPipe.pipedCaf.pages) {
					$.REPipe.imageCount = 0;
					 if ($.REPipe.currentCaf == null) { 
						if ($.REPipe.pipedCaf != null) {
							$("#cafImages1").html($("#hiddenImageDiv1").html());
							$("#hiddenImageDiv1").html("");
							$.REPipe.currentCaf = $.REPipe.pipedCaf;
							if($.REPipe.activationDate == null){
								$.REPipe.currentCaf.activationDate = null;
							}
							if($.REPipe.dob == null){
								$.REPipe.currentCaf.dob = null;
							}
							if($.REPipe.poaIssueDate == null){
								$.REPipe.currentCaf.poaIssueDate = null;
							}
							if($.REPipe.poiIssueDate == null){
								$.REPipe.currentCaf.poiIssueDate = null;
							}
							fillForm1($.REPipe.currentCaf);
							$.REPipe.loadNextCafAndImage();
						} else {
							$.REPipe.cafNotFound();
						}
					}
//				} else {
//					$.REPipe.imageCount = $.REPipe.imageCount + 1;
//				}
			} else {
				if($.REPipe.currentCaf == null){
					$.REPipe.cafNotFound();
				}
			}
		},
		loadNextCafAndImage : function() {
			$.post("../audit/getauditdata", null,function(data) {
				var serviceResponse = data;
				$.REPipe.pipedCaf = serviceResponse.result;
				if (serviceResponse.successful) {
					$.REPipe.getCafImages($.REPipe.pipedCaf);
				} else {
					$.REPipe.pipedCaf = null;
					if($.REPipe.currentCaf == null){
						$.REPipe.cafNotFound();
					}
				}
			});
		},
		cafNotFound : function(cafId) {
			$("#cafImages1").html("");
			jAlert("CAF is not available for audit.","Data Entry",function(){
				
			});
		},
		reCancel : function() {
			var ccafId = "";
			var cnextCafId = "";
			if($.REPipe.pipedCaf != null){
				cnextCafId = $.REPipe.pipedCaf.cafId;
			}
			if ($.REPipe.currentCaf != null) {
				ccafId = $.REPipe.currentCaf.cafId;
			}

			var url = "../audit/cancelremcaf?cafId=" + ccafId
					+ "&nextCafId=" + cnextCafId;
			jQuery.ajaxSetup({async:false});
			$.post(url, null, function(data) {
				
			});
			window.location="../audit/cancelvrcaf";
			jQuery.ajaxSetup({async:true});
		},
		stringToTimestamp : function(data){
			if(data === null || data === ""){
				data = new Date();
			}else{
				var mydate = moment.utc(data).toDate() ;
				var d =  new Date(mydate) ;
				var formateDate = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
				return formateDate ;
			}	
		}
	} ;

	rePipeNextCaf = function() {
		$.REPipe.getNextCaf();
	} ;

	rePipeCancel = function() {
		$.REPipe.reCancel();
	} ;
});
