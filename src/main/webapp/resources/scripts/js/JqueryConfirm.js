var JqueryConfirm = function() {
	this.init();
}

$.extend(JqueryConfirm.prototype, {
	init : function() {

	},

	confirm : function(params) {
		//alert("called . . .");
		$('a').attr("tabindex", -1);
		$('input').attr("tabindex", -1);
		var buttonCount = 2;
		
		if($('#confirmOverlay').length){
			// A confirm is already shown on the page:
			return false;
		}
		
		var buttonHTML = '';
		$.each(params.buttons,function(name,obj){
			
			// Generating the markup for the buttons:
			
			buttonHTML += '<a href="#" class="button '+obj['class']+'" id='+name+'>'+name+'<span></span></a>';
			if(name == 'Continue')
				{
					buttonCount = 1;
				}
			
			if(!obj.action){
				obj.action = function(){};
			}
		});
		
		var markup = [
			'<div id="confirmOverlay">',
			'<div id="confirmBox">',
			'<h1>',params.title,'</h1>',
			'<div style="padding-left:15px; padding-right:15px;"><img src="/dec-web/images/Are-you-sure-you-want-to-start-the-task.png" align="absmiddle">&nbsp;&nbsp;',params.message,'</div>',
			'<div id="confirmButtons">',
			buttonHTML,
			'</div></div></div>'
		].join('');
		
		$(markup).hide().appendTo('body').fadeIn();
		if(buttonCount == 1)
			{
				$('#Continue').focus();
			}
		else
			{
				$('#Stop').focus();
			}
		
		
		
		var buttons = $('#confirmBox .button'),
			i = 0;

		$.each(params.buttons,function(name,obj){
			buttons.eq(i++).click(function(){
				
				// Calling the action attribute when a
				// click occurs, and hiding the confirm.
				
				obj.action();
                //$.confirm.hide();
				jqueryConfirm.closeDailog();
				return false;
			});
		});
	},

	closeDailog : function() {
		//alert("close");
		$("a").removeAttr('tabindex');
		$("input").removeAttr('tabindex');
		
		$('#confirmOverlay').fadeOut(function(){
			$(this).remove();
		});
	}

});
var jqueryConfirm = new JqueryConfirm();