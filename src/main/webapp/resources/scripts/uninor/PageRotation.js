(function($) {
	$.PageRotation = {
		init : function() {
		},
		//for flc image rotate
		getRotatePageFlc : function() {
			jQuery.ajaxSetup({
				async : false
			});
			var rotateVO = $('#rotatecafflc').serializeObject();
			$.postJSON("../imageController/rotateImage", rotateVO, function(data) {
				var response = data;
				if (response.successful) {
					var location = $("#fileLocation").val();

					$.post("../imageController/getcafimagenames", $.param({cafFileLocation : location}), function(data) {
						var serviceResponse = data;
						if (serviceResponse.successful) {
							$.PageRotation.reloadRotatedImagesFlc(serviceResponse);
						}
					});
				} 
			});
		},
		
		reloadRotatedImagesFlc : function(serviceResponse) {
			var imageNames = null;
			imageNames = serviceResponse.result.split(";");
			var htmlStr = "";
			var timestamp = new Date().getTime();

			for ( var i = 1; i < imageNames.length; i++) {
				if (imageNames[i] != "") {
					htmlStr += "<img class=\"image_class\" id=\"hiddenImageDiv\" alt=\"\" src=\"../flc/getcaffile?fileName="
							+ imageNames[i]
							+ "&timestamp="
							+ timestamp
							+ "\"> <br>";
				}
			}
			$("#currentImgDiv").empty();

			$("#rotateFlcHiddendiv").html(htmlStr);
			$(".image_class").bind("load", function() {
				$("#currentImgDiv").html($("#rotateFlcHiddendiv").html());
				$("#rotateFlcHiddendiv").html("");
			});
			
			jQuery.ajaxSetup({
				async : true
			});
		},
		
		//for slc image rotate
		getRotatePageSlc : function() {
			jQuery.ajaxSetup({
				async : false
			});						
			var rotateVO = $('form#rotatecafslc').serializeObject();
			$.postJSON("../imageController/rotateImage", rotateVO, function(
					data) {
				var response = data;
				if (response.successful) {
					var location = $("#cafFileLocation").val();
					$.post("../imageController/getcafimagenames", $.param({cafFileLocation : location}), function(data) {
						var serviceResponse = data;
						if (serviceResponse.successful) {
							$.PageRotation.reloadRotatedImagesSlc(serviceResponse);
						}
					});
				} 
			});
		},
		
		reloadRotatedImagesSlc : function(serviceResponse) {
			var imageNames = null;
			imageNames = serviceResponse.result.split(";");
			$("#cafImages").empty();
			var htmlStr = "";
			var timestamp = new Date().getTime();

			for ( var i = 1; i < imageNames.length; i++) {
				if (imageNames[i] != "") {
					htmlStr += "<img class=\"image_class\" id=\"imageDiv\" alt=\"\" src=\"../flc/getcaffile?fileName="
							+ imageNames[i]
							+ "&timestamp="
							+ timestamp
							+ "\" style='width:100%'> <br>";
				}
			}
			$("#rotateSlcHiddendiv").html(htmlStr);
			$(".image_class").bind("load", function() {
				$("#cafImages").html($("#rotateSlcHiddendiv").html());
				$("#rotateSlcHiddendiv").html("");
			});
		},

		// for audit image rotate
		getRotatePageAudit : function() {
			jQuery.ajaxSetup({
				async : false
			});
			var rotateVO = $('form#rotateCafImageAudit').serializeObject();

			$.postJSON("../imageController/rotateImage", rotateVO, function(
					data) {
				var response = data;
				if (response.successful) {
					var location = $("#cafFileLocation1").val();

					$.post("../imageController/getcafimagenames", $.param({cafFileLocation : location}), function(data) {
						var serviceResponse = data;
						if (serviceResponse.successful) {
							$.PageRotation.reloadRotatedImagesAudit(serviceResponse);
						}
					});
				} 
			});
		},
		
		reloadRotatedImagesAudit : function(serviceResponse) {
			var imageNames = null;
			imageNames = serviceResponse.result.split(";");
			$("#cafImages1").empty();
			var htmlStr = "";
			var timestamp = new Date().getTime();

			for ( var i = 1; i < imageNames.length; i++) {
				if (imageNames[i] != "") {
					htmlStr += "<img class=\"image_class\" id=\"hiddenImageDiv\" alt=\"\" src=\"../flc/getcaffile?fileName="
							+ imageNames[i]
							+ "&timestamp="
							+ timestamp
							+ "\" style='width:100%'> <br>";
				}
			}

			$("#rotateAuditHiddendiv").html(htmlStr);
			$(".image_class").bind("load", function() {
				$("#cafImages1").html($("#rotateAuditHiddendiv").html());
				$("#rotateAuditHiddendiv").html("");
			});
		}
	};

	rotateFlcCafImage = function() {
		$.PageRotation.getRotatePageFlc();
	};

	rotateSlcCafImage = function() {
		$.PageRotation.getRotatePageSlc();
	};

	rotateAuditCafImage = function() {
		$.PageRotation.getRotatePageAudit();
	};

})(jQuery);