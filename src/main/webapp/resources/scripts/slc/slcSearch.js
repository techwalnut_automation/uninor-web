//var slcremak = "NOT_SELECTED"; 

var states;
var cities;
var districts;
var cityresponse = 0;
var distresponse = 0;
var noOfPages = 0;
var mdnbeforeChange = null;
var activationdate = null;
var oldDataEntryVO = null;
var placeholder = "" ;
var rejectReasons=[];
$(document).ready(function(){
		
	$("#rejectOpt2").hide();
	$("#rejectOpt3").hide();
	$("#rejectOpt4").hide();
	$("#rejectOpt5").hide();
	$("#rejectOpt6").hide();
	$("#rejectOpt7").hide();
	$("#rejectOpt8").hide();
	$("#rejectOpt9").hide();
	$("#rejectOpt10").hide();
	$("#rejectOpt11").hide();
	$("#rejectOpt12").hide();
	
	$("#speedocId").focus();
	
	$("#rejectDone").hide();
	$("#rejectDone").button();
	
	$("#photoComplianceBtn").click(function(){
		var photoCom = $("#photoComplianceBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt1").html(data);
			setSelected("#rejectOpt1");
			$("#rejectOpt1").show();
			$("#rejectOpt2").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#documentComBtn").click(function(){
		var docu = $("#documentComBtn").val();
		$.post("../slc/getrejectreason?header="+docu,function(data){
			$("#rejectOpt2").html(data);
			setSelected("#rejectOpt2");
			$("#rejectOpt2").show();
			$("#rejectOpt1").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#cafComBtn").click(function(){
		var photoCom = $("#cafComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt3").html(data);
			setSelected("#rejectOpt3");
			$("#rejectOpt3").show();
			$("#rejectOpt1").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#customerComBtn").click(function(){
		var photoCom = $("#customerComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt4").html(data);
			setSelected("#rejectOpt4");
			$("#rejectOpt4").show();		
			$("#rejectOpt1").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#caoCombtn").click(function(){
		var photoCom = $("#caoCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt5").html(data);
			setSelected("#rejectOpt5");
			$("#rejectOpt5").show();	
			$("#rejectOpt1").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#retailerComBtn").click(function(){
		var photoCom = $("#retailerComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt6").html(data);
			setSelected("#rejectOpt6");
			$("#rejectOpt6").show();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});	
	});
	
	$("#distributorComBtn").click(function(){
		var photoCom = $("#distributorComBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt7").html(data);
			setSelected("#rejectOpt7");
			$("#rejectOpt7").show();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});	
	});
	
	$("#overwritingBtn").click(function(){
		var photoCom = $("#overwritingBtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt8").html(data);
			setSelected("#rejectOpt8");
			$("#rejectOpt8").show();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#outstationCombtn").click(function(){
		var photoCom = $("#outstationCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt9").html(data);
			setSelected("#rejectOpt9");
			$("#rejectOpt9").show();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt10").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#mnpCombtn").click(function(){
		var photoCom = $("#mnpCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){
			$("#rejectOpt10").html(data);
			setSelected("#rejectOpt10");
			$("#rejectOpt10").show();
			$("#rejectOpt9").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt11").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});
	
	$("#databaseCombtn").click(function(){
		var photoCom = $("#databaseCombtn").val();
		$.post("../slc/getrejectreason?header="+photoCom,function(data){		
			$("#rejectOpt11").html(data);
			setSelected("#rejectOpt11");
			$("#rejectOpt11").show();
			$("#rejectOpt10").hide();
			$("#rejectOpt9").hide();
			$("#rejectOpt8").hide();
			$("#rejectOpt7").hide();
			$("#rejectOpt6").hide();
			$("#rejectOpt5").hide();
			$("#rejectOpt4").hide();
			$("#rejectOpt3").hide();
			$("#rejectOpt2").hide();
			$("#rejectOpt1").hide();
			$("#rejectOpt12").hide();
			$("#rejectDone").show();
		});
	});	
	
	$("#rejectDone").click(function(){		
		if( rejectReasons.length != 0){
			var formObject = $("#myForm").serializeObject();
			formObject.dob = saveDateFormate(formObject.dob);
			formObject.poaIssueDate = saveDateFormate(formObject.poaIssueDate);
			formObject.poiIssueDate = saveDateFormate(formObject.poiIssueDate);
			formObject.activationDate = saveDateFormate(formObject.activationDate);	
			formObject.visaValidTill = saveDateFormate(formObject.visaValidTill);
			var speedoc = formObject.speedocId;
	
					$.post("../slc/searchrenamereject?slcremak="+rejectReasons, formObject , function(data){
						var response = data;
						alert(response.successful);
						if(response.successful){
							jAlert("Caf Rejected Successfully.",'Scrutiny',function(){
								$("#rejectOpt1").hide();
								$("#rejectOpt2").hide();
								$("#rejectOpt3").hide();
								$("#rejectOpt4").hide();
								$("#rejectOpt5").hide();
								$("#rejectOpt6").hide();
								$("#rejectOpt7").hide();
								$("#rejectOpt8").hide();
								$("#rejectOpt9").hide();
								$("#rejectOpt10").hide();
								$("#rejectOpt11").hide();
								$("#rejectOpt12").hide();
								$("#rejectDone").hide();
								window.close();
							});
							
						}else{
							jAlert("Dataentry Not Completed.","Scrutiny",function(){
								$("#rejectOpt1").hide();
								$("#rejectOpt2").hide();
								$("#rejectOpt3").hide();
								$("#rejectOpt4").hide();
								$("#rejectOpt5").hide();
								$("#rejectOpt6").hide();
								$("#rejectOpt7").hide();
								$("#rejectOpt8").hide();
								$("#rejectOpt9").hide();
								$("#rejectOpt10").hide();
								$("#rejectOpt11").hide();
								$("#rejectOpt12").hide();
								window.close();
							});
						}
				 	});	
		}else{
			$("#rejectDone").blur();
			jAlert("Reject reason mandatory. Please select whichever is applicable.","Data Entry",function(){
				$("#rejectDone").focus();
			});
		}			
	 });
	
	// save data
	$("#slcsave").click(function(){
		var formObject = $("#myForm").serializeObject();
		formObject.dob = saveDateFormate(formObject.dob);
		formObject.poaIssueDate = saveDateFormate(formObject.poaIssueDate);
		formObject.poiIssueDate = saveDateFormate(formObject.poiIssueDate);	
		formObject.activationDate = saveDateFormate(formObject.activationDate);	
		formObject.visaValidTill = saveDateFormate(formObject.visaValidTill);	
		
		var speedoc = formObject.speedocId;

				$.post("../slc/searchSave",formObject,function(data) {
					var sr = data; 
					if(sr.successful){
						jAlert("Caf updated successfully",function(){
							window.close();
						});
					}else{
						jAlert("Dataentry Not Completed.",'Scrutiny',function(){
							window.close();
						});
					}
				}) ;	
	});
	
	// cancel
	$("#cancelBut").click(function(){
				var cafId  = document.getElementById("id").value;			
				if( cafId != '' && cafId != null){
					jQuery.ajaxSetup({async:false});
						$.get("../slc/cancelCaf?cafId="+ $("#id").val() , function(){
							document.getElementById("loadHome").submit();
						});
						window.close();
				}
	});
	
	// added on 13-04-2015	for rotation
	$("#slcImageRotateBtn").click(function(){			
		rotateSlcCafImage();
	});
	
}) ;
var rejectReasons=[];
function addRejectReasons(value, ele){ 
	if (ele.checked) {
		rejectReasons.push(value);
	 }else{
		 var reasons1 = [];
		 for(var i=0; i < rejectReasons.length; i++){
			 if(rejectReasons[i] != value){
				 reasons1.push(rejectReasons[i]);
			 } 
		 }
		 rejectReasons = reasons1;
	 }
}

function setSelected(id){ 
	$(id + " input[type=checkbox]").each(
		    function() {
		    	 for(var i=0; i < rejectReasons.length; i++){
					 if(rejectReasons[i] == this.value){
						 this.checked = true;
					 } 
				 }
		    }
		);
} 

function saveDateFormate(date){
	if(date != null && date != ""){ 
		var d = date.split("-") ;
		var formateDate = d[2]+"-"+d[1]+"-"+d[0]+" "+"00:"+"00:"+"00";
		return formateDate ;
	}
}