$(document).ready(function(){
$.REPipe = {
		currentCaf : null,
		pipedCaf : null,
		imageCount : 1,
		dob : null,
		activationDate : null,
		poaIssueDate : null,
		poiIssueDate : null,
		init : function() {
			$.REPipe.imageCount = 1;
		},
		getNextCaf : function() {

			if ($.REPipe.currentCaf == null) {
				$.REPipe.loadNextCafAndImage();
			} else {
				$.REPipe.currentCaf = null;
				$.REPipe.pipedImagesLoaded();
			}
		},
		getCafImages : function(cafFlcVo) {
			
			cafFlcVo.cafDate = $.REPipe.stringToTimestamp(cafFlcVo.cafDate) ;
			cafFlcVo.createdDate = $.REPipe.stringToTimestamp(cafFlcVo.createdDate) ;
			
			$.REPipe.dob = cafFlcVo.dob;
			cafFlcVo.dob = $.REPipe.stringToTimestamp(cafFlcVo.dob) ;
			
			cafFlcVo.modifiedDate = $.REPipe.stringToTimestamp(cafFlcVo.modifiedDate) ;
			
			$.REPipe.poaIssueDate = cafFlcVo.poaIssueDate;
			cafFlcVo.poaIssueDate = $.REPipe.stringToTimestamp(cafFlcVo.poaIssueDate) ;
			
			$.REPipe.poiIssueDate = cafFlcVo.poiIssueDate;
			cafFlcVo.poiIssueDate = $.REPipe.stringToTimestamp(cafFlcVo.poiIssueDate) ;
			
			cafFlcVo.scannedDate = $.REPipe.stringToTimestamp(cafFlcVo.scannedDate) ;
			cafFlcVo.visaValidTill = $.REPipe.stringToTimestamp(cafFlcVo.visaValidTill) ;
			
			$.REPipe.activationDate = cafFlcVo.activationDate;
			cafFlcVo.activationDate = $.REPipe.stringToTimestamp(cafFlcVo.activationDate) ;
			
			$.post("../slc/getcafimagenames",cafFlcVo,function(data) {
								var sr = data;
								if (sr.successful) {
									var imageNames = null;
									imageNames = sr.result.split(";");
									$.REPipe.pipedCaf.images = imageNames;
									var htmlStr = "";
									var timestamp = new Date().getTime();

									var indivWidth = "100%";
									var indivHeight = "100%";
//									htmlStr = "<div id=\"highlighter\">&nbsp;</div><div id =\"indiv\" style=\"width:"
//											+ indivWidth
//											+ ";height:"
//											+ indivHeight
//											+ ";position:relative;top:-19px\">";

//									$("#lblNoOfPage").html(
//											imageNames.length - 1);
//									document.getElementById("lblNoOfPage").value= imageNames.length-1 ;
									for (var i = 0; i < imageNames.length; i++) {
										if (imageNames[i] != "") {
											htmlStr += "<img class=\"image_class\" id=\"imageDiv\" alt=\"\" src=\"../flc/getcaffile?fileName="
													+ imageNames[i]
													+ "&timestamp="
													+ timestamp
													+ "\" style='width:100%'> <br>";	
											
										}
									}
//									htmlStr += "</div>";

									$("#hiddenImageDiv").html(htmlStr);
									$(".image_class").bind("load", function() {
										$.REPipe.pipedImagesLoaded();
									});
								}
							});
		},
		pipedImagesLoaded : function() {
			//$("#msgDiv").html("Image Count : " + $.REPipe.imageCount );
			if ($.REPipe.pipedCaf != null) {
//				if ($.REPipe.imageCount == $.REPipe.pipedCaf.pages) {
					$.REPipe.imageCount = 0;
					 if ($.REPipe.currentCaf == null) { 
						if ($.REPipe.pipedCaf != null) {
							$("#cafImages").html($("#hiddenImageDiv").html());
							$("#hiddenImageDiv").html("");
							$.REPipe.currentCaf = $.REPipe.pipedCaf;
							if($.REPipe.activationDate == null){
								$.REPipe.currentCaf.activationDate = null;
							}
							if($.REPipe.dob == null){
								$.REPipe.currentCaf.dob = null;
							}
							if($.REPipe.poaIssueDate == null){
								$.REPipe.currentCaf.poaIssueDate = null;
							}
							if($.REPipe.poiIssueDate == null){
								$.REPipe.currentCaf.poiIssueDate = null;
							}
							fillForm1($.REPipe.currentCaf);
							$.REPipe.loadNextCafAndImage();
						} else {
							$.REPipe.cafNotFound();
						}
					}
//				} else {
//					$.REPipe.imageCount = $.REPipe.imageCount + 1;
//				}
			} else {
				if($.REPipe.currentCaf == null){
					$.REPipe.cafNotFound();
				}
			}
		},
		loadNextCafAndImage : function() {
			$.post("../slc/getslcdata", null,function(data) {
				var serviceResponse = data;
				$.REPipe.pipedCaf = serviceResponse.result;
				if (serviceResponse.successful) {
					$.REPipe.getCafImages($.REPipe.pipedCaf);
				} else {
					$.REPipe.pipedCaf = null;
					if($.REPipe.currentCaf == null){
						$.REPipe.cafNotFound();
					}
				}
			});
		},
		cafNotFound : function(cafId) {
			$("#cafImages").html("");
			jAlert("CAF is not available for scrutiny.","Data Entry",function(){
				
			});
		},
		reCancel : function() {
			var ccafId = "";
			var cnextCafId = "";
			if($.REPipe.pipedCaf != null){
				cnextCafId = $.REPipe.pipedCaf.cafId;
			}
			if ($.REPipe.currentCaf != null) {
				ccafId = $.REPipe.currentCaf.cafId;
			}

			var url = "../slc/cancelremcaf?cafId=" + ccafId
					+ "&nextCafId=" + cnextCafId;
			jQuery.ajaxSetup({async:false});
			$.post(url, null, function(data) {
				
			});
			window.location="../slc/cancelvrcaf";
			jQuery.ajaxSetup({async:true});
		},
		stringToTimestamp : function(data){
			if(data === null || data === ""){
				data = new Date();
			}else{
				var mydate = moment.utc(data).toDate() ;
				var d =  new Date(mydate) ;
				var formateDate = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate()+" "+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
				return formateDate ;
			}
		},
		setLeftTop : function(eleId) {       // added on 13-10-2014
			// alert(""+cafElementList.result.length)
			var left;
			var top;
			for ( var i = 0; i < cafElementListHorizontal.result.length; i++) {
				// alert("cafElementList.result[i].deElementId
				// "+cafElementListHorizontal.result[i].deElementId);
				if (cafElementListHorizontal.result[i].deElementId == eleId) {
					left = cafElementListHorizontal.result[i].leftPos;
					top = cafElementListHorizontal.result[i].topPos;
					/*alert("LEFT: "+left+" TOP:"+top +"eleId"
					+eleId);*/
					if (eleId == "photograph") {
						alert("hi");
						$("#highlighter").css({
							'left' : left,
							'top' : top,
							'width' : '220px',
							'height' : '320px',
							'border' : '0px solid red'
						});

						$("#indiv").css({
							'top' : '-340px'
						});
						$("#highlighter").scrollintoview();
					} else {
						//alert("element :  "+eleId+"\nLeft : "+left+"\ntop : "+top);
						var width = '600px';
						var height = '120px';
						if (eleId == "firstName" || eleId == "middleName" || eleId == "lastName"
							|| eleId == "fatherFirstName" || eleId == "fatherMiddleName" || eleId == "fatherLastName"){
							width = '850px';
							height = '80px';
						}
						if (eleId == "simNumber" || eleId == "mobileNumber"  || eleId == "dob"   
							|| eleId == "nationality" ||  eleId == "imsiNumber"){
							width = '400px';
							height = '60px';
						}

						$("#highlighter").css({
							'left' : left,
							'top' : top,
							'width' : width,
							'height' : height,
							'border' : '0px solid red',
							'border-top' : '0px solid red',
							'border-bottom' : '0px solid red'
						});
						$("#indiv").css({
							'top' : '-101px'
						});

						$("#highlighter").scrollintoview();
					}
				}
			}			
		}
	} ;

	rePipeNextCaf = function() {
		$.REPipe.getNextCaf();
	} ;

	rePipeCancel = function() {
		$.REPipe.reCancel();
	} ;
	
	REPipeSetLeftTop = function(focusId) {
		$.REPipe.setLeftTop(focusId);
	}
	
	$(".focusH").focus(function(event){
//		alert("focusId : " +event.target.id);
		var focusId = event.target.id;
		REPipeSetLeftTop(focusId);
		event.preventDefault();
	});
});
