
var cafElementListHorizontal = {
	"errorCode" : null,
	"message" : null,
	"result" : [ {
		"cafEleId" : null,
		"cafElement" : "physicalCafNumber",
		"cafTypeId" : "25",
		"deElementId" : "physicalCafNumber",
		"leftPos" : "0.00",

		"topPos" : "220.0"
		},{
		"cafEleId" : null,
		"cafElement" : "MNP",
		"cafTypeId" : "25",
		"deElementId" : "mnp",
		"leftPos" : "0.00",

		"topPos" : "220.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Existing number",
		"cafTypeId" : "25",
		"deElementId" : "existingNumber",
		"leftPos" : "0.0",

		"topPos" : "220.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "CAF Number",
		"cafTypeId" : "25",
		"deElementId" : "cafNumber",
		"leftPos" : "0.0",

		"topPos" : "220.0"
	}, 	{
		"cafEleId" : null,
		"cafElement" : "Speedoc Id",
		"cafTypeId" : "25",
		"deElementId" : "speedocId1",
		"leftPos" : "100.0",

		"topPos" : "120.0"
	},	{
		"cafEleId" : null,
		"cafElement" : "Cell Number",
		"cafTypeId" : "25",
		"deElementId" : "mobileNumber",
		"leftPos" : "950.0",
		"topPos" : "80.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "SIM Number",
		"cafTypeId" : "25",
		"deElementId" : "simNumber",
		"leftPos" : "950.0",
		"topPos" : "20.0"
	},	{
		"cafEleId" : null,
		"cafElement" : "IMSI Number",
		"cafTypeId" : "25",
		"deElementId" : "imsiNumber",
		"leftPos" : "950.0",
		"topPos" : "140.0"
	},  {
		"cafEleId" : null,
		"cafElement" : "Photograph",
		"cafTypeId" : "25",
		"deElementId" : "photograph",
		"leftPos" : "220.0",

		"topPos" : "00.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "First Name",
		"cafTypeId" : "25",
		"deElementId" : "firstName",
		"leftPos" : "350.0",
		"topPos" : "350.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Middle Name",
		"cafTypeId" : "25",
		"deElementId" : "middleName",
		"leftPos" : "350.0",
		"topPos" : "350.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Last Name",
		"cafTypeId" : "25",
		"deElementId" : "lastName",
		"leftPos" : "350.0",
		"topPos" : "350.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Gender",
		"cafTypeId" : "25",
		"deElementId" : "gender",
		"leftPos" : "300.0",
		"topPos" : "450.0"
	},{
		"cafEleId" : null,
		"cafElement" : "DOB",
		"cafTypeId" : "25",
		"deElementId" : "dobddmmyyyy",
		"leftPos" : "0.0",
		"topPos" : "270.0"
		
	}, {
		"cafEleId" : null,
		"cafElement" : "DOB",
		"cafTypeId" : "25",
		"deElementId" : "dob",
		"leftPos" : "450.0",
		"topPos" : "450.0"
	},	{
		"cafEleId" : null,

		"cafElement" : "Nationality",
		"cafTypeId" : "25",
		"deElementId" : "nationality",
		"leftPos" : "850.0",
		"topPos" : "450.0"
	}, {
		"cafEleId" : null,

		"cafElement" : "Father First Name",
		"cafTypeId" : "25",
		"deElementId" : "fatherFirstName",
		"leftPos" : "350.0",
		"topPos" : "400.0"
	},

	{
		"cafEleId" : null,
		"cafElement" : "Father Middle Name",
		"cafTypeId" : "25",
		"deElementId" : "fatherMiddleName",
		"leftPos" : "350.0",
		"topPos" : "400.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Father Last Name",
		"cafTypeId" : "25",
		"deElementId" : "fatherLastName",
		"leftPos" : "350.0",
		"topPos" : "400.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Address 1",
		"cafTypeId" : "25",

		"deElementId" : "localAddress",
		"leftPos" : "0.0",
		"topPos" : "645.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Address1 LandMark",
		"cafTypeId" : "25",

		"deElementId" : "localAddressLandmark",
		"leftPos" : "0.0",
		"topPos" : "645.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "City",
		"cafTypeId" : "25",

		"deElementId" : "localCity",
		"leftPos" : "0",
		"topPos" : "645.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Pin Code",

		"cafTypeId" : "25",
		"deElementId" : "localPincode",
		"leftPos" : "0.0",
		"topPos" : "645.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "District",

		"cafTypeId" : "25",
		"deElementId" : "localDistrict",
		"leftPos" : "0.0",
		"topPos" : "645.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "State",

		"cafTypeId" : "25",
		"deElementId" : "localState",
		"leftPos" : "0.0",
		"topPos" : "645.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Alt Mobile Number",

		"cafTypeId" : "25",
		"deElementId" : "alternateMobileNumber",
		"leftPos" : "0.0",
		"topPos" : "865.0"
	}, {
		"cafEleId" : null,

		"cafElement" : "Permanent address",
		"cafTypeId" : "25",
		"deElementId" : "permanentAddress",
		"leftPos" : "0.0",
		"topPos" : "765.0"
	},

	{
		"cafEleId" : null,
		"cafElement" : "Permanent landmark",
		"cafTypeId" : "25",
		"deElementId" : "permanentAddressLandmark",

		"leftPos" : "000.0",
		"topPos" : "765.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Permanent City",
		"cafTypeId" : "25",
		"deElementId" : "permanentCity",

		"leftPos" : "0.0",
		"topPos" : "765.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Permanent PIN Code",
		"cafTypeId" : "25",

		"deElementId" : "permanentPincode",
		"leftPos" : "0.0",
		"topPos" : "765.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Permanent District",

		"cafTypeId" : "25",
		"deElementId" : "permanentDistrict",
		"leftPos" : "00.0",
		"topPos" : "765.0"
	}, {
		"cafEleId" : null,

		"cafElement" : "Permanent State",
		"cafTypeId" : "25",
		"deElementId" : "permanentState",
		"leftPos" : "0.0",
		"topPos" : "765.0"
	},
/*
	{
		"cafEleId" : null,
		"cafElement" : "PAN/GIR No.",
		"cafTypeId" : "25",
		"deElementId" : "panGirNumber",
		"leftPos" : "0.0",
		"topPos" : "895.0"
	},*/

	{
		"cafEleId" : null,
		"cafElement" : "POI",
		"cafTypeId" : "25",
		"deElementId" : "poi",
		"leftPos" : "00.0",
		"topPos" : "1030.0"
	},

	{
		"cafEleId" : null,
		"cafElement" : "POI Ref",
		"cafTypeId" : "25",
		"deElementId" : "poiref",
		"leftPos" : "00.0",
		"topPos" : "1030.0"
	},

	{
		"cafEleId" : null,
		"cafElement" : "POA",
		"cafTypeId" : "25",
		"deElementId" : "poa",
		"leftPos" : "0.0",
		"topPos" : "1060.0"
	},

	{
		"cafEleId" : null,
		"cafElement" : "POA Ref",
		"cafTypeId" : "25",
		"deElementId" : "poaref",
		"leftPos" : "0.0",
		"topPos" : "1060.0"
	},

	{
		"cafEleId" : null,
		"cafElement" : "Foreign National",
		"cafTypeId" : "25",
		"deElementId" : "foreignNational",
		"leftPos" : "0.0",

		"topPos" : "220.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Outstation",
		"cafTypeId" : "25",
		"deElementId" : "outstation",
		"leftPos" : "0.0",

		"topPos" : "220.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Passport No.",
		"cafTypeId" : "25",
		"deElementId" : "passportNumber",

		"leftPos" : "00.0",
		"topPos" : "1130.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Type of visa",
		"cafTypeId" : "25",

		"deElementId" : "typeOfVisa",
		"leftPos" : "00.0",
		"topPos" : "1130.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Visa Valid Date",

		"cafTypeId" : "25",
		"deElementId" : "visaValidTillddmmyyyy",
		"leftPos" : "00.0",
		"topPos" : "1130.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Visa No",

		"cafTypeId" : "25",
		"deElementId" : "visaNumber",
		"leftPos" : "000.0",
		"topPos" : "1130.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "LR First Name",
		"cafTypeId" : "25",
		"deElementId" : "localReferenceName",
		"leftPos" : "00.0",
		"topPos" : "1230.0"
	}, 

	 {
		"cafEleId" : null,
		"cafElement" : "LR Address",
		"cafTypeId" : "25",
		"deElementId" : "localReferenceAddress",
		"leftPos" : "000.0",
		"topPos" : "1230.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "LR City",
		"cafTypeId" : "25",
		"deElementId" : "localReferenceCity",
		"leftPos" : "000.0",
		"topPos" : "1230.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "LR PIN Code",
		"cafTypeId" : "25",
		"deElementId" : "localReferencePincode",
		"leftPos" : "00.0",
		"topPos" : "1230.0"
	}, {
		"cafEleId" : null,
		"cafElement" : "Local Contact Number",
		"cafTypeId" : "25",
		"deElementId" : "localReferenceContactNumber",
		"leftPos" : "0.0",
		"topPos" : "1230.0"
	}, {
		"cafEleId" : null,

		"cafElement" : "Category",
		"cafTypeId" : "25",
		"deElementId" : "category",
		"leftPos" : "0.0",
		"topPos" : "880.0"
	}, {
		"cafEleId" : null,

		"cafElement" : "Form 60/61",
		"cafTypeId" : "25",
		"deElementId" : "form6061",
		"leftPos" : "000.0",
		"topPos" : "900.0"
	}, {
		"cafEleId" : null,

		"cafElement" : "Connection Type",
		"cafTypeId" : "25",
		"deElementId" : "connectionType",
		"leftPos" : "0.0",
		"topPos" : "220.0"
	},	{
		"cafEleId" : null,
		"cafElement" : "Customer Signature",
		"cafTypeId" : "25",
		"deElementId" : "customersignature",
		"leftPos" : "00.0",

		"topPos" : "1900.0"
	},{
		"cafEleId" : null,
		"cafElement" : "GSM",
		"cafTypeId" : "25",
		"deElementId" : "gsm",
		"leftPos" : "00.0",

		"topPos" : "220.0"
	},{
		"cafEleId" : null,
		"cafElement" : "CDMA",
		"cafTypeId" : "25",
		"deElementId" : "cdma",
		"leftPos" : "00.0",

		"topPos" : "220.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POI Issue Place",
		"cafTypeId" : "25",
		"deElementId" : "poiIssuePlace",
		"leftPos" : "00.0",
		"topPos" : "1030.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POI Issuing Authority",
		"cafTypeId" : "25",
		"deElementId" : "poiIssuingAuthority",
		"leftPos" : "00.0",
		"topPos" : "1030.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POI Issue Date",
		"cafTypeId" : "25",
		"deElementId" : "poiIssueDate",
		"leftPos" : "00.0",
		"topPos" : "1030.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POA Issue Date",
		"cafTypeId" : "25",
		"deElementId" : "poaIssueDate",
		"leftPos" : "00.0",
		"topPos" : "1060.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POA Issue Place",
		"cafTypeId" : "25",
		"deElementId" : "poaIssuePlace",
		"leftPos" : "00.0",
		"topPos" : "1060.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POA Issuing Authority",
		"cafTypeId" : "25",
		"deElementId" : "poaIssuingAuthority",
		"leftPos" : "00.0",
		"topPos" : "1060.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POS Code(Retailer)",
		"cafTypeId" : "25",
		"deElementId" : "posCode",
		"leftPos" : "00.0",

		"topPos" : "1000.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POS Address",
		"cafTypeId" : "25",
		"deElementId" : "posAddress",
		"leftPos" : "00.0",

		"topPos" : "1000.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POS State",
		"cafTypeId" : "25",
		"deElementId" : "posState",
		"leftPos" : "00.0",

		"topPos" : "1000.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POS City",
		"cafTypeId" : "25",
		"deElementId" : "posCity",
		"leftPos" : "00.0",

		"topPos" : "1000.0"
	},{
		"cafEleId" : null,
		"cafElement" : "POS Pin Code",
		"cafTypeId" : "25",
		"deElementId" : "posPinCode",
		"leftPos" : "00.0",

		"topPos" : "1000.0"
	} ],
	"successful" : true
};

var cafProperty = {
	"errorCode" : null,
	"message" : null,
	"result" : {
		"cafHeight" : "15",
		"cafType" : "Uninorlegal",
		"cafTypeId" : 25,

		"cafWidth" : "8.3",
		"clipHeight" : "0.25",
		"clipWidth" : "1.4",
		"dpi" : "200",
		"imageHeightMax" : null,
		"imageHeightMin" : null,

		"imageWidthMax" : null,
		"imageWidthMin" : null,
		"leftPos" : "0.0",
		"topPos" : "0.7"
	},
	"successful" : true
};