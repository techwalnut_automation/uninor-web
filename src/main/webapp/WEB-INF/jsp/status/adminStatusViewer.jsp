<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script type="text/javascript">
	$(document).ready(function() {
		$("#searchBtn").button();
		$("#reButton").button();

		$("#fromDateStatus").datepicker({dateFormat: 'dd/mm/yy'}).datepicker("setDate",new Date());
	    $("#toDateStatus").datepicker({dateFormat:'dd/mm/yy'}).datepicker("setDate",new Date());
		
	 		var url = "<c:url value='/statusController/searchStatusViewer' />" ;
			var fd = new FormData(document.getElementById("form1"));	
			$.ajax({
			 	url: url,
			 	data: fd,
			 	dataType: 'text',
			 	processData: false,
			 	contentType: false,
			 	type: 'POST',
			 	success: function(data){
			 	   $('#displayResultStatus').html(data); 	          				 	          				 	        	          					                                
			 	}	    	
			});

			
		$("#fromDateStatus").datepicker({
		      changeMonth: true,
		      numberOfMonths: 1,
		      dateFormat: 'dd/mm/yy',			         
		      onClose: function(selectedDate) {
		        $("#toDateStatus").datepicker("option", "minDate", selectedDate);
		      }
		 });
		 
		 $("#toDateStatus").datepicker({  
		     changeMonth: true,
		     numberOfMonths: 1,
		     dateFormat: 'dd/mm/yy',	
		     onClose: function(selectedDate) {
		       $("#fromDateStatus").datepicker("option", "maxDate", selectedDate+"+1D");
		     }
		  });	
			
		$("#searchBtn").click(function(data){
			if(validateForm()){
				var url = "<c:url value='/statusController/searchStatusViewer' />" ;
				var fd = new FormData(document.getElementById("form1"));	
				$.ajax({
			 		url: url,
			 		data: fd,
			 		dataType: 'text',
			 		processData: false,
			 		contentType: false,
			 		type: 'POST',
			 		success: function(data){
			 	   	$('#displayResultStatus').html(data); 	          				 	          				 	     	          					                                
			 		}	    	
				});
		    }				
		});	

		$("#reButton").click(function(){
			$("#fromDateStatus").val("");
			$("#toDateStatus").val("");
			$("#displayResultStatus").html("");
		});  		  
	});

	function validateForm(){
		var trimFromDate = $.trim($('#fromDateStatus').val());
		var trimToDate = $.trim($('#toDateStatus').val());		
			if(trimFromDate == null || trimFromDate == ""){
				jAlert("Enter From Date.");
				$("#fromDateStatus").focus();
				return false;
			}	
			if(trimToDate == null || trimToDate == ""){
				jAlert("Enter To Date.");
				$("#toDateStatus").focus();
				return false;
			}
			if($('#fromDateStatus').datepicker("getDate") > $('#toDateStatus').datepicker("getDate")) {
	    		jAlert("Please Enter Valid Date Range.");
	    		$("#displayResultStatus").html("");
	    		return false;
	    	}
		return true;
	}
	
</script>

<div id="wrapper">
	<div id="int-container">
		<div class="cbox-cont">
			<div class="content">
				<div id="form">
					<form name="form1" id="form1" method="post" action="${url}">
						<table id="data">
							<tr>
								<td><label style="font-size: 12px;">From Date : </label></td>
								<td><input id="fromDateStatus" type="text" name="fromDate" tabindex="1" style="font-size: 12px;"/></td>
								<td><label style="margin-left: 10px; font-size: 12px;">To Date : </label></td>
								<td><input id="toDateStatus" type="text" name="toDate" tabindex="2" style="font-size: 12px;"/>
								</td>
								<td colspan="3" style="padding-left: 15px">
									<input type="button" value="Search" id="searchBtn" tabindex="3"/>
									<button type="button" id="reButton" tabindex="4">Reset</button>
								</td>
							</tr>							
						</table>
					</form>
					<br>
					<div id="displayResultStatus"></div>
					<!-- End: Scan Status Table -->
				</div>
			</div>

		</div>
	</div>
</div>


