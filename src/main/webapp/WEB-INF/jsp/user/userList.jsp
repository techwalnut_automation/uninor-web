<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<c:url var="imageUrl" value="/${imageUrl}uninor/" />
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-1.10.0.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-migrate-1.1.0.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/jquery-ui-1.9.2.custom.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/scripts/js/json.min.js" />"></script>
<script type="text/javascript" src='<c:url value="/resources/scripts/js/jquery.alerts.js"/>'></script>
<link href="<c:url value="/resources/styles/css/jquery-ui-1.9.2.custom.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/style.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.alerts.css" />" rel="stylesheet" type="text/css" />
<link href="<c:url value="/resources/styles/css/jquery.confirm.css" />" rel="stylesheet" type="text/css" />
<script type="text/javascript">

	function deleteUserById(userId)
	{
		var answer = confirm("Are you sure to delete?");
		if(answer){
		//	$.postJSON("<c:url value='/user/deleteuser?userId=" + userId+"'/>", function(data) {
		//	});
			alert("User deleted.");
		}
	}
	function changeStatus(userId,status)
	{
		//alert("userId "+userId +" status "+status);
		var answer = confirm("Do you want to change the user status?");
		if(answer){
			$.postJSON("<c:url value='/user/changestatus?userId=" + userId+"&status="+status+"'/>", function(data) {
			});
			alert("Status changed.");
		}
		//alert("userId "+userId +" status "+status);
	}
	$(document).ready(function() {
				
		 $.idleTimeout('#idletimeout', '#idletimeout a', {
				idleAfter: 540,
				pollingInterval: 5,
				keepAliveURL: '../keepalive.jsp',
				serverResponseEquals: 'OK',
				onTimeout: function(){
					$(this).slideUp();
					//window.location = "<c:url value='/logoutPage.jsp' />";
					
					window.location ='../logout';
				},
				onIdle: function(){
					$(this).slideDown(); // show the warning bar
				},
				onCountdown: function( counter ){
					$(this).find("span").html( counter ); // update the counter
				},
				onResume: function(){
					$(this).slideUp(); // hide the warning bar
				}
			});
		
	});
	
	
</script>
<style type="text/css">
#idletimeout { /*background:#CC5100;*/ /*border:3px solid #FF6500;*/
	color: #fff;
	font-family: arial, sans-serif;
	text-align:center;
	font-size: 12px;
	padding: 10px;
	position: relative;
	top: 0px;
	left: 0;
	right: 0;
	z-index: 2;
	display: none;
}

#idletimeout a {
	color: #fff;
	font-weight: bold
}

#idletimeout span {
	font-weight: bold
}
</style>
<div>
<div id="idletimeout" style="color: red; ">You will be logged off in <span><!-- countdown place holder --></span>&nbsp;seconds
due to inactivity. <a id="idletimeout-resume" href="#" style="color: red">Click here to continue using this web page</a>.</div>


<div id="wrapper">

<sec:authorize ifAnyGranted="ROLE_ADMIN,ROLE_OPERATOR">


<div id="int-container">
	<div class="cbox-top">
	    <span></span>
	</div>
	<div class="cbox-cont">
		<div class="content-header">
			<table width="100%">
                 <tr>
                     <td class="content-title" align="left">  Users</td>
                     <td class="content-toolbar" align="right">
                         <span><a  href="<c:url value='/userController/saveuserform'/>" class="anchorclass" rel="submenu1" >Create New User</a></span>
                     </td>
                 </tr>
             </table>
        </div>
		<div class="content">
		<div id="form">
			<table width="97%" cellspacing="2" cellpadding="0" border="0" align="center" id="tbl" style="margin-left:20px;">
				<tr align="left" valign="middle" class="tbl-head">
	         		<th class="tbl-top" style="display: none;">Id</th>
					<th class="tbl-top" class="hd">User Name</th>
					<th class="tbl-top" class="hd">First Name</th>
					<th class="tbl-top" class="hd">Middle Name</th>
					<th class="tbl-top" class="hd">Last Name</th>
					<th class="tbl-top">Roles</th>
					<th class="tbl-top" style="width: 100px;">Action</th>
					<th class="tbl-top">Status</th>
				 </tr>
				 <c:forEach items="${userVos}" var="userVo" varStatus="len">
				 	<c:choose>
			           <c:when test="${len.count%2==0}">
			            <tr align="left" valign="middle" class="row-alter">
			            </c:when> 
			            <c:otherwise>
			            <tr align="left" valign="middle" class="row">
			            </c:otherwise>
			        </c:choose>
			        	<td style="display: none;">${userVo.id}</td>
						<td>${userVo.username}</td>
						<td>${userVo.firstName}</td>
						<td>${userVo.middleName}</td>
						<td>${userVo.lastName}</td>
						<td>${fn:join(userVo.roles,",")}</td>
						<td> <a href="" onclick="deleteUserById( ${userVo.id})">[ Delete ]</a></td>
						<td>${userVo.status} &nbsp;&nbsp; <a href="" onclick="changeStatus( ${userVo.id},'${userVo.status}')" >[Active/Deactive]</a></td>
				    </tr>
				</c:forEach>
			</table><!-- End: Scan Status Table -->  
		</div>
        <div class="cbox-bottom">
            <span></span>
        </div>
		</div>
        <!-- End: Container -->
	</div>
</div>
<div id="int-bottom"><span></span></div><!-- End: Content Bottom -->
</sec:authorize>
</div>
