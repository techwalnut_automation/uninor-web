<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript">
$(document).ready(function(){
	$("#goBtn").button();
	
	$('#month').datepicker({dateFormat:"MM yy"}).datepicker("setDate",new Date());
	var month=$("#month").val().trim();
	month = month.split(' ').join(' ') ;

	$.get("../avamController/getgridpage/"+month,function(data){			 
		$("#result1").html(data);			 
 	});
 		
	$('#month').datepicker({
	     changeMonth: true,
	     changeYear: true,
	     dateFormat: 'MM yy',
	       
	     onClose: function() {
	        var iMonth = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
	        var iYear = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
	        $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	     },
	       
	     beforeShow: function() {
	       if ((selDate = $(this).val()).length > 0) 
	       {
	          iYear = selDate.substring(selDate.length - 4, selDate.length);
	          iMonth = jQuery.inArray(selDate.substring(0, selDate.length - 5), $(this).datepicker('option', 'monthNames'));
	          $(this).datepicker('option', 'defaultDate', new Date(iYear, iMonth, 1));
	           $(this).datepicker('setDate', new Date(iYear, iMonth, 1));
	       }
	    }
	  });

	 	$("#goBtn").click(function(){	    		    		 	    
	     	$.get("../avamController/getgridpage/"+month,function(data){			 
				$("#result1").html(data);			 
		 	});	 	    		    		    	    			    		   		    		
	 	});	   
});


</script>

	<div>		
		<form id="avamForm">
			<label>Month :</label>
			<input type="text" id="month" name="month"/>
			<button type="button" id="goBtn">Go</button>
		</form>
		<div id="result1" style="padding-top: 15px; text-align: center;"> </div>
	</div>
		
