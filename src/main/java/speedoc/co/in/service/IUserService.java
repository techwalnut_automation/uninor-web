package speedoc.co.in.service;

import java.util.List;

import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;

public interface IUserService {
	public ServiceResponse<UserVo> findById(Long userId) ;

	public ServiceResponse<UserVo> findByUsername(String userName) throws Exception;

	public UserVo findByUniqueUserName(String username);

	public void saveOrUpdate(UserVo userVo,String prevOption);

	public RoleVo getAllRolesNameByUserId(Long userId);
	
	public ServiceResponse<List<UserVo>> getAllUser() ;
	
	public ServiceResponse<GridDataVo<List<UserVo>>> getUsersByRowsCount(FilterVo<UserVo> filterVo)  ;

	public boolean getExistingUserName(String username);

	public ServiceResponse<GridDataVo<List<UserVo>>> getUserList(
			FilterVo<UserVo> filterVo);

}
