package speedoc.co.in.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.VendorManager;
import speedoc.co.in.vo.VendorUserVo;
import speedoc.co.in.vo.VendorVo;

@Service
public class VendorService implements IVendorService{
	
	@Autowired
	VendorManager vendorManager;
	
	@Override
	public VendorVo addVendor(VendorVo vendorVo) {
		return vendorManager.addVendor(vendorVo);
	}
	
	@Override
	public VendorUserVo addVendorUser(VendorUserVo vendorUserVo) throws Exception {
		return vendorManager.addVendorUser(vendorUserVo);
	}
	
}
