package speedoc.co.in.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.CafImageManager;

@Service
public class CafImageService implements ICafImageService{
	
	
	@Autowired
	private CafImageManager cafImageManager;
	
	public String getImageRotatePdf(String fileName, int pageNo){
		return cafImageManager.getImageRotatePdf(fileName, pageNo); 
	}
}
