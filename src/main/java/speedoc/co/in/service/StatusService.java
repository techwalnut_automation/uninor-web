package speedoc.co.in.service;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.StatusManager;
import speedoc.co.in.vo.StatusViewerVo;

@Service
public class StatusService implements IStatusService{
	
	@Autowired
	StatusManager statusManager;
	
	private static Logger logger=Logger.getLogger(StatusService.class);

	@Override
	public ServiceResponse<List<StatusViewerVo>> getCountbyAdmin(String fromdate,
			String todate, String username) {
		logger.info("fromdate :: "+fromdate+" todate :: "+todate);
		List<StatusViewerVo> list = statusManager.getCountbyAdmin(fromdate, todate, username);
		logger.info("list :: "+list.size());
		ServiceResponse<List<StatusViewerVo>> serviceResponse = new ServiceResponse<List<StatusViewerVo>>();
		serviceResponse.setResult(list);		
		return serviceResponse;
	}

	@Override
	public File getFtpReport(Timestamp fromDate, Timestamp toDate) {
		return statusManager.getFtpReport(fromDate,toDate);	 
	}

	@Override
	public File getIndexingData(String fromDate, String toDate) {
		return statusManager.getIndexingData(fromDate,toDate);	
	}

	@Override
	public File getScruitinyRejectData(String fromDate, String toDate) {
		return statusManager.getScruitinyRejectData(fromDate,toDate);	
	}
}
