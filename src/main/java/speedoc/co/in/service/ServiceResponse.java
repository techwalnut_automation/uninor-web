package speedoc.co.in.service;
import java.io.Serializable;

/**
 * @author DileepS
 *	This is generic class used for sending response to controller 
 * @param <T> accepts any class for which you want to create response object 
 */
public class ServiceResponse <T> implements Serializable{

	private static final long serialVersionUID = 1L;
	private T result;
	private boolean isSuccessful;
	private String message;
	private String errorCode;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public T getResult() {
		return result;
	}

	public void setResult(T result) {
		this.result = result;
	}

	public boolean isSuccessful() {
		return isSuccessful;
	}

	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	
	
}
