package speedoc.co.in.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import speedoc.co.in.manager.IndexingManager;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.IndexingVo;
import speedoc.co.in.vo.ScrutinyCaptureVo;

@Service
public class IndexingService {
	
	@Autowired
	IndexingManager indexingManager;
	
	private static Logger log = Logger.getLogger(IndexingService.class.getName());

	public Integer saveIndexData(IndexingVo indexingVo) {
		Integer resultcode = indexingManager.saveIndexData(indexingVo);
		log.info("IndexingService :: resultcode :: "+resultcode);
		return resultcode;
	}

	public List<ScrutinyCaptureVo> getScrutinyCompletedRecords() {
		return indexingManager.getScrutinyCompletedRecords();
	}

	public String setIndexingCompleted(ScrutinyCaptureVo scrutinyCaptureVO) {
		return indexingManager.setIndexingCompleted(scrutinyCaptureVO);
	}

	public ArrayList<String> getUserToAllot() {
		return indexingManager.getUserToAllot();
	}
	
	public Integer updateIndexingStatus(){
		Integer updateStatus = indexingManager.updateIndexingStatus();
		return updateStatus;	
	}

	public IndexingVo getElectroData() {
		return indexingManager.getElectroData();
	}

	public Integer saveElectroData(IndexingDataVO indexingDataVO) {
		Integer resultCode=indexingManager.saveElectroData(indexingDataVO);
		return resultCode;
	}
}