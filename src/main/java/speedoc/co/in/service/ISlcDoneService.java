package speedoc.co.in.service;

import java.util.List;

import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.SlcDoneFileVo;

public interface ISlcDoneService {

	ServiceResponse<GridDataVo<List<SlcDoneFileVo>>> getSlcDoneList(
			FilterVo<SlcDoneFileVo> filterVo, String month);
   
}
