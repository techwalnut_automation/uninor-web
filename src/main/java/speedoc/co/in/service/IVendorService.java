package speedoc.co.in.service;

import speedoc.co.in.vo.VendorUserVo;
import speedoc.co.in.vo.VendorVo;

public interface IVendorService {
	public VendorVo addVendor(VendorVo vendorVo);
	public VendorUserVo addVendorUser(VendorUserVo vendorUserVo) throws Exception;
}
