package speedoc.co.in.service;

import java.util.List;
import java.io.File;
import java.util.Collection;
import java.util.Iterator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import speedoc.co.in.manager.AvamManager;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.AvamImportVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.util.CAFUtils;

@Service
public class AvamService {
	
	private static Logger logger=Logger.getLogger(AvamService.class);
	
	@Value("${AVAM_INPROCESS_DATA_PATH}")
	private String avamInprocessDataPath;

	@Value("${AVAM_COMPLETE_DATA_PATH}")
	private String avamCompletedDataPath;
	
	@Autowired
	AvamManager avamManager;

	@SuppressWarnings("rawtypes")
	public boolean readExcelData() {
		logger.info("inside reading data from schedular of readExcelData...");
		Collection<File> avamFiles = FileUtils.listFiles(new File(avamInprocessDataPath), new String[]{"xls","xlsx"}, true);
		logger.info("Total file found = " + avamFiles.size());
		for (Iterator iterator = avamFiles.iterator(); iterator.hasNext();) {
			File file = (File) iterator.next();
			boolean readExcelData = avamManager.readExcelData(file.getPath());
			try {
				if(readExcelData == true){
					CAFUtils.moveFile1(file.getPath(), avamCompletedDataPath + File.separator + FilenameUtils.getName(file.getPath()));
				}				
			} catch (Exception e) {
				logger.error(e);
			}
		}
		return true;
	}

	public ServiceResponse<GridDataVo<List<AvamImportVo>>> getAvamMonthWiseList(
			FilterVo<AvamImportVo> filterVo, String month) {
		ServiceResponse<GridDataVo<List<AvamImportVo>>> serviceResponse = new ServiceResponse<GridDataVo<List<AvamImportVo>>>();
		GridDataVo<List<AvamImportVo>> gridDataVo = new GridDataVo<List<AvamImportVo>>();
		List<AvamImportVo> avamImportVos = avamManager.getAvamMonthWiseList(filterVo,month);
		gridDataVo.setResult(avamImportVos);	
		Long count = avamManager.getAvamCount();
		gridDataVo.setRecords(count.toString());
		if (filterVo.getRowsPerPage() == avamImportVos.size()) {
			gridDataVo.setHasMorePages(true);
		} else {
			gridDataVo.setHasMorePages(false);
		}
		gridDataVo.setMaxPageNumber((int)Math.floor(count/filterVo.getRowsPerPage()+ (count%filterVo.getRowsPerPage() >= 0 ? 1 : 0)));
		gridDataVo.setMaxRowCount(count);
		serviceResponse.setResponse(gridDataVo);
		serviceResponse.setSuccessful(true);
		return serviceResponse;
	}
}
