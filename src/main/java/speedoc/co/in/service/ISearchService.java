package speedoc.co.in.service;

import java.util.List;

import speedoc.co.in.vo.SearchResultVo;

public interface ISearchService {
	List<SearchResultVo> getSearchDataByMobileNumber(String mobileNumber);
	List<SearchResultVo> getSearchDataBySpeedocId(String speedocId);
}
