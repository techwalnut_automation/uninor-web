package speedoc.co.in.service;

import java.io.OutputStream;
import java.util.List;

import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

public interface IFlcService {
	public ServiceResponse<CafFlcVo> getUnknownCaf(UserVo userVo) ;
	public ServiceResponse<String> getCafImageNames(String cafFileName)  ;
	public void getCafFile(String fileName, OutputStream out)  ;
	public ServiceResponse<CafFlcVo>  save(CafFlcVo cafFlcVo, UserVo userVo) ;
	public ServiceResponse<String> rejectFlc(String remark,CafFlcVo cafFlcVo, UserVo userVo)  ;
	public ServiceResponse<String> skipFlc(CafFlcVo cafFlcVo, UserVo userVo)  ;
	public ServiceResponse<List<RemarkVo>> getRejectReasons() ;
	public boolean checkSpeedocIdExist(String speedocId);
	public void updateCafTempPool(UserVo userVo);
	public Boolean updateCafStatus(UserVo userVo);
}
