package speedoc.co.in.service;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;
import speedoc.co.in.vo.StatusViewerVo;

public interface IStatusService {

	ServiceResponse<List<StatusViewerVo>> getCountbyAdmin(String string,
			String string2, String name);

	File getFtpReport(Timestamp timestamp, Timestamp timestamp2);

	File getIndexingData(String fromDate, String toDate);
	
	File getScruitinyRejectData(String fromDate, String toDate);

	

}
