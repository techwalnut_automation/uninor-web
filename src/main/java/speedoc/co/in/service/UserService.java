package speedoc.co.in.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import speedoc.co.in.manager.UserManager;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;

@Service
public class UserService implements IUserService {

	@Autowired
	protected UserManager userManager;

	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}

	public ServiceResponse<UserVo> findById(Long userId) {
		ServiceResponse<UserVo> serviceResponse = new ServiceResponse<UserVo>();
		serviceResponse.setResponse(userManager.findById(userId) );
		return serviceResponse;
	}

	public ServiceResponse<UserVo> findByUsername(String userName)
			throws Exception {
		ServiceResponse<UserVo> serviceResponse = new ServiceResponse<UserVo>();
		serviceResponse.setResponse( userManager.findByUserName(userName) );
		return serviceResponse;
	}

	public UserVo findByUniqueUserName(String username) {
		return userManager.findByUniqueUserName(username);
	}
	
	public void saveOrUpdate(UserVo userVo, String userType) {
		userVo.setEnabled(true);
		userVo.setAccountExpired(false);
		userVo.setAccountLocked(false);
		try {
			userManager.saveOrUpdate(userVo,userType);
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	public RoleVo getAllRolesNameByUserId(Long userId) {
		
		return userManager.getAllRolesNameByUserId(userId);
	}
	
	@RequestMapping(value = "/get-all-user", method = RequestMethod.GET)
	public ServiceResponse<List<UserVo>> getAllUser(){
		ServiceResponse<List<UserVo>> response = new ServiceResponse<List<UserVo>>() ;
		response.setResponse(userManager.getAllUser());
		return response ;
	}
	
	
	public ServiceResponse<GridDataVo<List<UserVo>>> getUsersByRowsCount(FilterVo<UserVo> filterVo) {
		ServiceResponse<GridDataVo<List<UserVo>>> serviceResponse = new ServiceResponse<GridDataVo<List<UserVo>>>();
		GridDataVo<List<UserVo>> gridDataVo = new GridDataVo<List<UserVo>>();
		List<UserVo> userVos = userManager.getUsersByRowsCount(filterVo);
		gridDataVo.setResult(userVos);
		serviceResponse.setResponse(gridDataVo);
		serviceResponse.setSuccessful(true);
		return serviceResponse;
	}

	@Override
	public boolean getExistingUserName(String username) {
		return userManager.getExistingUserName(username);
	}

	@Override
	public ServiceResponse<GridDataVo<List<UserVo>>> getUserList(
			FilterVo<UserVo> filterVo) {
		ServiceResponse<GridDataVo<List<UserVo>>> serviceResponse = new ServiceResponse<GridDataVo<List<UserVo>>>();
		GridDataVo<List<UserVo>> gridDataVo = new GridDataVo<List<UserVo>>();
		List<UserVo> userVos = userManager.getUserList(filterVo);
		gridDataVo.setResult(userVos);		
		Long count = userManager.getUsersCount();
		gridDataVo.setRecords(count.toString());
		if (filterVo.getRowsPerPage() == userVos.size()) {
			gridDataVo.setHasMorePages(true);
		} else {
			gridDataVo.setHasMorePages(false);
		}
		
		gridDataVo.setMaxPageNumber((int)Math.floor(count/filterVo.getRowsPerPage()+ (count%filterVo.getRowsPerPage() >= 0 ? 1 : 0)));
		gridDataVo.setMaxRowCount(count);
		serviceResponse.setResponse(gridDataVo);
		serviceResponse.setSuccessful(true);
		return serviceResponse;
	}
	
	
}
