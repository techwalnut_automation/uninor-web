package speedoc.co.in.controller;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import speedoc.co.in.service.ISlcDoneService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.util.JqgridResponse;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;
import speedoc.co.in.vo.SlcDoneFileVo;

@Controller
@RequestMapping("/slcdone")
public class SlcDoneController {
	
	private static Logger logger=Logger.getLogger(SlcDoneController.class);
	
	@Value("${SLC_DONE_INPROCESS_FILE_PATH}")
	public String slcDoneInprocessFilePath;
	
	@Autowired
	ISlcDoneService slcDoneService;
	
	@Autowired
	IUserService userService;
	
	//slc done page
	@RequestMapping(value="/getslcdoneuploadform", method=RequestMethod.GET)
	public String getSlcDoneForm(ModelMap modelMap){
		return "slc-done/slc-done-page";
	}
	
	// upload file in inprocess folder
	@SuppressWarnings("deprecation")
	@RequestMapping(value="/uploadSlcDoneFile", method=RequestMethod.POST)
	public String uploadSlcDoneFile(MultipartHttpServletRequest uploadSlcForm, ModelMap modelMap, HttpServletRequest servletRequest){
		Iterator<String> itr =  uploadSlcForm.getFileNames();
		MultipartFile files = uploadSlcForm.getFile(itr.next());
		if (null != files) {
			try {
				String fileName = files.getOriginalFilename();
				Date date = new Date();
				String slcFilePath = slcDoneInprocessFilePath + File.separator
						+ FilenameUtils.getBaseName(fileName) + "_"
						+ date.getHours() + "_" + date.getMinutes() + "_"
						+ date.getSeconds() + "."
						+ FilenameUtils.getExtension(fileName);
				files.transferTo(new File(slcFilePath));				
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}
		return "slc-done/slc-refresh-page";
	}
	
	// show list of uploaded files
	@RequestMapping(value = "/getgridpage/{month}" ,method = RequestMethod.GET)
	public String getGridPage(ModelMap modelMap, @PathVariable String month){	
		modelMap.addAttribute("month", month);
		return "slc-done/slc-done-grid-page";
	}
	
	@RequestMapping(value="/getslcdonepage", method=RequestMethod.GET)
	public String getAvamPage(ModelMap modelMap){		
		return "slc-done/slc-refresh-page";
	}
	
	// get list of files
	@RequestMapping(value = "/getslcdonelist", produces = "application/json")
	@ResponseBody JqgridResponse<SlcDoneFileVo> getSlcDoneList(@RequestParam(value = "_search", required = false) Boolean search,
			@RequestParam(value = "filters", required = false) String filters,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx,
			@RequestParam(value = "sord", required = false) String sord,
			@RequestParam (value = "month") String month) {
		ServiceResponse<GridDataVo<List<SlcDoneFileVo>>> serviceResponse = null;
		try {
			FilterVo<SlcDoneFileVo> filterVo = new FilterVo<SlcDoneFileVo>();
			filterVo.setPageNumber(page);
			filterVo.setRowsPerPage(rows);
			serviceResponse = slcDoneService.getSlcDoneList(filterVo,month);
		} catch (Exception e) {
			logger.error("Error in getavammonthwiselist of AvamController::"+ e.getMessage());
		}
		JqgridResponse<SlcDoneFileVo> jqgridResponse = null;
		if(serviceResponse != null){
			 jqgridResponse = JQGridJSONObject1(serviceResponse, page, rows);
		}
		return jqgridResponse;
	}

	private JqgridResponse<SlcDoneFileVo> JQGridJSONObject1(ServiceResponse<GridDataVo<List<SlcDoneFileVo>>> serviceResponse, Integer page, Integer rows) {
		JqgridResponse<SlcDoneFileVo> response = new JqgridResponse<SlcDoneFileVo>();	
		response.setRows(serviceResponse.getResponse().getResult());
		if (serviceResponse.getResponse().getMaxPageNumber() != null) {
			response.setTotal(serviceResponse.getResponse().getMaxPageNumber().toString());
		} else {
			response.setTotal("1");
		}
		response.setPage(Integer.valueOf(page).toString());
		response.setRecords(serviceResponse.getResponse().getRecords());
		return response;
	}
}
