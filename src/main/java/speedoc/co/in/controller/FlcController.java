package speedoc.co.in.controller;

import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.service.ServiceResponse;
import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/flc")
public class FlcController {
	private Logger log = Logger.getLogger(FlcController.class);

	@Autowired
	private IFlcService flcService;

	@Autowired
	private IUserService userService;

	@RequestMapping(value = "/getflcpage", method = RequestMethod.GET)
	public String getFlcPage(ModelMap modelMap) {
		ServiceResponse<List<RemarkVo>> sr = flcService.getRejectReasons();
		modelMap.addAttribute("remarkVoList", sr.getResult());
		return "flc/flc-page";
	}

	@RequestMapping(value = "getflcdata", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<CafFlcVo> getFlcData(ModelMap modelMap) {
		log.info("In getFlcData");
		ServiceResponse<CafFlcVo> response = null;
		 UserVo userVo=getUserLogin();
		response = flcService.getUnknownCaf(userVo);
		return response;
	}

	@RequestMapping(value = "getcafimagenames", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> getCafImageNames(CafFlcVo cafFlcVo){
		log.debug("In getcafimagenames");
		if (cafFlcVo != null) {
			log.info("Caf File Location = " + cafFlcVo.getFileLocation());
			return flcService.getCafImageNames(cafFlcVo.getFileLocation());
		} else {
			ServiceResponse<String> response = new ServiceResponse<String>();
			response.setSuccessful(false);
			return response;
		}
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<CafFlcVo> saveData(CafFlcVo cafFlcVo, ModelMap modelMap) {
		log.info("cafFlcVo = " + cafFlcVo);
		ServiceResponse<CafFlcVo> response = null;
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		log.info("FlcController user.getUsername() :: " + user.getUsername());
		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
		log.info("FlcController userVo :: " + userVo);
		response = flcService.save(cafFlcVo, userVo);
		return response;
	}

	@RequestMapping(value = "getcaffile", method = RequestMethod.GET)
	public void getCafFile(@RequestParam String fileName,
			@RequestParam String timestamp, OutputStream out) {
		flcService.getCafFile(fileName, out);
	}
	
//	@RequestMapping(value = "getcaffile", method = RequestMethod.GET)
//	public void getCafFile(@RequestParam String fileName,
//			@RequestParam String timestamp,HttpServletResponse response) {
//		try {
//			flcService.getCafFile(fileName, response.getOutputStream());
//		} catch (IOException e) {	
//			log.error("FlcController :: getcaffile "+e.getMessage()); 
//			e.printStackTrace();
//			
//		}finally{
//			try {
//				response.flushBuffer();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
//	}

	@RequestMapping(value = "/renamereject", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> rejectFlc(@RequestParam String remark, CafFlcVo cafFlcVo) {
		log.info("renameReject :: cafFlcVo= " + cafFlcVo);
		UserVo userVo = getUserLogin();
		ServiceResponse<String> rs = flcService.rejectFlc(remark, cafFlcVo, userVo);
		return rs;
	}

	@RequestMapping(value = "/skip", method = RequestMethod.POST)
	public @ResponseBody ServiceResponse<String> skipFlc(CafFlcVo cafFlcVo) {
		UserVo userVo = getUserLogin();
		ServiceResponse<String> response = flcService.skipFlc(cafFlcVo, userVo);
		return response;
	}

	@RequestMapping(value = "/check-speedocId-exist", method = RequestMethod.POST)
	public @ResponseBody String checkSpeedocIdExist(@RequestParam String speedocId, ModelMap modelMap) {
		boolean checkSpeedocIdExist = flcService.checkSpeedocIdExist(speedocId);
		if (checkSpeedocIdExist == true) {
			return "success";
		} else {
			return "fail";
		}
	}

	private UserVo getUserLogin() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		log.info("user.getUsername() :: " + user.getUsername());
		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
		log.info("userVo :: " + userVo);
		return userVo;
	}
}
