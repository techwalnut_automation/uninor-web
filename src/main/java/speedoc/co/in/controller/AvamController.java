package speedoc.co.in.controller;

import java.io.File;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import speedoc.co.in.service.AvamService;
import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.util.JqgridResponse;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.AvamImportVo;
import speedoc.co.in.vo.FileUploadVO;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.GridDataVo;

@Controller
@RequestMapping("/avamController")
public class AvamController {
	
	private static Logger logger=Logger.getLogger(AvamController.class);
	
	@Value("${AVAM_INPROCESS_DATA_PATH}")
	private String avamInprocessDataPath;

	@Autowired
	AvamService avamService;
	
	@Autowired
	IUserService userService;
	
	@Autowired
	IFlcService flcService;
	
	@RequestMapping(value="/getuploadavamform",method=RequestMethod.GET)
	public String getUploadAvam(ModelMap modelMap){
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		logger.info("user.getUsername() :: " + user.getUsername());
//		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
//			flcService.updateCafTempPool(userVo) ;
		modelMap.addAttribute("uploadForm",new FileUploadVO());
		return "avam/uploadAvamForm";
	}

	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public String save(MultipartHttpServletRequest uploadForm, ModelMap modelMap , HttpServletRequest request) {
		Iterator<String> itr =  uploadForm.getFileNames();
		MultipartFile files = uploadForm.getFile(itr.next());
		if (null != files) {
			try {
				String fileName = files.getOriginalFilename();
				Date date = new Date();
				String avamFilePath = avamInprocessDataPath + File.separator
						+ FilenameUtils.getBaseName(fileName) + "_"
						+ date.getHours() + "_" + date.getMinutes() + "_"
						+ date.getSeconds() + "."
						+ FilenameUtils.getExtension(fileName);
				files.transferTo(new File(avamFilePath));				
			} catch (Exception e) {
				logger.error(e.getMessage(),e);
			}
		}
		return "avam/avamPage";
	}
		
	@RequestMapping(value = "/getgridpage/{month}" ,method = RequestMethod.GET)
	public String getGridPage(@PathVariable String month, ModelMap modelMap){	
		modelMap.addAttribute("month", month);
		return "avam/avamImportGrid";
	}
	
	@RequestMapping(value="/getavampage", method=RequestMethod.GET)
	public String getAvamPage(ModelMap modelMap){		
		return "avam/avamPage";
	}
	
	@RequestMapping(value = "/getavammonthwiselist", produces = "application/json")
	@ResponseBody JqgridResponse<AvamImportVo> getAvamMonthWiseList(
			@RequestParam(value = "_search", required = false) Boolean search,
			@RequestParam(value = "filters", required = false) String filters,
			@RequestParam(value = "page", required = false) Integer page,
			@RequestParam(value = "rows", required = false) Integer rows,
			@RequestParam(value = "sidx", required = false) String sidx,
			@RequestParam(value = "sord", required = false) String sord,
			@RequestParam(value = "month") String month) {
		ServiceResponse<GridDataVo<List<AvamImportVo>>> serviceResponse = null; 
		try {			
			FilterVo<AvamImportVo> filterVo = new FilterVo<AvamImportVo>() ;
			filterVo.setPageNumber(page);
			filterVo.setRowsPerPage(rows) ;
			serviceResponse = avamService.getAvamMonthWiseList(filterVo,month);
		} catch (Exception e) {
			logger.error("Error in getavammonthwiselist of AvamController::" +e.getMessage(),e);
		}		
		JqgridResponse<AvamImportVo> jqgridResponse = JQGridJSONObject(serviceResponse, page, rows);
		return jqgridResponse;		
	}
	
	private JqgridResponse<AvamImportVo> JQGridJSONObject(ServiceResponse<GridDataVo<List<AvamImportVo>>> serviceResponse,Integer page, Integer rows) {
		JqgridResponse<AvamImportVo> response = new JqgridResponse<AvamImportVo>();
		response.setRows(serviceResponse.getResponse().getResult());
		if (serviceResponse.getResponse().getMaxPageNumber() != null) {
			response.setTotal(serviceResponse.getResponse().getMaxPageNumber().toString());
		} else {
			response.setTotal("1");
		}
		response.setPage(Integer.valueOf(page).toString());
		response.setRecords(serviceResponse.getResponse().getRecords());
		return response;
	}
}
