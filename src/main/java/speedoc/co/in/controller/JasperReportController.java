package speedoc.co.in.controller;

import java.io.File;
import java.io.FileInputStream;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import speedoc.co.in.service.IStatusService;
import speedoc.co.in.service.ScanService;

@Controller
@RequestMapping("/jasper")
public class JasperReportController {

	@Autowired
	DataSource dataSource;

	@Autowired
	IStatusService statusService;

	@Autowired
	ScanService scanService;

	private static Logger logger = Logger
			.getLogger(JasperReportController.class);

	@RequestMapping(value = "/downloadreport", method = RequestMethod.POST)
	public String downloadReport(ModelMap modelMap,
			HttpServletResponse response, @RequestParam String fromDate,
			@RequestParam String toDate, @RequestParam String statusId)
			throws Exception {
		logger.info(" mis export ::   fromDate :: " + fromDate
				+ "  to date :: " + toDate + " export statusId :: " + statusId);
		String returnView = null;
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			Date dateStr = formatter.parse(fromDate);

			String endDate = toDate + " 23:59:00";
			SimpleDateFormat formatter1 = new SimpleDateFormat(
					"dd/MM/yyyy hh:mm:ss");
			Date dateEnd = formatter1.parse(endDate);

			Timestamp timestamp = new Timestamp(dateStr.getTime());
			Timestamp timestamp2 = new Timestamp(dateEnd.getTime());

			if (statusId.equalsIgnoreCase("SCAN_COMPLETE")) {
				returnView = "scaningCompleteReport";
			} else if (statusId.equalsIgnoreCase("AVAM_DATA")) {
				returnView = "avamDataReport";
			} else if (statusId.equalsIgnoreCase("AVAM_DONE")) {
				returnView = "avamDoneReport";
			} else if (statusId.equalsIgnoreCase("AVAM_FAILED")) {
				returnView = "avamFailedReport";
			} else if (statusId.equalsIgnoreCase("DE_UPLOAD_DATA")) {
				returnView = "deUploadReport";
			} else if (statusId.equalsIgnoreCase("FLC_READY")) {
				returnView = "flcReadyReport";
			} else if (statusId.equalsIgnoreCase("FLC_ACCEPT")) {
				returnView = "flcCompleteReport";
			} else if (statusId.equalsIgnoreCase("FLC_REJECT")) {
				returnView = "flcRejectReport";
			} else if (statusId.equalsIgnoreCase("FLC_SKIP")) {
				returnView = "flcSkipReport";
			} else if (statusId.equalsIgnoreCase("DE_COMPLETE")) {
				returnView = "deCompleteReport";
			} else if (statusId.equalsIgnoreCase("SLC_UPLOAD_DATA")) {
				returnView = "slcUploadReport";
			} else if (statusId.equalsIgnoreCase("SCRUTINY_READY")) {
				returnView = "slcReadyReport";
			} else if (statusId.equalsIgnoreCase("SCRUTINY_COMPLETE")) {
				returnView = "slcCompleteReport";
			} else if (statusId.equalsIgnoreCase("SCRUTINY_REJECT")) {
				File file=statusService.getScruitinyRejectData(fromDate,toDate);
				logger.info("file :: " + file);
				if (file != null) {
					response.setHeader("Content-Disposition","attachment; filename="
									+ FilenameUtils.getName(file.getPath()));
					response.setContentType("application/octet-stream");
					IOUtils.copy(new FileInputStream(file),response.getOutputStream());
					response.flushBuffer();
				}else{
					return "norecord";
				}
				//returnView = "slcRejectReport";
			} else if (statusId.equalsIgnoreCase("INDEXING_DATA")) {
//				File file=statusService.getIndexingData(fromDate,toDate);
//				logger.info("file :: " + file);
//				if (file != null) {
//					response.setHeader("Content-Disposition","attachment; filename="
//									+ FilenameUtils.getName(file.getPath()));
//					response.setContentType("application/octet-stream");
//					IOUtils.copy(new FileInputStream(file),
//							response.getOutputStream());
//					response.flushBuffer();
//				}
				returnView = "indexingTableReport";
			} else if (statusId.equalsIgnoreCase("AUDIT_READY")) {
				returnView = "auditReadyReport";
			} else if (statusId.equalsIgnoreCase("AUDIT_DONE")) {
				returnView = "auditDoneReport";
			} else if (statusId.equalsIgnoreCase("AUDIT_REJECT")) {
				returnView = "auditRejectReport";
			} else if (statusId.equalsIgnoreCase("MASTER_MIS_REPORT")) {
				returnView = "masterMisReport";
			} else if (statusId.equalsIgnoreCase("FTP_PROCESS_REPORT")) {
				File file = statusService.getFtpReport(timestamp, timestamp2);
				logger.info("file :: " + file);
				if (file != null) {
					response.setHeader("Content-Disposition","attachment; filename="
									+ FilenameUtils.getName(file.getPath()));
					response.setContentType("application/octet-stream");
					IOUtils.copy(new FileInputStream(file),
							response.getOutputStream());
					response.flushBuffer();
				}
				else {
					return "norecord";
				}
			}
			modelMap.addAttribute("fromDate", timestamp);
			modelMap.addAttribute("toDate", timestamp2);
			modelMap.addAttribute("dataSource", dataSource);
			modelMap.addAttribute("format", "csv");
			return returnView;
		} catch (ParseException e) {
			logger.error("downloadReport :: " + e.getMessage(), e);
		}
		return null;
	}
}
