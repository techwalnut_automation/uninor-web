package speedoc.co.in.controller;

import java.io.OutputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.IAuditService;
import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.IScrutinyService;
import speedoc.co.in.service.IUserService;
import speedoc.co.in.service.ServiceResponse;
import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/audit")
public class AuditController {

	@Autowired
	private IAuditService auditService;

	@Autowired
	private IFlcService flcService;

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IScrutinyService scrutinyService;

	private static Logger logger = Logger.getLogger(AuditController.class);

	@RequestMapping(value = "/getauditform", method = RequestMethod.GET)
	public String getAuditPage(ModelMap modelMap) {
//		UserVo userVo = getUserLogin();
//		flcService.updateCafTempPool(userVo) ;
		return "audit/audit-page";
	}

	@RequestMapping(value = "getauditdata", method = RequestMethod.POST)
	public @ResponseBody
	ServiceResponse<CafAuditVo> getAuditData(ModelMap modelMap) {
		logger.info("In getAuditData");
		ServiceResponse<CafAuditVo> response = null;
		UserVo userVo = getUserLogin();
		response = auditService.getSlcRejectedCafs(userVo);
		return response;
	}

	@RequestMapping(value = "getcafimagenames", method = RequestMethod.POST)
	public @ResponseBody
	ServiceResponse<String> getCafImageNames(CafAuditVo cafAuditVo) {
		logger.debug("In getcafimagenames");
		if (cafAuditVo != null) {
			logger.info("Caf File Location = "+ cafAuditVo.getCafFileLocation());
			return flcService.getCafImageNames(cafAuditVo.getCafFileLocation());
		} else {
			ServiceResponse<String> response = new ServiceResponse<String>();
			response.setSuccessful(false);
			return response;
		}
	}

	@RequestMapping(value = "getcaoimage", method = RequestMethod.POST)
	public @ResponseBody
	ServiceResponse<CaoVo> getCaoImage(@RequestParam String caoCode,
			ModelMap modelMap) {
		logger.info("In getCaoImage caoCode = " + caoCode);
		ServiceResponse<CaoVo> response = null;
		response = auditService.getCaoImage(caoCode);
		return response;
	}

	@RequestMapping(value = "getcaoimage", method = RequestMethod.GET)
	public void getCafFile(@RequestParam String fileName,
			@RequestParam String timestamp, OutputStream out) {
		auditService.getCafFile(fileName, out);
	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public @ResponseBody
	ServiceResponse<String> save(CafAuditVo cafAuditVo, ModelMap modelMap) {
		logger.info("cafAuditVo = " + cafAuditVo);
		ServiceResponse<String> response = null;
		UserVo userVo = getUserLogin();
		response = auditService.save(cafAuditVo, userVo);
		return response;
	}

	private UserVo getUserLogin() {
		User user = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		logger.info("user.getUsername() :: " + user.getUsername());
		UserVo userVo = userService.findByUniqueUserName(user.getUsername());
		logger.info("userVo :: " + userVo);
		return userVo;
	}

	@RequestMapping(value = "/renamereject", method = RequestMethod.POST)
	public @ResponseBody
	ServiceResponse<String> rejectAudit(@RequestParam String auditremak,
			CafAuditVo cafAuditVo) {
		logger.info("AuditController rejectAudit :: cafAuditVo= " + cafAuditVo);
		logger.info("auditremak=" + auditremak);
		UserVo userVo = getUserLogin();
		ServiceResponse<String> rs = auditService.rejectAudit(auditremak,
				cafAuditVo, userVo);
		return rs;
	}

	@RequestMapping(value = "getrejectreasonaudit", method=RequestMethod.POST)
	public String getRejReason(ModelMap modelMap, @RequestParam String header){
		ServiceResponse<List<RemarkVo>> sr = scrutinyService.getRejectReasons(header);	
		modelMap.addAttribute("remarkVoList", sr.getResult());
		return "audit/audit-reason-page";
	}
	
	@RequestMapping(value = "getpriviewpageforedit", method=RequestMethod.GET)
	public String getDataEntryPageForEdit(@RequestParam String cafTblId,ModelMap modelMap){
		logger.info("dehorizontalview cafTblId ::  "+cafTblId);		
		modelMap.addAttribute("cafTblId", cafTblId);
		return "audit/preveiwPage";
	}
	
	@RequestMapping(value="getCafData",method=RequestMethod.POST )
	@ResponseBody ServiceResponse<CafAuditVo> getCafData(CafAuditVo cafAuditVo){
		ServiceResponse<CafAuditVo> response = auditService.getCafData(cafAuditVo.getId());
		return response;
	}
}
