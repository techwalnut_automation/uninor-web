package speedoc.co.in.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import speedoc.co.in.service.IFlcService;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.service.UserService;
import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;

@Controller
@RequestMapping("/login")
public class LoginController {

	@Autowired
	protected UserService userService;

	@Autowired
	private SessionService sessionService;

	@Autowired
	IFlcService flcService;

	private static Logger logger = Logger.getLogger(LoginController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String defaultLoginPage(
			@RequestParam(value = "error", required = false) boolean error,
			ModelMap model) {
		if (error == true) {
			model.put("cond", true);
			model.put("error",
					"You have entered an invalid username or password!");
		} else {
			model.put("cond", false);
			model.put("error", "");
		}
		return "loginPage";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap modelMap) {
		// UserVo userVo=new UserVo();
		// flcService.updateCafTempPool(userVo) ;
		return "loginPage";
	}

	@RequestMapping(value = "loginPage", method = RequestMethod.GET)
	public String showLoginPage() {
		return "loginPage";
	}

	@RequestMapping(value = "loginSuccess", method = RequestMethod.GET)
	public String processRequest(ModelMap modelMap, HttpSession httpSession) {
		try {
			User user = (User) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal();
			logger.info("LoginController user :: " + user);
			UserVo userVo = userService.findByUniqueUserName(user.getUsername());
			logger.info("LoginController userVo :: " + userVo);
	
			sessionService.setSessionId(httpSession.getId());
			
			sessionService.setUserVo(userVo);
			RoleVo roleVo = userService.getAllRolesNameByUserId(userVo.getId());
			logger.info("LoginController roleVo :: " + roleVo);
			sessionService.setRoleVo(roleVo);
			modelMap.addAttribute("username", user.getUsername());
			return "success";
		} catch (ClassCastException e) {
			return "loginPage";
		}
	}
	@RequestMapping(value = "accessdenied", method = RequestMethod.GET)
	public String accessDenied() {
		return "accessDenied";
	}
}