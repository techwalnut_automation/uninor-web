package speedoc.co.in.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.service.ISearchService;
import speedoc.co.in.service.ServiceResponse;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.SearchVo;

@Controller
@RequestMapping("/searchController")
public class SearchController {

	private static Logger logger = Logger.getLogger(SearchController.class.getName());
	
	@Autowired
	ISearchService searchService;
	
	@RequestMapping(value = "/getsearchpage", method = RequestMethod.GET)
	public String getSearchPage(ModelMap modelMap) {
		modelMap.addAttribute("searchVo", new SearchVo());
		return "search/searchPage";
	}

	@RequestMapping(value = "/getsearchdata", method = RequestMethod.POST)
	@ResponseBody ServiceResponse<List<SearchResultVo>> getSearchData(ModelMap modelMap, @RequestParam String searchTxt, @RequestParam String searchOption, HttpServletResponse httpServletResponse) {
		logger.info(" searchTxt = " + searchTxt+" searchOption = "+searchOption);
		ServiceResponse<List<SearchResultVo>> response = new ServiceResponse<List<SearchResultVo>>();
		List<SearchResultVo> searchResultVos=null;
		if(searchOption.equalsIgnoreCase("Mobile Number")){
			 searchResultVos = searchService.getSearchDataByMobileNumber(searchTxt);
			logger.info("Mobile Number searchResultVos size :: " + searchResultVos.size());
		}else if(searchOption.equalsIgnoreCase("Speedoc Id")){
			searchResultVos=searchService.getSearchDataBySpeedocId(searchTxt);
			logger.info("Speedoc Id searchResultVos size :: " + searchResultVos.size());
		}
		if (searchResultVos.size() > 0) {
			response.setResult(searchResultVos);
			response.setSuccessful(true);
		} else {
			response.setSuccessful(false);
			response.setMessage("No Record found.");
		}
		return response;
	}
}
