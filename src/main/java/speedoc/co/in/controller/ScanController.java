package speedoc.co.in.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import speedoc.co.in.mail.MailMail;
import speedoc.co.in.service.ScanService;
import speedoc.co.in.util.ServiceResponse;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.ClientInfoVO;
import speedoc.co.in.vo.ScanCafVO;
import speedoc.co.in.vo.ScanClientInfoVO;
import speedoc.co.in.vo.UserVo;

import com.thoughtworks.xstream.XStream;

@Controller
@RequestMapping("/scan")
public class ScanController {
	 
	@Autowired
	ScanService scanService;
	
	@Autowired
	MailMail mail;
	
	private static Logger logger=Logger.getLogger(ScanController.class);
		
	@RequestMapping(value = "/saveFile", method = RequestMethod.POST)
	public @ResponseBody String saveFile(ScanCafVO scanCafVO) {
		logger.debug("Entering into setcaf :: CAF Received ::  "+ scanCafVO.getFileName());
		ServiceResponse<String> serviceResponse = new ServiceResponse<String>();
	
		XStream xStream = new XStream();
		xStream.setMode(XStream.NO_REFERENCES);
		xStream.alias("CafVO", CafVo.class);
		xStream.alias("CafStatusVO", CafStatusVo.class);
		InputStream in = null;  
		try {	
			in = scanCafVO.getFile().getInputStream();
			serviceResponse = scanService.savePDF(in , scanCafVO.getFileName());
			if(serviceResponse.getMessage() == "success"){
				return "success";
			}else if(serviceResponse.getMessage() == "already_exists"){
				return "already_exists";
			}else {
				return "failure";
			}	
		} catch (IOException e) {
			logger.error("Error ScanController in saveFile :: "+e.getMessage(),e);
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("Error Getting inputstream ...");
		} finally {
			try {
				in.close(); 
			} catch (IOException e) {
				logger.error("ScanController :: savePdf :: " + e.getMessage());
				serviceResponse.setSuccessful(false);
				serviceResponse.setMessage("Error Closing file...");
			}
		}
		String response = serviceResponse.getMessage();
		logger.debug("Exit from savePDF. with response = "+ response);
		return response;
	}

	@RequestMapping(value = "getscanclientinfo", method = RequestMethod.POST)
	public @ResponseBody String getScanClientInfo(ClientInfoVO clientInfoVO) {		
		ServiceResponse<ScanClientInfoVO> serviceResponse = null;
		XStream xStream = new XStream();
		xStream.setMode(XStream.NO_REFERENCES);
		xStream.alias("ScanClientInfoVO", ScanClientInfoVO.class);
		xStream.alias("UserVO", UserVo.class);
		ScanClientInfoVO scanClientInfoVO = (ScanClientInfoVO) xStream.fromXML(clientInfoVO.getScanClientInfoVO());	
		try {
			serviceResponse = scanService.getScanClientInfo(scanClientInfoVO);		
		} catch (Exception e) {
			logger.error("ScanController :: getScanClientInfo :: " + e.getMessage());
			serviceResponse.setSuccessful(false);
			serviceResponse.setMessage("Error Getting inputstream ...");
		}
		String  statusXml= xStream.toXML(serviceResponse.getResponse());
		return statusXml;
	}
	
	@RequestMapping(value = "getJnlp", method = RequestMethod.GET)
	public String getRegKey(String key, String uvId, HttpServletResponse response, ModelMap modelMap) {
		modelMap.addAttribute("userId", uvId);
		modelMap.addAttribute("regKey", key);
		modelMap.addAttribute("url", scanService.getPostUrl());
		modelMap.addAttribute("hostUrl", scanService.getHostUrl());
		response.setContentType("application/x-java-jnlp-file");
		response.setHeader("Content-Disposition","attachment;filename=speedoc-scan.jnlp");
		return "jnlpFile";
	}
}
