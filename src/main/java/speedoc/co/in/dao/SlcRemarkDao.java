package speedoc.co.in.dao;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.SlcRemark;
import speedoc.co.in.vo.SlcRemarkVo;


@Repository
public class SlcRemarkDao implements ISlcRemarkDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	private static Logger logger =Logger.getLogger(SlcRemarkDao.class);
	
	public Long save(SlcRemarkVo slcRemarkVo ) throws Exception{
			SlcRemark slcRemark = new SlcRemark() ;
			BeanUtils.copyProperties(slcRemark, slcRemarkVo) ;
			Session session = sessionFactory.getCurrentSession();
			session.save(slcRemark) ;
//			session.flush() ;
			return slcRemark.getId() ;
	}

	@Override
	public boolean deleteSlcPool() {
		try {
			Session session=sessionFactory.getCurrentSession();
			  session.createSQLQuery("DELETE FROM slc_pool_tbl WHERE start_date < (NOW() - INTERVAL '5 minute')")
			  .addScalar("id", StandardBasicTypes.BIG_INTEGER)
			  .addScalar("caf_tbl_id", StandardBasicTypes.BIG_INTEGER)
			  .addScalar("start_date", StandardBasicTypes.TIMESTAMP)		 
			  .executeUpdate();		
			return true;
		} catch (Exception e) {
			logger.error("Error in deleteFlcPool() "+ e.getMessage() ,e ) ;
			return false;
		}
	}


}
