package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.UserVo;

public interface IUserDao {
	UserVo findByUsername(String username) throws Exception;

	List<UserVo> getAllUsers();

	UserVo findById(Long userId);

	public UserVo findByUniqueUserName(String username);

	public UserVo saveOrUpdate(UserVo userVo,String prevOption) throws Exception;
	
	public Long getUsersCount() ;
	 
	public List<UserVo> getUsersByRowsCount(FilterVo<UserVo> filterVo) ;

	boolean getExistingUserName(String username);

	List<UserVo> getUserList(FilterVo<UserVo> filterVo);
	
}
