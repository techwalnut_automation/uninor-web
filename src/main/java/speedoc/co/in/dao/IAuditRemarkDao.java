package speedoc.co.in.dao;

import speedoc.co.in.vo.AuditRemarkVo;

public interface IAuditRemarkDao {

	Long save(AuditRemarkVo auditRemarkVo);

}
