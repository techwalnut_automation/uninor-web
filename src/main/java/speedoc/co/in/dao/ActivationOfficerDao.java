package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Cao;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.FilterVo;

@Repository
public class ActivationOfficerDao implements IActivationOfficerDao {
	private static Logger logger = Logger.getLogger(ActivationOfficerDao.class);

	@Autowired
	SessionFactory sessionFactory;

	public CaoVo saveActivationOfficer(CaoVo caoVo) {
		Cao cao = new Cao();
		try {
			Utils.copyProperties(cao, caoVo);
			if (caoVo.getId() == null) {
				Session session = sessionFactory.getCurrentSession();
				session.save(cao);
				session.flush();
				if (cao != null) {
					caoVo = new CaoVo();
					Utils.copyProperties(caoVo, cao);
				}
			} else {
				logger.info("Updating Cao ::" + caoVo.getId());
				String hql = "UPDATE Cao set caoName = :caoName, caoCode = :caoCode, imageName = :imageName WHERE id = :id";
				Query query = sessionFactory.getCurrentSession().createQuery(hql);
				query.setParameter("caoName", caoVo.getCaoName());
				query.setParameter("caoCode", caoVo.getCaoCode());
				query.setParameter("imageName", caoVo.getImageName());
				query.setParameter("id", caoVo.getId());
				int result = query.executeUpdate();
				logger.info("result :: " + result);
			}
		} catch (Exception e) {
			logger.error("saveActivationOfficer" + e.getMessage(), e);
			return null;
		}
		return caoVo;
	}

	@Override
	public boolean changeCaoStatus(Long id,int status) {
		
		try {
				logger.info("Updating Cao status::" + id);
				String hql = "UPDATE Cao set status = :status WHERE id = :id";
				Query query = sessionFactory.getCurrentSession().createQuery(hql);
				query.setParameter("status", status);				
				query.setParameter("id", id);
				int result = query.executeUpdate();
				logger.info("result :: " + result);
				return true;
			
		} catch (Exception e) {
			logger.error("changeCaoStatus " + e.getMessage(), e);
			return false;
		}		
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<CaoVo> getActivationList(FilterVo<CaoVo> filterVo) {
		try {
			if (filterVo != null) {
				filterVo.setPageNumber((filterVo.getPageNumber() - 1)
						* filterVo.getRowsPerPage());
			}
			Session session = sessionFactory.getCurrentSession();
			List list = session.createCriteria(Cao.class)
//					.addOrder(Order.asc("caoCode"))
					.addOrder(Order.asc("status"))
					.setFirstResult(filterVo.getPageNumber())
					.setMaxResults(filterVo.getRowsPerPage()).list();
			return convertToActivationOfficerVoList(list);
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	private List<CaoVo> convertToActivationOfficerVoList(List<Cao> list)
			throws Exception {
		ArrayList<CaoVo> activationOfficerVos = new ArrayList<CaoVo>();
		for (Cao activationOfficer : list) {
			CaoVo activationOfficerVo = convertToActivationOfficerVo(activationOfficer);
			activationOfficerVos.add(activationOfficerVo);
		}
		return activationOfficerVos;
	}

	private CaoVo convertToActivationOfficerVo(Cao activationOfficer)
			throws Exception {
		CaoVo activationOfficerVo = new CaoVo();
		Utils.copyProperties(activationOfficerVo, activationOfficer);
		return activationOfficerVo;
	}

	@Override
	public Boolean isCaoCodeAlreadyExist(String caoCode) {
		logger.info("caoCode :: " + caoCode);
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(Cao.class)
					.add(Restrictions.eq("caoCode", caoCode))
					.setProjection(Projections.rowCount()).uniqueResult();
			logger.info("isCaoCodeAlreadyExist :: count ::" + count);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error("Erron in isCaoCodeAlreadyExist method " + e.getMessage(),e);
			return false;
		}
	}
	
	@Override
	public Boolean isCaoCodeAlreadyExist(String caoCode, int activeStatus) {
		logger.info("caoCode :: " + caoCode);
		Long count = null; 
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(Cao.class)
					.add(Restrictions.eq("caoCode", caoCode))
					.add(Restrictions.eq("status", activeStatus))
					.setProjection(Projections.rowCount()).uniqueResult();
			logger.info("isCaoCodeAlreadyExist :: count ::" + count);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error("Erron in isCaoCodeAlreadyExist method " + e.getMessage(),e);
			return false;
		}
	}

	@Override
	public CaoVo getSignatureById(Long id) {
		CaoVo caoVo = null;
		try {
			Cao cao = (Cao) sessionFactory.getCurrentSession()
					.createCriteria(Cao.class).add(Restrictions.eq("id", id))
					.uniqueResult();
			if (cao != null) {
				caoVo = new CaoVo();
				Utils.copyProperties(caoVo, cao);
			}
			logger.info("caoVo :: " + caoVo);
		} catch (Exception e) {
			logger.error("getSignatureById :: " + e.getMessage(), e);
			return null;
		}
		return caoVo;
	}

	@Override
	public Long getActivationCount() {
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(Cao.class)
					.setProjection(Projections.rowCount()).uniqueResult();
			logger.info("count :: " + count);
		} catch (Exception e) {
			logger.error("Error in getActivationCount of class ActivationOfficerDao"+ e.getMessage(), e);
		}
		return count;
	}
}