package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.AuditPoolVo;
import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.UserVo;

public interface IAuditDao {

	CafStatusVo getSlcRejectedCafs(UserVo userVo) throws Exception;

//	Long save(AuditPoolVo auditPoolVo);

	boolean delete(Long auditPoolId);

	AuditPoolVo getAuditPoolByCafTblId(Long id);

	boolean deleteAuditPool();

	List<CafAuditVo>  findSlcRejectData(Long long1);

	CafVo getCafData(long parseLong);

	CafStatusVo getCafStatusDataByCafTblId(Long l);

}
