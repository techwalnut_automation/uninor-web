package speedoc.co.in.dao;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Caf;
import speedoc.co.in.vo.AvamVo;
import speedoc.co.in.vo.CafVo;

@Repository
public class ScanDao implements IScanDao {

	private static Logger log = Logger.getLogger(ScanDao.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unused")
	@Override
	public boolean updateCafTblByAvamData(CafVo cafVo, AvamVo avamVo) {
		String hql = "UPDATE Caf set mobileNumber = :mobileNumber , cafNumber = :cafNumber , cafFileLocation = :cafFileLocation,"
				+ " dtrCode= :dtrCode , caoCode=:caoCode, modifiedDate=:modifiedDate, activationDate=:activationDate, " +
						"modifiedBy=:modifiedBy , cafTrackingStatus =:cafTrackingStatus WHERE id = :id AND speedocId = :speedocId ";
		try{
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			query.setParameter("mobileNumber", avamVo.getMobileNumber());
			query.setParameter("cafNumber", avamVo.getCafNumber());
			query.setParameter("cafFileLocation", cafVo.getCafFileLocation());
			query.setParameter("dtrCode", avamVo.getDtrCode());
			query.setParameter("caoCode", avamVo.getCaoCode());
			query.setParameter("modifiedDate",cafVo.getModifiedDate());
			query.setParameter("activationDate",avamVo.getActivationDate());
			query.setParameter("modifiedBy", cafVo.getModifiedBy());
			query.setParameter("cafTrackingStatus", avamVo.getCafTrackingStatus());
			query.setParameter("id", cafVo.getId());
			query.setParameter("speedocId", avamVo.getSpeedocId());
			
			int result = query.executeUpdate();
			return true;
		}catch (Exception e) {
			log.error("updateCafTblByAvamData :: "+e.getMessage(),e);
			return false;
		}

	}

	@Override
	public boolean checkFileIdExistsInCaf(String fileId , String speedocId) {
		Long count = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			Criteria criteria = session.createCriteria(Caf.class);
			Criterion fileIdCriteria = Restrictions.eq("fileId", fileId);
			Criterion speedocIdCriteria = Restrictions.eq("speedocId", speedocId);
			
			criteria.setProjection(Projections.rowCount()).add(Restrictions.or(fileIdCriteria, speedocIdCriteria));
			count = (Long)criteria.uniqueResult();
			log.info("checkFileIdExistsInCaf :: getCountByFileId :: count ::"+count);
			if(count > 0){
				return true;
			}else{
				return false;
			}
		} catch (Exception e) {
			log.error("checkFileIdExistsInCaf :: " + e.getMessage(), e);
			return false;
		}
	}
}
