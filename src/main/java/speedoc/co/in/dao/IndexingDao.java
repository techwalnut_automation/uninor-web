package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.IndexingVo;

public interface IndexingDao {

	Integer saveIndexData(IndexingVo indexingV);

	List<IndexingVo> findIndexDataByStatus(Integer indexingStatusCode);
	List<IndexingVo> findNewIndexData(Integer indexingStatusCode);

	CafStatusVo searchCafTblForNewMobileIndexData(String mobileNumber, String cafNumber) throws Exception;

	void updateIndexingTbl(Long id, Integer count, Integer status, Long cafTblId);
	
	List<speedoc.co.in.vo.ScrutinyCaptureVo> getScrutinyCompletedRecords();

	String saveStatusToIndexingTbl( int status , Long cafTblId);

	speedoc.co.in.vo.UserSummaryVo getUserToAllot();

	Integer insertUserIntoSummary(speedoc.co.in.vo.UserSummaryVo userSummaryVO);

	void updateScrutinySummaryTbl(Integer allotedToUserSummaryId);

	Integer updateIndexingStatus();

	IndexingVo getElectroData();

	Integer saveElectroData(IndexingDataVO electroDataVo);

	Integer updateIndexingTbl(IndexingDataVO indexingDataVO);

	Integer updateIndexingNOTSaveTbl(IndexingDataVO indexingDataVO);



}
