package speedoc.co.in.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.classic.Session;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.SlcDoneFile;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.SlcDoneFileVo;

@Repository
public class SlcDoneDao implements ISlcDoneDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static Logger logger = Logger.getLogger(SlcDoneDao.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<SlcDoneFileVo> getSlcDoneMonthWiseList(
			FilterVo<SlcDoneFileVo> filterVo, int monthNumber, String year) {
		String sql = "select sdf.id, sdf.file_name, sdf.total_record, sdf.record_updated,"
				+ "sdf.status, sdf.created_date,ut.username from slc_done_file_tbl sdf "
				+ "join user_tbl ut on sdf.created_by=ut.id "
				+ "where extract(month from sdf.created_date)="
				+ monthNumber
				+ " AND extract(year from sdf.created_date)="
				+ year
				+ " "
				+ "ORDER BY sdf.created_date desc";
		try {
			Session session = sessionFactory.getCurrentSession();
			List<Object> list = session.createSQLQuery(sql).list();
			return convertToSlcDoneFileVoList(list);
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	private List<SlcDoneFileVo> convertToSlcDoneFileVoList(List<Object> list) {
		Iterator<Object> it = list.iterator();
		List<SlcDoneFileVo> avamVoList = new ArrayList<SlcDoneFileVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			SlcDoneFileVo slcDoneFileVo = new SlcDoneFileVo();
			// avamImportVo.setId((Long) object[0]);
			slcDoneFileVo.setFileName((String) object[1]);
			slcDoneFileVo.setTotalRecord((Integer) object[2]);
			slcDoneFileVo.setRecordUpdated((Integer) object[3]);
			slcDoneFileVo.setStatus((String) object[4]);
			slcDoneFileVo.setCreatedDate((Timestamp) object[5]);
			slcDoneFileVo.setUsername((String) object[6]);
			avamVoList.add(slcDoneFileVo);
		}
		return avamVoList;
	}

	@Override
	public Long getSlcDoneCount() {
		Long count = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			count = (Long) session.createCriteria(SlcDoneFile.class)
					.setProjection(Projections.rowCount()).uniqueResult();
			return count;
		} catch (Exception e) {
			logger.error(
					"Error in SlcDoneDao :: getSlcDoneCount :: "
							+ e.getMessage(), e);
			return null;
		}
	}

	// insert file data to table slc_done_data_tbl
	@Override
	public void addSlcDoneDataFile(String path) {
		try {
			String sql = "COPY slc_done_data_tbl (mobile_number, speedoc_id) FROM '"+path+"' DELIMITER ',' CSV HEADER ;";
			logger.info("sql addSlcDoneDataFile :: " + sql);
			Session session = sessionFactory.getCurrentSession();
			Transaction transaction = session.beginTransaction();
			int executeUpdate = session.createSQLQuery(sql).executeUpdate();
			logger.info("addSlcDoneDataFile :: executeUpdate :: "+executeUpdate);
			transaction.commit();
		} catch (Exception e) {
			logger.error(
					"Error in addSlcDoneDataFile :: SlcDoneDao :: "
							+ e.getMessage(), e);
		}
	}

	// update caf_status_tbl and slc_done_data_tbl by procedure
	@Override
	public Integer uploadSlcDoneFile() {
		try {
			logger.info("uploadSlcDoneFile :: slcUploadDao ");
			Session session = sessionFactory.getCurrentSession();
			Transaction transaction = session.beginTransaction();
			Query query = session.createSQLQuery("select slc_done_update_end_date_fn()");
			@SuppressWarnings("rawtypes")
			List list = query.list();
			logger.info("uploadSlcDoneFile :: " + query);
			logger.info(" uploadSlcDoneFile :: list :: " + list.size());
			transaction.commit();
			return list.size();
		} catch (Exception e) {
			logger.error("Error in uploadSlcDoneFile :: " + e.getMessage(), e);
			return null;
		}
	}

	// update slc_done_data_tbl file id
	@Override
	public Boolean updateSlcFileId(Long slcFileId) {
		try {
			String hql = "UPDATE SlcDoneData SET slcFileId=? WHERE status=?";
			Session session = sessionFactory.getCurrentSession();
			Transaction transaction = session.beginTransaction();
			int result = session.createQuery(hql).setLong(0, slcFileId).setLong(1, 0)
					.executeUpdate();
			logger.info("updateSlcFileId :: result :: "+result);
			transaction.commit();
			return true;
		} catch (Exception e) {
			logger.error(
					"Error in updateSlcFileId :: SlcDoneDao ::"
							+ e.getMessage(), e);
			return false;
		}
	}

	// update slcDoneFiletbl status and count
	@Override
	public Boolean updateSlcDoneFileByFIleId(Integer recordUpdated,
			Integer duplicateRecord, Long slcFileId) {
		try {
			String hql = "UPDATE SlcDoneFile SET recordUpdated=?, status=? "
					+ "WHERE id=?";
			Session session = sessionFactory.getCurrentSession();
			Transaction transaction = session.beginTransaction();
			int update = session.createQuery(hql).setInteger(0, recordUpdated)
					.setString(1, "COMPLETE").setLong(2, slcFileId)
					.executeUpdate();
			logger.info("update updateSlcDoneFileByFIleId :: "+update);
			transaction.commit();
			return true;
		} catch (Exception e) {
			logger.error("updateSlcImportByImportId :: SlcUploadDao ::"+ e.getMessage(), e);
			return false;
		}
	}

	// insert slc file info to slc_done_file_tbl
	@Override
	public SlcDoneFileVo addSlcDoneFileRecord(SlcDoneFileVo slcDoneFileVo) {
		SlcDoneFile slcDoneFile=new SlcDoneFile();
		try {
			Utils.copyProperties(slcDoneFile, slcDoneFileVo);
			Session session = sessionFactory.getCurrentSession();
			Transaction transaction = session.beginTransaction();
			session.save(slcDoneFile);
			session.flush();
			transaction.commit();
			return convertEntityToVo(slcDoneFile);			
		} catch (Exception e) {
			logger.error("Error in addSlcDoneFileRecord :: "+e.getMessage(),e);
			return null;
		}
	}

	// convert entity to vo
	private SlcDoneFileVo convertEntityToVo(SlcDoneFile slcDoneFile) {
		SlcDoneFileVo doneFileVo = null;
		try {
			if(slcDoneFile != null){
				doneFileVo=new SlcDoneFileVo();
				Utils.copyProperties(doneFileVo, slcDoneFile);
			}
			return doneFileVo;
		} catch (Exception e) {
			logger.error("Error in convertEntityToVo :: "+e.getMessage(),e);
			return null;
		}
	}

	//clear all ready records
	@Override
	public boolean clearAllCurrentAccessList() {
		String sql="update caf_status_tbl set currently_accessing_user_id = null, latest_ind = true, end_date = null " +
				"where currently_accessing_user_id is not null and latest_ind=false and end_date is not null";
		try {
			Session session = sessionFactory.getCurrentSession();
			Query query=session.createSQLQuery(sql);
			int result = query.executeUpdate();
			logger.info("SLCDoneDao :: clearAllCurrentAccessList :: "+result);
			return true;
		} catch (Exception e) {
			logger.error("SLCDoneDao :: clearAllCurrentAccessList :: "+e.getMessage(),e);
			return false;
		}
		
	}

}
