package speedoc.co.in.dao;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Cao;
import speedoc.co.in.vo.CaoVo;

@Repository
public class CaoDao implements ICaoDao {

	private static Logger log = Logger.getLogger(CaoDao.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	public CaoVo getCaoImageByCaoCode(String caoCode) {
		CaoVo caoVo = null;
		try {
			Cao cao = (Cao) sessionFactory.getCurrentSession()
					.createCriteria(Cao.class)
					.add(Restrictions.eq("caoCode", caoCode)).uniqueResult();
			if (cao != null) {
				caoVo = new CaoVo();
				BeanUtils.copyProperties(caoVo, cao);
			}

		} catch (Exception e) {
			log.error("Error in getCaoImage of CaoDao :" + e.getMessage(), e);
		}
		return caoVo;
	}
}
