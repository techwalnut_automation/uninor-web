package speedoc.co.in.dao;

import speedoc.co.in.vo.SlcRemarkVo;

public interface ISlcRemarkDao {
	public Long save(SlcRemarkVo slcRemarkVo ) throws Exception ;

	public boolean deleteSlcPool();
}
