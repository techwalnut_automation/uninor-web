package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.AuditPool;
import speedoc.co.in.domain.Caf;
import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.AuditPoolVo;
import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.UserVo;

@Repository
public class AuditDao implements IAuditDao {

	@Autowired
	SessionFactory sessionFactory;

	private static Logger logger = Logger.getLogger(AuditDao.class);

	@Override
	public CafStatusVo getSlcRejectedCafs(UserVo userVo) throws Exception {
//		String sql = "select cst.* from caf_status_tbl as cst left join "
//				+ "audit_pool_tbl as fpt on fpt.caf_tbl_id = cst.caf_tbl_id where cst.end_date is null "
//				+ "and cst.status_id ="
//				+ StatusCodeEnum.SCRUTINY_REJECT.getStatusCode()
//				+ " and cst.latest_ind is true and fpt.caf_tbl_id is null limit 1";
	
		// NEW QUERY ADDED ON 12-11-2014
		String sql = "select * from sp_get_caf_from_audit_pool('"
			+ StatusCodeEnum.SCRUTINY_REJECT.getStatusCode() +"','"+userVo.getId()+ "')"; 
		CafStatus cafStatus = (CafStatus) sessionFactory.getCurrentSession()
				.createSQLQuery(sql).addEntity(CafStatus.class).uniqueResult();
		if (cafStatus != null) {
			return convertToCafStatusVo(cafStatus);
		}
		return null;
	}

	public CafStatusVo convertToCafStatusVo(CafStatus cafStatus)
			throws Exception {
		CafStatusVo cafStatusVo = new CafStatusVo();
		Utils.copyProperties(cafStatusVo, cafStatus);
		return cafStatusVo;
	}

//	@Override
//	public Long save(AuditPoolVo auditPoolVo) {
//		try {
//			AuditPool auditPool = new AuditPool();
//			Utils.copyProperties(auditPool, auditPoolVo);
//			Session session = sessionFactory.getCurrentSession();
//			session.save(auditPool);
////			session.flush();
//			return auditPool.getId();
//		} catch (Exception e) {
//			logger.error("Error in save :: " + e.getMessage(), e);
//		}
//		return null;
//	}

	@Override
	public boolean delete(Long cafTblId) {
		logger.info("Deleting audit_pool_tbl by cafTblId = " + cafTblId);		
		try{
			String hql = "DELETE FROM AuditPool WHERE cafTblId = :cafTblId";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setParameter("cafTblId", cafTblId);
			int result = query.executeUpdate();
			logger.info("result :: "+result);
			return true;
		}catch (Exception e) {
			logger.error("delete :: "+e.getMessage(),e);
			return false;
		}
	}

	@Override
	public AuditPoolVo getAuditPoolByCafTblId(Long id) {
		try {
			AuditPoolVo auditPoolVo = new AuditPoolVo();
			AuditPool auditPool = (AuditPool) sessionFactory
					.getCurrentSession().createCriteria(AuditPool.class)
					.add(Restrictions.eq("cafTblId", id)).uniqueResult();
			if (auditPool != null) {
				BeanUtils.copyProperties(auditPoolVo, auditPool);
			}
			return auditPoolVo;
		} catch (Exception e) {
			logger.error("Error in getAuditPoolByCafTblId :: " + e.getMessage(), e);
		}
		return null;
	}

	@SuppressWarnings("deprecation")
	@Override
	public boolean deleteAuditPool() {
		try {
			Session session = sessionFactory.getCurrentSession();
			session.createSQLQuery(
					"DELETE FROM audit_pool_tbl WHERE start_date < (NOW() - INTERVAL '5 minute')")
					.addScalar("id", Hibernate.BIG_INTEGER)
					.addScalar("caf_tbl_id", Hibernate.BIG_INTEGER)
					.addScalar("start_date", Hibernate.TIMESTAMP)
					.addScalar("end_date", Hibernate.TIMESTAMP).executeUpdate();
			return true;
		} catch (Exception e) {
			logger.error("Error in deleteAuditPool() " + e.getMessage(), e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CafAuditVo> findSlcRejectData(Long cafTblId) {
		List<CafAuditVo> cafAuditVos = new ArrayList<CafAuditVo>();
		try {
			String sql = "select min(cst.created_date) as createdDate, min(ut.username) as username, "
					+ "array_to_string(array_agg(remark_tbl.reject_reason), ',') as rejectReason from caf_tbl ct "
					+ "join caf_status_tbl cst ON ct.id = cst.caf_tbl_id "
					+ "join user_tbl ut ON cst.created_by = ut.id "
					+ "join slc_remark_tbl srt ON cst.id = srt.caf_status_id "
					+ "join remark_tbl ON srt.remark_id = remark_tbl.id "
					+ "where cst.status_id = '"
					+ StatusCodeEnum.SCRUTINY_REJECT.getStatusCode()
					+ "' and cst.caf_tbl_id = '"
					+ cafTblId
					+ "' "
					+ "group by cst.created_date ";

			cafAuditVos = sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
					.addScalar("username", StandardBasicTypes.STRING)
					.addScalar("rejectReason", StandardBasicTypes.STRING)
					.setResultTransformer(
							Transformers.aliasToBean(CafAuditVo.class)).list();

			return cafAuditVos;
		} catch (Exception e) {
			logger.error("Error in findSlcRejectData :: " + e.getMessage(), e);
			return null;
		}

	}

	@Override
	public CafVo getCafData(long cafTblId) {
		CafVo cafVo = new CafVo();
		try {
			Caf caf = (Caf) sessionFactory.getCurrentSession()
					.createCriteria(Caf.class)
					.add(Restrictions.eq("id", cafTblId)).uniqueResult();
			if (caf != null) {
				Utils.copyProperties(cafVo, caf);
			}
			return cafVo;
		} catch (Exception e) {
			logger.error("Error in getCafData :: " + e.getMessage(), e);
			return null;
		}
	}

	@Override
	public CafStatusVo getCafStatusDataByCafTblId(Long cafTblId) {
		CafStatusVo cafStatusVo = new CafStatusVo();
		try {
			CafStatus cafStatus = (CafStatus) sessionFactory.getCurrentSession().createCriteria(CafStatus.class)
					.add(Restrictions.eq("cafTblId", cafTblId)).uniqueResult();
			if(cafStatus != null){
				Utils.copyProperties(cafStatusVo, cafStatus);				
			}
			return cafStatusVo;
		} catch (Exception e) {
			logger.error("Error in getCafStatusDataByCafTblId :: "+e.getMessage(),e);
			return null;
		}	
	}

}
