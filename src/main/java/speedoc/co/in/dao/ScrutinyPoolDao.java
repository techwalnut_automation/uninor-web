package speedoc.co.in.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Caf;
import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.domain.ScrutinyPool;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.ScrutinyPoolVo;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.StatusResultVo;
import speedoc.co.in.vo.UserVo;

@Repository
public class ScrutinyPoolDao implements IScrutinyPoolDao {

	@Autowired
	private SessionFactory sessionFactory;
	private static Logger logger = Logger.getLogger(ScrutinyPoolDao.class);

	public CafStatusVo getDECompletedCafs(UserVo userVo) throws Exception {

		// String sql = "select cst.* from caf_status_tbl as cst left join "
		// +
		// "slc_pool_tbl as fpt on fpt.caf_tbl_id = cst.caf_tbl_id where cst.end_date is null "
		// + "and cst.status_id ="+StatusCodeEnum.DE_COMPLETE.getStatusCode()
		// +" and cst.latest_ind is true and fpt.caf_tbl_id is null limit 1";

		String sql = "select * from sp_get_caf_from_slc_pool('"
				+ StatusCodeEnum.DE_COMPLETE.getStatusCode() + "','"
				+ userVo.getId() + "')"; // NEW QUERY ADDED ON 10-11-2014
		CafStatus cafStatus = (CafStatus) sessionFactory.getCurrentSession()
				.createSQLQuery(sql).addEntity(CafStatus.class).uniqueResult();

		if (cafStatus != null) {
			return convertToCafStatusVo(cafStatus);
		}
		return null;
	}

	public List<CafStatusVo> convertToFlcPoolVoList(
			List<CafStatus> cafStatusList) throws Exception {
		List<CafStatusVo> cafStatusVos = new ArrayList<CafStatusVo>();
		for (CafStatus cafStatus : cafStatusList) {
			cafStatusVos.add(convertToCafStatusVo(cafStatus));
		}
		return cafStatusVos;
	}

	public CafStatusVo convertToCafStatusVo(CafStatus cafStatus)
			throws Exception {
		CafStatusVo cafStatusVo = new CafStatusVo();
		Utils.copyProperties(cafStatusVo, cafStatus);
		return cafStatusVo;
	}

	public Long save(ScrutinyPoolVo scrutinyPoolVo) throws Exception {
		ScrutinyPool scrutinyPool = new ScrutinyPool();
		Utils.copyProperties(scrutinyPool, scrutinyPoolVo);
		Session session = sessionFactory.getCurrentSession();
		session.save(scrutinyPool);
		// session.flush() ;
		return scrutinyPool.getId();
	}

	@Override
	public boolean delete(Long cafTblId) {
		logger.info("Deleting slc_pool_tbl by cafTblId = " + cafTblId);
		try {
			String hql = "DELETE FROM SlcPool WHERE cafTblId = :cafTblId";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setParameter("cafTblId", cafTblId);
			int result = query.executeUpdate();
			logger.info("result :: " + result);
			return true;
		} catch (Exception e) {
			logger.error("delete :: " + e.getMessage(), e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchResultVo> getSlcSearchDataBySpeedocId(String speedocId) {
		String sql = "select ct.speedoc_id, ct.mobile_number, ct.caf_number, ct.first_name, ct.last_name, ct.id , cst.id as cst_id from caf_tbl as ct "
				+ "join caf_status_tbl cst on ct.id = cst.caf_tbl_id "
				+ "where cst.latest_ind=true AND cst.end_date is null AND speedoc_id='"
				+ speedocId + "'";
		try {
			List<Object> list = sessionFactory.getCurrentSession()
					.createSQLQuery(sql).list();
			return convertToSearchResultVoList(list);
		} catch (Exception e) {
			logger.error("getSlcSearchDataBySpeedocId :: " + e.getMessage(), e);
		}
		return null;
	}

	// Audit Search
	@SuppressWarnings("unchecked")
	@Override
	public List<SearchResultVo> getSlcSearchDataByCafNumber(String cafNumber) {
		String sql = "select ct.id As cafTblId,ct.speedoc_id AS speedocId, ct.mobile_number AS mobileNumber, ct.caf_number AS cafNumber, ct.first_name AS firstName, ct.last_name AS lastName , cst.id AS cafStatusId from caf_tbl as ct "
				+ "join caf_status_tbl cst on ct.id = cst.caf_tbl_id "
				+ "where cst.latest_ind=true AND cst.end_date is null AND caf_number='"
				+ cafNumber + "'";
		try {
			List<SearchResultVo> searchResultVos = sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("speedocId", StandardBasicTypes.STRING)
					.addScalar("mobileNumber", StandardBasicTypes.STRING)
					.addScalar("cafNumber", StandardBasicTypes.STRING)
					.addScalar("firstName", StandardBasicTypes.STRING)
					.addScalar("lastName", StandardBasicTypes.STRING)
					.addScalar("cafStatusId", StandardBasicTypes.LONG)
					// .addScalar("createdDate", StandardBasicTypes.STRING)
					// .addScalar("username", StandardBasicTypes.STRING)
					.addScalar("cafTblId", StandardBasicTypes.LONG)
					.setResultTransformer(
							Transformers.aliasToBean(SearchResultVo.class))
					.list();
			return searchResultVos;
		} catch (Exception e) {
			logger.error(
					"Error in getSlcSearchDataByCafNumber :: " + e.getMessage(),
					e);
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SearchResultVo> getSlcSearchDataByMobileNumber(
			String mobileNumber) {
		String sql = "select ct.speedoc_id, ct.mobile_number, ct.caf_number, ct.first_name, ct.last_name, ct.id , cst.id as cst_id from caf_tbl as ct "
				+ "join caf_status_tbl cst on ct.id = cst.caf_tbl_id "
				+ "where cst.latest_ind=true AND cst.end_date is null AND mobile_number='"
				+ mobileNumber + "'";
		try {
			List<Object> list = sessionFactory.getCurrentSession()
					.createSQLQuery(sql).list();

			return convertToSearchResultVoList(list);
		} catch (Exception e) {
			logger.error("getSlcSearchDataByMobileNumber :: " + e.getMessage(),
					e);
		}
		return null;
	}

	private List<SearchResultVo> convertToSearchResultVoList(List<Object> list)
			throws Exception {
		Iterator<Object> it = list.iterator();
		List<SearchResultVo> searchResultVos = new ArrayList<SearchResultVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			SearchResultVo searchResultVo = new SearchResultVo();
			searchResultVo.setSpeedocId((String) object[0]);
			searchResultVo.setMobileNumber((String) object[1]);
			searchResultVo.setCafNumber((String) object[2]);
			searchResultVo.setFirstName((String) object[3]);
			searchResultVo.setLastName((String) object[4]);
			searchResultVo.setCafTblId(Long.valueOf(object[5].toString()));
			searchResultVo.setCafStatusId(Long.valueOf(object[6].toString()));
			// searchResultVo.setUsername((String) object[7]);
			searchResultVos.add(searchResultVo);
		}
		return searchResultVos;
	}

	@Override
	public boolean deleteFromSlcPoolTbl(Long cafTblId) {
		try {
			String hql = "DELETE FROM ScrutinyPool WHERE cafTblId = :cafTblId";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setParameter("cafTblId", cafTblId);
			int result = query.executeUpdate();

			logger.info("result :: " + result);
			return true;
		} catch (Exception e) {
			logger.error("Error in deleteFromSlcPoolTbl :: " + e.getMessage(),
					e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<StatusResultVo> getStatusList(StatusResultVo statusResultVo) {
		// String sql =
		// "SELECT cst.id as cafStatusId, cst.caf_tbl_id as cafTblId, st.status_name as status, rt.reject_reason as remark, ut.username as createdBy, cst.created_date as createdDate FROM caf_status_tbl cst "
		// +
		// "left join slc_remark_tbl srt on srt.caf_status_id=cst.id " +
		// "left join remark_tbl rt on rt.id=srt.remark_id " +
		// "left join audit_remark_tbl art on art.remark_id=rt.id " +
		// "join status_tbl st on st.status_id=cst.status_id " +
		// "join user_tbl ut on ut.id=cst.created_by " +
		// "where cst.caf_tbl_id = "+statusResultVo.getCafTblId()+" order by cst.id ";
		String sql = "SELECT caf_status_tbl.id as cafStatusId, caf_status_tbl.caf_tbl_id as cafTblId, status_tbl.status_name as status, "
				+ "user_tbl.username as createdBy, caf_status_tbl.created_date as createdDate, max( "
				+ " CASE "
				+ "WHEN status_tbl.status_id =3001 OR status_tbl.status_id = 3002 THEN flc_remark.reject_reason "
				+ "ELSE NULL END) as remark, max( "
				+ "CASE "
				+ "WHEN status_tbl.status_id= 5001 OR status_tbl.status_id= 5002 THEN slc_remark.reject_reason "
				+ "ELSE NULL END) as remark, max( "
				+ "CASE "
				+ "WHEN status_tbl.status_id= 7001 OR status_tbl.status_id = 7002 THEN audit_remark.reject_reason "
				+ "ELSE NULL END) as remark  FROM caf_status_tbl "
				+ "JOIN status_tbl ON status_tbl.status_id = caf_status_tbl.status_id "
				+ "LEFT JOIN flc_remark_tbl ON flc_remark_tbl.caf_status_id = caf_status_tbl.id "
				+ "LEFT JOIN slc_remark_tbl ON slc_remark_tbl.caf_status_id = caf_status_tbl.id "
				+ "LEFT JOIN audit_remark_tbl ON audit_remark_tbl.caf_status_id = caf_status_tbl.id "
				+ "LEFT JOIN remark_tbl flc_remark ON flc_remark.id = flc_remark_tbl.remark_id "
				+ "LEFT JOIN remark_tbl slc_remark ON slc_remark.id = slc_remark_tbl.remark_id "
				+ "LEFT JOIN remark_tbl audit_remark ON audit_remark.id = audit_remark_tbl.remark_id "
				+ "JOIN user_tbl ON user_tbl.id = caf_status_tbl.created_by "
				+ "WHERE caf_status_tbl.caf_tbl_id = "
				+ statusResultVo.getCafTblId()
				+ " "
				+ "GROUP BY caf_status_tbl.id, caf_status_tbl.caf_tbl_id, status_tbl.status_name, user_tbl.username "
				+ "ORDER BY caf_status_tbl.id;";

		try {
			List<StatusResultVo> statusResultVos = sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("cafStatusId", StandardBasicTypes.LONG)
					.addScalar("cafTblId", StandardBasicTypes.LONG)
					.addScalar("status", StandardBasicTypes.STRING)
					.addScalar("createdBy", StandardBasicTypes.STRING)
					.addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
					.addScalar("remark", StandardBasicTypes.STRING)
					.setResultTransformer(
							Transformers.aliasToBean(StatusResultVo.class))
					.list();
			logger.info("ScrutinyPoolDao :: getStatusList.size() :: "
					+ statusResultVos.size());
			return statusResultVos;
		} catch (Exception e) {
			logger.error("Error in getStatusList :: " + e.getMessage(), e);
			return null;
		}

	}

	@Override
	public CafVo getCafData(Long cafTblId) {
		CafVo cafVo = null;
		try {
			Caf caf = (Caf) sessionFactory.getCurrentSession()
					.createCriteria(Caf.class)
					.add(Restrictions.eq("id", cafTblId)).uniqueResult();
			if (caf != null) {
				cafVo = new CafVo();
				Utils.copyProperties(cafVo, caf);
			}
			return cafVo;
		} catch (Exception e) {
			logger.error("Error in getCafData :: " + e.getMessage(), e);
			return null;
		}

	}

	@Override
	public boolean cancelCaf(Long cafTblId) {
		String sql = "UPDATE caf_status_tbl SET currently_accessing_user_id = null,latest_ind=true, end_date= null where caf_tbl_id="
				+ cafTblId
				+ " "
				+ "and currently_accessing_user_id is not null and latest_ind=false and end_date is not null";
		try {
			int result = sessionFactory.getCurrentSession().createSQLQuery(sql)
					.executeUpdate();
			logger.info("cancelCaf Rows affected: " + result);
			return true;
		} catch (Exception e) {
			logger.error("Error in cancelCaf :: " + e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean cafNotDeComplete(Long id) {
		String sql = "select count(*) from caf_status_tbl where status_id IN (4001) "
				+ "and latest_ind =false and end_date is not null and caf_tbl_id="
				+ id + "";
		BigInteger count;

		try {
			Query query = sessionFactory.getCurrentSession()
					.createSQLQuery(sql);
			count = (BigInteger) query.uniqueResult();
			Long count1 = count.longValue();
			logger.info("cafNotDeComplete count1 = " + count1);
			if (count1 > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error("Error in cafNotDeComplete :: " + e.getMessage(), e);
			return false;
		}
	}

	@Override
	public IndexingDataVO getElectroData(String poiref) {
		try {
			String sql = "SELECT voter_id as voterId, polling_station as pollingStation from electro_data_tbl where voter_id='" + poiref + "'";
			IndexingDataVO electroDataVo=(IndexingDataVO) sessionFactory.getCurrentSession().createSQLQuery(sql)
					.addScalar("voterId", StandardBasicTypes.STRING)
					.addScalar("pollingStation", StandardBasicTypes.STRING)
					.setResultTransformer(Transformers.aliasToBean(IndexingDataVO.class))
					.uniqueResult();
			return electroDataVo;
		} catch (Exception e) {
			logger.error("Error in getElectroData :: "+e.getMessage(),e);
			return null;
		}
	}
}
