package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.FilterVo;

public interface IActivationOfficerDao {
	public CaoVo saveActivationOfficer(CaoVo activationOfficerVo);
	public List<CaoVo> getActivationList(
			FilterVo<CaoVo> filterVo);
	public Boolean isCaoCodeAlreadyExist(String caoCode);
	public Boolean isCaoCodeAlreadyExist(String caoCode, int activeStatus);   // 18-12-2014	
	public CaoVo getSignatureById(Long id);
	public Long getActivationCount();
	public boolean changeCaoStatus(Long id, int status);  // 18-12-2014
	
}
