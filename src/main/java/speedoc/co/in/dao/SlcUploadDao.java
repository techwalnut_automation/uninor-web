package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.domain.SlcData;
import speedoc.co.in.domain.SlcImport;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.SlcDataVo;
import speedoc.co.in.vo.SlcImportVo;

@Repository
public class SlcUploadDao implements ISlcUploadDao {

	@Autowired
	SessionFactory sessionFactory;

	private static Logger logger = Logger.getLogger(SlcUploadDao.class);

	@Override
	public Long getSlcCount() {
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(SlcImport.class)
					.setProjection(Projections.rowCount()).uniqueResult();
			logger.info("count :: " + count);
		} catch (Exception e) {
			logger.error("Error in getSlcCount of class SlcUploadDao"+ e.getMessage(), e);
		}
		return count;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SlcImportVo> getSlcList(FilterVo<SlcImportVo> filterVo) {
		List<SlcImportVo> slcImportVos = new ArrayList<SlcImportVo>();
		try {
			String sql = "SELECT slc_import_tbl.id as id, slc_import_tbl.file_name as fileName,slc_import_tbl.total_record as totalRecord, "
					+ "slc_import_tbl.record_updated as recordUpdated, slc_import_tbl.duplicate_record as duplicateRecord, slc_import_tbl.status as status, "
					+ "slc_import_tbl.created_date as createdDate,user_tbl.username as username FROM slc_import_tbl "
					+ "JOIN user_tbl on slc_import_tbl.created_by = user_tbl.id "
					+ "ORDER BY slc_import_tbl.created_date DESC";

			if (filterVo != null) {
				filterVo.setPageNumber((filterVo.getPageNumber() - 1)
						* filterVo.getRowsPerPage());
			}
			slcImportVos = sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("id", StandardBasicTypes.LONG)
					.addScalar("fileName", StandardBasicTypes.STRING)
					.addScalar("totalRecord", StandardBasicTypes.INTEGER)
					.addScalar("recordUpdated", StandardBasicTypes.INTEGER)
					.addScalar("duplicateRecord", StandardBasicTypes.INTEGER)
					.addScalar("status", StandardBasicTypes.STRING)
					.addScalar("createdDate", StandardBasicTypes.TIMESTAMP)
					.addScalar("username", StandardBasicTypes.STRING)
					.setResultTransformer(Transformers.aliasToBean(SlcImportVo.class))
					.setFirstResult(filterVo.getPageNumber())
					.setMaxResults(filterVo.getRowsPerPage()).list();
			return slcImportVos;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Boolean isSlcAlreadyDone(Long cafTblId) {
		Long count = null;

		try {
			
			List statusId = new ArrayList();
			statusId.add(5001);
			statusId.add(5002);
			statusId.add(5003);
			statusId.add(2002);
			statusId.add(2003);
			statusId.add(2001);
			statusId.add(3001);
			statusId.add(3002);
			statusId.add(3003);
			statusId.add(6001);
			statusId.add(6002);
			statusId.add(6003);
			statusId.add(7001);
			statusId.add(7002);

			List latestInd = new ArrayList();
			latestInd.add(true);
			// latestInd.add(false);

			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(CafStatus.class)
					.add(Restrictions.eq("cafTblId", cafTblId))
					.add(Restrictions.in("statusId", statusId))
					.add(Restrictions.isNull("endDate"))
					.add(Restrictions.in("latestInd", latestInd))
					.setProjection(Projections.rowCount()).uniqueResult();

			logger.info("isSlcAlreadyDone :: count ::" + count);

			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			logger.error(
					"Erron in isCaoCodeAlreadyExist method " + e.getMessage(),
					e);
			return false;
		}
	}

	@Override
	public CafStatusVo getAllStatusVo(String speedocId) {
		try {
			String sql = "select  min(cst.caf_tbl_id) as cafTblId from caf_status_tbl cst "
					+ "join caf_tbl ct on ct.id = cst.caf_tbl_id "
					+ "where ct.speedoc_id = '" + speedocId + "'";
			CafStatusVo cafStatusVo = (CafStatusVo) sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("cafTblId", StandardBasicTypes.LONG)
					.setResultTransformer(
							Transformers.aliasToBean(CafStatusVo.class))
					.uniqueResult();
			return cafStatusVo;
		} catch (Exception e) {
			logger.error("Error in getAllStatusVo :: " + e.getMessage(), e);
		}
		return null;
	}

	@Override
	public SlcImportVo addSlcImportRecord(SlcImportVo slcImportVo) {
		SlcImport slcImport = new SlcImport();
		try {
			Utils.copyProperties(slcImport, slcImportVo);
			Session session = sessionFactory.getCurrentSession();
			session.save(slcImport);
			session.flush();
			if (slcImport != null) {
				SlcImportVo slcImportVo2 = new SlcImportVo();

				Utils.copyProperties(slcImportVo2, slcImport);
				return slcImportVo2;
			}
		} catch (Exception e) {
			logger.error("addSlcImportRecord :: SlcUploadDao :: " + e);
		}
		return slcImportVo;
	}

	@Override
	public CafStatusVo getAllCafStatusData(String speedocId) {
		try {
			String sql = "select cst.id as id, cst.caf_tbl_id as cafTblId, cst.created_by as createdBy, cst.status_id as statusId,  "
					+ "cst.end_date as endDate, cst.latest_ind as latestInd from caf_status_tbl cst "
					+ "join caf_tbl ct on ct.id = cst.caf_tbl_id "
					+ "where cst.status_id ='"
					+ StatusCodeEnum.DE_COMPLETE.getStatusCode()
					+ "' and cst.end_date is null and cst.latest_ind =false and ct.speedoc_id = '"
					+ speedocId + "'";
			CafStatusVo cafStatusVo = (CafStatusVo) sessionFactory
					.getCurrentSession()
					.createSQLQuery(sql)
					.addScalar("id", StandardBasicTypes.LONG)
					.addScalar("cafTblId", StandardBasicTypes.LONG)
					.addScalar("createdBy", StandardBasicTypes.LONG)
					.addScalar("statusId", StandardBasicTypes.INTEGER)
					.addScalar("endDate", StandardBasicTypes.TIMESTAMP)
					.addScalar("latestInd", StandardBasicTypes.BOOLEAN)
					.setResultTransformer(
							Transformers.aliasToBean(CafStatusVo.class))
					.uniqueResult();
			return cafStatusVo;
		} catch (Exception e) {
			logger.info("" + e.getMessage(), e);
		}
		return null;
	}

	@Override
	public Boolean updateDEStatus(Long id) {
		try {
			String sql = "update caf_status_tbl SET latest_ind = true where caf_status_tbl.status_id ='"
					+ StatusCodeEnum.DE_COMPLETE.getStatusCode()
					+ "' "
					+ "and caf_status_tbl.end_date is null and caf_status_tbl.latest_ind = false and caf_status_tbl.id = '"
					+ id + "'";

			Session session = sessionFactory.getCurrentSession();
			int cafStatusVo = session.createSQLQuery(sql).executeUpdate();
			if (cafStatusVo > 0) {
				return true;
			} else {
				return false;
			}

		} catch (Exception e) {
			logger.error("updateDEStatus :: SlcUploadDao ::" + e);
			return false;
		}
	}

	@Override
	public Boolean updateSlcImportByImportId(Integer recordUpdated,
			Integer duplicateRecord, Long slcImportId) {
		try {
			String hql = "UPDATE SlcImport SET duplicateRecord=?, status=? "
				+ "WHERE id=?";
			Session session = sessionFactory.getCurrentSession();
			session.createQuery(hql)
					.setInteger(0, duplicateRecord).setString(1, "COMPLETE")
					.setLong(2, slcImportId).executeUpdate();
			return true;
		} catch (Exception e) {
			logger.error("updateSlcImportByImportId :: SlcUploadDao ::"+ e.getMessage(), e);
			return false;
		}
	}

	@Override
	public SlcDataVo addNewSlcDataNumber(SlcDataVo slcDataVo) {
		try {
			SlcData slcData = new SlcData();
			Utils.copyProperties(slcData, slcDataVo);
			Session session = sessionFactory.getCurrentSession();
			session.save(slcData);
			session.flush();
			if (slcData != null) {
				SlcDataVo slcDataVo2 = new SlcDataVo();
				Utils.copyProperties(slcDataVo2, slcData);
				return slcDataVo2;
			}
		} catch (Exception e) {
			logger.error("Error in addNewSlcDataNumber :: "+e.getMessage(),e);
		}
		return null;
	}

	@Override
	public Long getSlcDataCountBySpeedocId(String speedocId) {
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(SlcData.class)
					.add(Restrictions.eq("speedocId", speedocId))
					.setProjection(Projections.rowCount()).uniqueResult();
		} catch (Exception e) {
			logger.error("getSlcDataCountBySpeedocId :: slcUploadDao :: " + e.getMessage(),e);
		}
		return count;
	}

	// upload csv to slc_data_tbl	
	@Override
	public void addSlcDataFile(String slcFilePath) {
		try{
			String sql = "COPY slc_data_tbl (mobile_number, caf_number, speedoc_id) FROM '"+slcFilePath+"' DELIMITER ',' CSV HEADER ;";
			int executeUpdate = sessionFactory.getCurrentSession().createSQLQuery(sql).executeUpdate();
			logger.info("addSlcDataFile executeUpdate :: "+executeUpdate);
		}catch (Exception e) {
			logger.error("Error in addSlcDataFile :: "+e.getMessage(),e);
		}
	}
	
	@Override
	public Integer uploadSlcFile() {	
		try{
			logger.info("uploadSlcFile :: slcUploadDao ");
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createSQLQuery("select update_latest_ind_fn()");
			 @SuppressWarnings("rawtypes")
			List list = query.list();
	        logger.info("uploadSlcFile :: "+query );
	        logger.info(" uploadSlcFile :: list :: "+list.size());
	        return list.size();
		}catch (Exception e) {
			logger.error("Error in uploadSlcFile :: "+e.getMessage(),e);
			return null;
		}
	}

	@Override
	public Boolean updateSlcImortId(Long slcImportId) {
		try {
			String hql = "UPDATE SlcData SET slcImportId=? WHERE status=?";
			Session session = sessionFactory.getCurrentSession();
			session.createQuery(hql).setLong(0, slcImportId)
					.setLong(1, 0).executeUpdate();
			return true;
		} catch (Exception e) {
			logger.error("updateSlcImportByImportId :: SlcUploadDao ::"+ e.getMessage(), e);
			return false;
		}
	}
}
