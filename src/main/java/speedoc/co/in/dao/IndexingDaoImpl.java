package speedoc.co.in.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StandardBasicTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.domain.ElectroData;
import speedoc.co.in.domain.Indexing;
import speedoc.co.in.domain.ReverseIndexSummary;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.IndexingVo;
import speedoc.co.in.vo.ScrutinyCaptureVo;
import speedoc.co.in.vo.UserSummaryVo;

@Repository
@Transactional
public class IndexingDaoImpl implements IndexingDao {

	private static Logger log = Logger.getLogger(IndexingDaoImpl.class
			.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Integer saveIndexData(final IndexingVo indexingVo) {
		Indexing indexing = new Indexing();
		IndexingVo savedIndexingVo = null;
		int resultCode = 0;
		try {
			Utils.copyProperties(indexing, indexingVo);
			Session session = sessionFactory.getCurrentSession();		
			session.save(indexing);
			if (indexing != null) {
				savedIndexingVo = new IndexingVo();
				Utils.copyProperties(savedIndexingVo, indexing);
			}
			resultCode = 0;
		} catch (DuplicateKeyException dup) {
			log.error("dup " + dup.getMessage());
			resultCode = 1;
		} catch (DataIntegrityViolationException e) {
			log.error("DataIntegrityViolationException " + e.getMessage());
		} catch (ConstraintViolationException e) {
			log.error("ConstraintViolationException " + e.getMessage());
		} catch (Exception e) {
			log.error("Exception ::  " + e.getMessage());
			resultCode = 2;
		}
		return resultCode;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IndexingVo> findIndexDataByStatus(Integer indexingStatusCode) {
		log.info("Inside getNewIndexData... " + indexingStatusCode);
		try {
			Session session = sessionFactory.getCurrentSession();
			List<Indexing> indexings = session.createCriteria(Indexing.class)
					.add(Restrictions.eq("status", indexingStatusCode))
					.addOrder(Order.desc("createdDate")).setMaxResults(5000)
					.list();
			if (indexings != null) {
				List<IndexingVo> indexList = convertIndexingVoList(indexings);
				return indexList;
			}
		} catch (Exception e) {
			log.error("Error in checkIndexData ::: " + e.getMessage());
			return null;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<IndexingVo> findNewIndexData(Integer indexingStatusCode) {
		log.info("Inside findNewIndexData... " + indexingStatusCode);
		try {
			Session session = sessionFactory.getCurrentSession();
			String sql = "select * from sp_get_new_indexing_data("
					+ indexingStatusCode + ")";

			List<Indexing> indexings = (List<Indexing>) session
					.createSQLQuery(sql).addEntity(Indexing.class).list();

			if (indexings != null) {
				List<IndexingVo> indexList = convertIndexingVoList(indexings);
				return indexList;
			}
		} catch (Exception e) {
			log.error("Error in findNewIndexData ::: " + e.getMessage());
			return null;
		}
		return null;
	}

	private List<IndexingVo> convertIndexingVoList(List<Indexing> indexings) {
		ArrayList<IndexingVo> indexingVos = new ArrayList<IndexingVo>();
		for (Indexing indexing : indexings) {
			IndexingVo indexingVo = new IndexingVo();
			try {
				Utils.copyProperties(indexingVo, indexing);
			} catch (Exception e) {
				log.error(e);
			}
			indexingVos.add(indexingVo);
		}
		return indexingVos;
	}

	@Override
	public CafStatusVo searchCafTblForNewMobileIndexData(String mobileNumber,
			String cafNumber) throws Exception {
		log.info("Inside searchCafTbl :: MobileNumber = " + mobileNumber
				+ "  String cafNumber :: " + cafNumber);

		// String sql = "SELECT caf_status_tbl.* from caf_status_tbl "
		// + " JOIN caf_tbl ON caf_status_tbl.caf_tbl_id = caf_tbl.id "
		// + " WHERE caf_tbl.mobile_number = '"+ mobileNumber+ "' AND "
		// + " caf_status_tbl.status_id ="+
		// StatusCodeEnum.AVAM_SUCCESS.getStatusCode()
		// + " AND caf_status_tbl.latest_ind=true AND end_date is NULL LIMIT 1";
		String sql = "SELECT caf_status_tbl.* from caf_status_tbl "
				+ " JOIN caf_tbl ON caf_status_tbl.caf_tbl_id = caf_tbl.id "
				+ " WHERE caf_tbl.mobile_number = '" + mobileNumber + "' AND "
				+ " caf_status_tbl.status_id ="
				+ StatusCodeEnum.AVAM_SUCCESS.getStatusCode()
				+ " AND caf_status_tbl.latest_ind=true AND end_date is NULL "
				+ "ORDER BY created_date DESC LIMIT 1";
		CafStatusVo cafStatusVo = null;

		Session session = sessionFactory.getCurrentSession();
		CafStatus cafStatus = (CafStatus) session.createSQLQuery(sql)
				.addEntity(CafStatus.class).uniqueResult();
		if (cafStatus != null) {
			cafStatusVo = convertToCafStatusVo(cafStatus);
		}
		return cafStatusVo;
	}

	private CafStatusVo convertToCafStatusVo(CafStatus cafStatus)
			throws Exception {
		CafStatusVo cafStatusVo = new CafStatusVo();
		Utils.copyProperties(cafStatusVo, cafStatus);
		return cafStatusVo;
	}

	@Override
	public void updateIndexingTbl(Long id, Integer count, Integer status,
			Long cafTblId) {

		Session session = sessionFactory.getCurrentSession();
		if (status == StatusCodeEnum.NEW.getStatusCode()) {
			String queryNewHql = "UPDATE Indexing SET searchCount = " + count
					+ " WHERE id = " + id;
			int result = session.createQuery(queryNewHql).executeUpdate();
			log.info("updating NEW Count result ==" + result);
		} else {
			String queryFoundHql = "UPDATE Indexing SET status = " + status
					+ ", cafTblId = " + cafTblId + ", searchCount = " + count
					+ " WHERE id = " + id;
			int result = session.createQuery(queryFoundHql).executeUpdate();
			log.info("updating NEW as FOUND result ==" + result);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	public List<ScrutinyCaptureVo> getScrutinyCompletedRecords() {
		log.info("inside IndexingDao :: getScrutinyCompletedRecords. ");

		String sql = "SELECT indx.id AS index_id, indx.caf_tbl_id AS caf_tbl_id, indx.mobile_number AS mobile_number, "
				+ " cst.status_id AS status_id FROM indexing_tbl AS indx "
				+ " JOIN caf_status_tbl AS cst ON cst.caf_tbl_id=indx.caf_tbl_id  AND cst.status_id IN ("
				+ StatusCodeEnum.SCRUTINY_COMPLETE.getStatusCode()
				+ ") "
				+ " WHERE indx.status = "
				+ StatusCodeEnum.FOUND.getStatusCode()
				+ " AND indx.caf_tbl_id IS NOT NULL limit 2000 ";

		List scrutinyCompletedList = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			scrutinyCompletedList = session.createSQLQuery(sql).list();
			return convertToScrutinyCaptureVoList(scrutinyCompletedList);
		} catch (Exception e) {
			log.error("getScrutinyCompletedRecords :: " + e.getMessage(), e);
			return null;
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<ScrutinyCaptureVo> convertToScrutinyCaptureVoList(
			List scrutinyCompletedList) {
		Iterator<Object> it = scrutinyCompletedList.iterator();
		List<ScrutinyCaptureVo> scrutinyCaptureVos = new ArrayList<ScrutinyCaptureVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			ScrutinyCaptureVo scrutinyCaptureVo = new ScrutinyCaptureVo();
			BigInteger indxId = (BigInteger) object[0];
			scrutinyCaptureVo.setIndexId(indxId.intValue());
			BigInteger cafTblId = (BigInteger) object[1];
			scrutinyCaptureVo.setCafTblId(cafTblId.longValue());
			scrutinyCaptureVo.setMobileNumber((String) object[2]);
			scrutinyCaptureVo.setStatusId((Integer) object[3]);
			scrutinyCaptureVos.add(scrutinyCaptureVo);
		}
		return scrutinyCaptureVos;
	}

	@Override
	public String saveStatusToIndexingTbl(final int status, final Long cafTblId) {
		log.info("inside IndexingDAO :: saveStatusToIndexingTbl :: updating  status = "
				+ status);
		final String hql = " UPDATE Indexing set status = ? where cafTblId = ?";
		try {
			Session session = sessionFactory.getCurrentSession();
			session.createQuery(hql).setInteger(0, status).setLong(1, cafTblId)
					.executeUpdate();
			return "success";
		} catch (Exception e) {
			log.error("saveStatusToIndexingTbl :: " + e.getMessage(), e);
			return "failure";
		}
	}

	@Override
	public UserSummaryVo getUserToAllot() {
		log.info(" inside IndexingDAO :: getUserToAllot()");
		String sql = "SELECT riut.id AS user_id, riut.username AS user_name FROM reverse_index_user_tbl AS riut "
				+ "LEFT JOIN reverse_index_summary_tbl AS rist ON rist.index_user_id = riut.id "
				+ "AND date(created_date) = date(NOW()) "
				+ "WHERE rist.index_user_id IS NULL LIMIT 1";
		UserSummaryVo userSummaryVO = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			Object[] object = (Object[]) session.createSQLQuery(sql)
					.uniqueResult();
			if (object != null) {
				userSummaryVO = new UserSummaryVo();
				BigInteger userId = (BigInteger) object[0];
				userSummaryVO.setUserId(userId.intValue());
				userSummaryVO.setUserName((String) object[1]);
			}
			return userSummaryVO;
		} catch (Exception e) {
			log.error(" getUserToAllot :: " + e.getMessage(), e);
			return null;
		}
	}

	@Override
	public Integer insertUserIntoSummary(final UserSummaryVo userSummaryVO) {
		log.info("inside IndexingDAO :: insertUserIntoSummary  inserting for userID :: "
				+ userSummaryVO.getUserId());
		ReverseIndexSummary reverseIndexSummary = new ReverseIndexSummary();
		try {
			reverseIndexSummary.setIndexUserId(userSummaryVO.getUserId());
			reverseIndexSummary.setScrutinyCount(0);
			Session session = sessionFactory.getCurrentSession();
			session.save(reverseIndexSummary);
			return reverseIndexSummary.getSummaryId();
		} catch (Exception e) {
			log.error("OperationalUnitDaoImpl :: saveOperationUnit :: "
					+ e.getMessage());
			return null;
		}
	}

	@Override
	public void updateScrutinySummaryTbl(Integer allotedToUserSummaryId) {
		log.info("inside IndexingDAO :: updateScrutinySummaryTbl() increment on summaryId :: "
				+ allotedToUserSummaryId);
		String hql = "UPDATE ReverseIndexSummary ris set ris.scrutinyCount = ris.scrutinyCount+1 where ris.summaryId = :summaryId";
		try {
			Session session = sessionFactory.getCurrentSession();
			int result = session.createQuery(hql)
					.setInteger("summaryId", allotedToUserSummaryId)
					.executeUpdate();
			log.info("Updated scrutiny count done :: result = " + result);
		} catch (Exception e) {
			log.error("updateScrutinySummaryTbl ::  " + e.getMessage());
		}
	}

	@Override
	public Integer updateIndexingStatus() {
		try {
			String sql = "update indexing_tbl SET status = "
					+ StatusCodeEnum.INDEXING_COMPLETE.getStatusCode()
					+ " where status = "
					+ StatusCodeEnum.FOUND.getStatusCode()
					+ " and created_date < (select (current_timestamp - INTERVAL '10 DAYS'))";
			Session session = sessionFactory.getCurrentSession();
			int result = session.createSQLQuery(sql).executeUpdate();
			log.info("" + result);
			return result;
		} catch (Exception e) {
			log.error(
					"Error in indexingDao :: updateIndexingStatus :: "
							+ e.getMessage(), e);
		}
		return null;
	}

	@Override
	public IndexingVo getElectroData() {
		try {
			String sql = "SELECT poiref from indexing_tbl where poi like 'Voter%' and electro_chitti_status='0' limit 1";
//			String sql = "SELECT poiref from electro_pool_tbl where poi like 'Voter%' and electro_chitti_status='0' limit 1";

			IndexingVo indexingVo = (IndexingVo) sessionFactory.getCurrentSession().createSQLQuery(sql)
					.addScalar("poiref", StandardBasicTypes.STRING)
					.setResultTransformer(Transformers.aliasToBean(IndexingVo.class))
					.uniqueResult();
			return indexingVo;
		} catch (Exception e) {
			log.error(
					"Error in indexingDaoImpl :: getElectroData :: "+ e.getMessage(), e);
			return null;
		}

	}

	@Override
	public Integer saveElectroData(IndexingDataVO indexingDataVO) {
		ElectroData electroData = new ElectroData();
		Long id=null;
		Integer resultCode=0;
		try {
			Utils.copyProperties(electroData, indexingDataVO);
			Session session = sessionFactory.getCurrentSession();
			System.out.println("electroData :"+electroData);
			System.out.println("indexingDataVO :"+indexingDataVO);
				 session.save(electroData);
				 resultCode=0;
		} catch (DuplicateKeyException dup) {
			log.error("dup " + dup.getMessage());
			resultCode=1;
		} catch (DataIntegrityViolationException e) {
			log.error("DataIntegrityViolationException " + e.getMessage());
		} catch (ConstraintViolationException e) {
			log.error("ConstraintViolationException " + e.getMessage());
		} catch (Exception e) {
			log.error("Exception ::  " + e.getMessage());
		}
		return resultCode;
	}

	@Override
	public Integer updateIndexingTbl(IndexingDataVO indexingDataVO) {
		try {
			String sql = "update indexing_tbl SET electro_chitti_status = '1'"
					
					+ " where poiref = '"
					+ indexingDataVO.getVoterId() +" '  or poiref = '"
					+ indexingDataVO.getVoterId() +"'";
			
			Session session = sessionFactory.getCurrentSession();
			int result = session.createSQLQuery(sql).executeUpdate();
			log.info("" + result);
			return result;
		} catch (Exception e) {
			log.error(
					"Error in indexingDao :: updateIndexingStatus :: "
							+ e.getMessage(), e);
		}
		return null;
	}

	@Override
	public Integer updateIndexingNOTSaveTbl(IndexingDataVO indexingDataVO) {
		try {
			String sql = "update indexing_tbl SET electro_chitti_status = '2'"
					
					+ " where poiref = '"
					+ indexingDataVO.getVoterId() +" '  or poiref = '"
					+ indexingDataVO.getVoterId() +"'";
			
			Session session = sessionFactory.getCurrentSession();
			int result = session.createSQLQuery(sql).executeUpdate();
			log.info("" + result);
			return result;
		} catch (Exception e) {
			log.error(
					"Error in indexingDao :: updateIndexingStatus :: "
							+ e.getMessage(), e);
		}
		return null;
	}
}