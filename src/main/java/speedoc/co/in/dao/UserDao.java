package speedoc.co.in.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.domain.Role;
import speedoc.co.in.domain.User;
import speedoc.co.in.domain.UserRole;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;

@Repository
@Transactional
public class UserDao implements IUserDao {
	private Logger log = Logger.getLogger(UserDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private IRolesDao rolesDao;

	@SuppressWarnings("unchecked")
	public UserVo findByUsername(String username) throws Exception {
		log.info("Inside UserDaoImpl---->findByUsername()::username="
				+ username);
		UserVo userVo = null;
		try {
			User user = (User) sessionFactory.getCurrentSession()
					.createCriteria(User.class)
					.add(Restrictions.eq("username", username)).uniqueResult();
			if (user != null) {
				userVo = convertToUserVo(user);
			}
			String hql = "SELECT r FROM Role AS r,User AS u,UserRole AS ur "
					+ "WHERE ur.userId = u.id AND r.id = ur.roleId AND u.username=?";
			if (userVo != null) {
				List<Role> roleList = sessionFactory.getCurrentSession()
						.createQuery(hql).setString(0, username).list();
				if (roleList != null) {
					List<RoleVo> roleVoList = RolesDao
							.convertToRoleVoList(roleList);
					userVo.setRoleList(roleVoList);
				}
			}
		} catch (Exception e) {
			log.error(
					"Error in findByUsername of class UserDao::"
							+ e.getMessage(), e);
		}
		return userVo;
	}

	public UserVo findById(Long userId) {
		log.info("Inside UserDaoImpl---->findById()::userId=" + userId);
		UserVo userVO = null;
		try {
			User user = (User) sessionFactory.getCurrentSession().get(User.class, userId);
			if (user != null) {
				userVO = convertToUserVo(user);
			}
		} catch (Exception e) {
			log.error("Error in findById of class UserDao::" + e);
		}
		log.info("Inside UserDaoImpl---->findById():: userId=" + userId   + "  UserVO :: " + userVO);
		return userVO;
	}

	public UserVo findByUniqueUserName(String username) {

		log.info("Inside UserDaoImpl---->findByUserName()::username="+ username);
		UserVo userVO = null ;
		try{
		 User user = (User) sessionFactory.getCurrentSession()
		 .createCriteria(User.class)
		 .add(Restrictions.eq("username", username)).uniqueResult();
		if(user != null){
			userVO = convertToUserVo(user);
		}

		} catch (Exception e) {
			log.error("Error in findByUniqueUserName ::" + e.getMessage(), e);
		}
		return userVO;
	}

	public static UserVo convertToUserVo(User user) throws Exception {
		UserVo userVo = new UserVo();

		Utils.copyProperties(userVo, user);

		return userVo;
	}

	public static List<UserVo> convertToUserVoList(List<User> userList)
			throws Exception {
		ArrayList<UserVo> userVos = new ArrayList<UserVo>();
		for (User user : userList) {
			UserVo userVo = convertToUserVo(user);
			userVos.add(userVo);
		}
		return userVos;
	}
 
	   public UserVo saveOrUpdate(UserVo userVo,String prevOption) throws Exception {
		 User user = new User() ;

			Utils.copyProperties(user, userVo);
			if (userVo.getId() == null) {
				Session session = sessionFactory.getCurrentSession();
				session.save(user);
				session.flush();
				UserRole userRole = new UserRole();
				RoleVo roleVo = new RoleVo();
				if (prevOption.equalsIgnoreCase("Scan User")) {
					roleVo.setRoleType("ROLE_SCANNING");
				} else if(prevOption.equalsIgnoreCase("Scrutiny User")){
					roleVo.setRoleType("ROLE_VENDOR_OPERATOR");
				} else if(prevOption.equalsIgnoreCase("Flc User")){
					roleVo.setRoleType("ROLE_FLC_OPERATOR");
				} else{
					roleVo.setRoleType("ROLE_AUDIT_USER");
				}
				userRole.setRoleId(rolesDao.getRoleByRoleType(roleVo).getId());
				userRole.setUserId(user.getId());
				session.save(userRole);
				if (user != null) {
					userVo = new UserVo();
					Utils.copyProperties(userVo, user);
				}
			} else {
				log.info("Updating User ::" + userVo.getId());

				String hql = "UPDATE User set firstName = :firstName, lastName = :lastName,"
						+ "middleName = :middleName , address = :address, username = :username,"
						+ "mobileNumber = :mobileNumber, status=:status WHERE id = :id";
				Query query = sessionFactory.getCurrentSession().createQuery(
						hql);
				query.setParameter("firstName", userVo.getFirstName());
				query.setParameter("lastName", userVo.getLastName());
				query.setParameter("middleName", userVo.getMiddleName());
				query.setParameter("address", userVo.getAddress());
				query.setParameter("username", userVo.getUsername());
				query.setParameter("mobileNumber", userVo.getMobileNumber());
				query.setParameter("status", userVo.getStatus());
				query.setParameter("id", userVo.getId());
				int result = query.executeUpdate();
				log.info("Rows affected: " + result);
			}
		return userVo;
	}

	@SuppressWarnings("unchecked")
	public List<UserVo> getUsersByRowsCount(FilterVo<UserVo> filterVo) {
		List<UserVo> userVoList = null;
		if (filterVo != null) {
			filterVo.setPageNumber((filterVo.getPageNumber() - 1)
					* filterVo.getRowsPerPage());
		}
		try {
			List<User> userList = sessionFactory.getCurrentSession()
					.createCriteria(User.class).addOrder(Order.desc("id"))
					.setFirstResult(filterVo.getPageNumber())
					.setMaxResults(filterVo.getRowsPerPage()).list();
			userVoList = convertToUserVoList(userList);
		} catch (Exception e) {
			log.error(
					"Error in getUsersByRowsCount of class UserDao"
							+ e.getMessage(), e);
		}
		return userVoList;
	}

	public Long getUsersCount() {
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(User.class)
					.setProjection(Projections.rowCount()).uniqueResult();
		} catch (Exception e) {
			log.error(
					"Error in getUsersCount of class UserDao" + e.getMessage(),
					e);
		}
		return count;
	}

	public boolean updateUserActiveById(String status, Integer active, Long id) {
		try {
			String hql = "UPDATE User SET status=?, active=? WHERE id=?";
			Session session = sessionFactory.getCurrentSession();
			session.createQuery(hql).setString(0, status).setInteger(1, active)
					.setLong(2, id).executeUpdate();
			return true;
		} catch (Exception e) {
			log.error("updateUserActiveById :: " + e.getMessage(), e);
			return false;
		}
	}

	public boolean updatePassword(String newPassward, Long id) {
		try {
			Session session = sessionFactory.getCurrentSession();

			String hql = "UPDATE User SET pwdEncrypted=? WHERE id=?";
			session.createQuery(hql).setString(0, newPassward).setLong(1, id)
					.executeUpdate();
			return true;
		} catch (Exception e) {
			log.error("updatePassword :: " + e.getMessage(), e);
			return false;
		}
	}

	@Override
	public boolean getExistingUserName(String username) {
		long count = 0;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(User.class)
					.add(Restrictions.eq("username", username))
					.setProjection(Projections.rowCount()).uniqueResult();
			log.info("getExistingUserName :: count ::" + count);
			if (count > 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			log.error("Erron in getExistingUserName method " + e.getMessage(),
					e);
			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<UserVo> getUserList(FilterVo<UserVo> filterVo) {
		
		try {
			if (filterVo != null) {
				filterVo.setPageNumber((filterVo.getPageNumber() - 1)
						* filterVo.getRowsPerPage());
			}
			Session session = sessionFactory.getCurrentSession();
			List list = session
					.createCriteria(User.class)
					.add(Restrictions.eq("createdBy", sessionService
							.getUserVo().getId()))
//							.addOrder(Order.desc("id"))
							.addOrder(Order.asc("status"))
					.setFirstResult(filterVo.getPageNumber())
					.setMaxResults(filterVo.getRowsPerPage()).list();
			return convertUserVoList(list);
		} catch (Exception e) {
			log.error("getUserList :: " + e.getMessage(), e);
		}
		return null;
	}

	private List<UserVo> convertUserVoList(List<User> list) throws Exception {
		ArrayList<UserVo> userVos = new ArrayList<UserVo>();
		for (User user : list) {
			UserVo userVo = convertToUserVo(user);
			userVos.add(userVo);
		}
		return userVos;
	}

	@SuppressWarnings("unchecked")
	public List<UserVo> getAllUsers() {
	
		List<UserVo> userVOs = null;
		try {
			List<User> userList = sessionFactory.getCurrentSession()
					.createCriteria(User.class).addOrder(Order.desc("createdDate")).list();
			if (userList != null) {
				userVOs = convertToUserVoList(userList);
			}
			log.info("getAllUsers size" + userVOs.size());
		} catch (Exception e) {
			log.error(
					"Error in getAllUsers of class UserDao::" + e.getMessage(),
					e);
		}
		return userVOs;
	}

}
