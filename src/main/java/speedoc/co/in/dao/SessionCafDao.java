package speedoc.co.in.dao;
import speedoc.co.in.vo.SessionCafVo;

public interface SessionCafDao {
	public void insertSessionCaf(SessionCafVo sessionCafVo);
	public boolean getSessionCafVoByCafId(Long cafTblId) ;
	
}
