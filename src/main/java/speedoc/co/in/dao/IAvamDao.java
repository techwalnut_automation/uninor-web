package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.AvamImportVo;
import speedoc.co.in.vo.AvamVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.PosCposVo;

public interface IAvamDao {
	AvamVo addNewAvamNumber(AvamVo avamVo);
	AvamVo getAvamVoBySpeedocId(String speedocId);
	AvamVo getAvamVoByCafTblId(String cafTblId);
	boolean updateAvamTblByAvamId(CafVo cafVo, Long avamTblId) throws Exception;
	List<CafStatusVo> getAvamFailAndFLCCompleteRecords();
	public AvamImportVo addAvamImportRecord(AvamImportVo avamImportVo);
	public Long getAvamCountBySpeedocId(String speedocId);
	public Boolean updateAvamImportByImportId(Integer recordInserted,Integer duplicateRecord, Long importId);
	public List<AvamImportVo> getAvamMonthWiseList(
			FilterVo<AvamImportVo> filterVo, Integer month,String year) ;
	Long getAvamCount();
	PosCposVo addNewPosTbl(PosCposVo posCposVo);
}
