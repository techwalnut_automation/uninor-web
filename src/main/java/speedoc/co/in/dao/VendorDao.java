package speedoc.co.in.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.domain.Vendor;
import speedoc.co.in.domain.VendorScanWorkstation;
import speedoc.co.in.domain.VendorUser;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.VendorScanWorkstationVo;
import speedoc.co.in.vo.VendorUserVo;
import speedoc.co.in.vo.VendorVo;

@Repository
@Transactional
public class VendorDao implements IVendorDao {

	private static Logger logger = Logger.getLogger(VendorDao.class);

	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private SessionService sessionService ; 
	
	@Override
	public VendorVo addVendor(VendorVo vendorVo) {
		Vendor vendor = new Vendor();
		try {
			Utils.copyProperties(vendor, vendorVo);
			Session session = sessionFactory.getCurrentSession();
			vendor.setAdminUserId(sessionService.getUserVo().getId());
			session.save(vendor);
			if (vendor != null) {
				vendorVo = convertToVendorVo(vendor);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return vendorVo;
	}

	@Override
	public VendorUserVo addVendorUser(VendorUserVo vendorUserVo) throws Exception {
		VendorUser vendorUser = new VendorUser();
		
			Utils.copyProperties(vendorUser, vendorUserVo);
			Session session = sessionFactory.getCurrentSession();
			session.save(vendorUser);
			if (vendorUser != null) {
				vendorUserVo = convertToVendorUserVo(vendorUser);
			}
		return vendorUserVo;
	}

	@Override
	public VendorVo getVendorByAdminUserId(Long adminId) {
		logger.info("Inside VendorDao---->getVendorByAdminUserId()::adminId="
				+ adminId);
		VendorVo vendorVo = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			Vendor vendor = (Vendor) session.createCriteria(Vendor.class)
					.add(Restrictions.eq("adminUserId", adminId))
					.uniqueResult();
			if (vendor != null) {
				vendorVo = convertToVendorVo(vendor);
			}
		} catch (Exception e) {
			logger.error("Error in getVendorByAdminUserId of class VendorDao::"
					+ e.getMessage(), e);
		}
		return vendorVo;
	}

	@Override
	public VendorUserVo getVendorUserByUserId(Integer userId) {
		VendorUserVo vendorUserVo = null;
		try {
			Session session = (Session) sessionFactory.getCurrentSession();
			VendorUser vendorUser = (VendorUser) session
					.createCriteria(VendorUser.class)
					.add(Restrictions.eq("userId", userId)).uniqueResult();
			if (vendorUser != null) {
				vendorUserVo = convertToVendorUserVo(vendorUser);
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return vendorUserVo;
	}

	public static VendorVo convertToVendorVo(Vendor vendor) throws Exception {
		VendorVo vendorVo = new VendorVo();
		Utils.copyProperties(vendorVo, vendor);

		return vendorVo;
	}

	@Override
	public VendorScanWorkstationVo getVendorScanWorkStationByVendorId(
			Long vendorId) {
		logger.info("Inside VendorDao---->getVendorScanWorkStationByVendorId()::vendorId="
				+ vendorId);
		VendorScanWorkstationVo vendorScanWorkstationVo = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			VendorScanWorkstation vendorScanWorkstation = (VendorScanWorkstation) session
					.createCriteria(VendorScanWorkstation.class)
					.add(Restrictions.eq("vendorId", vendorId)).uniqueResult();
			if (vendorScanWorkstation != null) {
				vendorScanWorkstationVo = convertToVendorScanWorkstationVo(vendorScanWorkstation);
			}
		} catch (Exception e) {
			logger.error(
					"Error in getVendorScanWorkStationByVendorId of class VendorDao::"
							+ e.getMessage(), e);
		}
		return vendorScanWorkstationVo;
	}

	@Override
	public VendorScanWorkstationVo getVendorScanWorkStationByRegKey(
			String regKey) {
		logger.info("Inside VendorDao---->getVendorScanWorkStationByRegKey()::regKey="+ regKey);
		VendorScanWorkstationVo vendorScanWorkstationVo = null;
		try {
			Session session = sessionFactory.getCurrentSession();
			VendorScanWorkstation vendorScanWorkstation = (VendorScanWorkstation) session
					.createCriteria(VendorScanWorkstation.class)
					.add(Restrictions.eq("regKey", regKey)).uniqueResult();

			if (vendorScanWorkstation != null) {
				vendorScanWorkstationVo = convertToVendorScanWorkstationVo(vendorScanWorkstation);
			}
		} catch (Exception e) {
			logger.error("Error in getVendorScanWorkStationByRegKey of class VendorDao::"
							+ e.getMessage(), e);
		}
		logger.info("Inside VendorDao---->  getVendorScanWorkStationByRegKey()::  vendorScanWorkstationVo="+ vendorScanWorkstationVo);
		return vendorScanWorkstationVo;
	}

	public VendorScanWorkstationVo updateVendorScanWorkStationByRegKey(
			String macId, String regKey) {
		VendorScanWorkstationVo vendorScanWorkstationVo = null;
		try {
			String hql = "UPDATE VendorScanWorkstation SET macId=? WHERE regKey=?";
			Session session = sessionFactory.getCurrentSession();
			session.createQuery(hql).setString(0, macId).setString(1, regKey)
					.executeUpdate();
			VendorScanWorkstation vendorScanWorkstation = (VendorScanWorkstation) session
					.createCriteria(VendorScanWorkstation.class)
					.add(Restrictions.eq("regKey", regKey)).uniqueResult();
			if (vendorScanWorkstation != null) {
				vendorScanWorkstationVo = convertToVendorScanWorkstationVo(vendorScanWorkstation);
			}
			logger.info(" updateVendorScanWorkStationByRegKey  :: updation done ");
		} catch (Exception e) {
			logger.error(e);

		}
		return vendorScanWorkstationVo;
	}

	public static VendorScanWorkstationVo convertToVendorScanWorkstationVo(
			VendorScanWorkstation vendorScanWorkstation) throws Exception {
		VendorScanWorkstationVo vendorScanWorkstationVo = new VendorScanWorkstationVo();
		Utils.copyProperties(vendorScanWorkstationVo, vendorScanWorkstation);

		return vendorScanWorkstationVo;
	}

	public static VendorUserVo convertToVendorUserVo(VendorUser vendorUser)
			throws Exception {
		VendorUserVo vendorUserVo = new VendorUserVo();
		Utils.copyProperties(vendorUserVo, vendorUser);

		return vendorUserVo;
	}


	@Override
	public VendorVo getVendorDataByUserId(Long userId) throws Exception {
		VendorVo vendorVo =  null ;
		
			Vendor vendor = (Vendor) sessionFactory.getCurrentSession().createCriteria(Vendor.class)
			.add(Restrictions.eq("adminUserId", userId)).uniqueResult();
			if(vendor != null){
				vendorVo = new VendorVo();
				Utils.copyProperties(vendorVo, vendor) ;
			}
		return vendorVo;
	}

	@Override
	public VendorScanWorkstationVo addVendorScanWorkStation(
			VendorScanWorkstationVo vendorScanWorkstationVo) throws Exception {
		VendorScanWorkstation vendorScanWorkstation=new VendorScanWorkstation();
	
			Utils.copyProperties(vendorScanWorkstation, vendorScanWorkstationVo);
			Session session=sessionFactory.getCurrentSession();
			session.save(vendorScanWorkstation);
			if(vendorScanWorkstation != null){
				vendorScanWorkstationVo=new VendorScanWorkstationVo();
				Utils.copyProperties(vendorScanWorkstationVo, vendorScanWorkstation);
			}
		return vendorScanWorkstationVo;
	}


}
