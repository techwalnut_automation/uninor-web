package speedoc.co.in.dao;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.FlcRemark;
import speedoc.co.in.vo.FlcRemarkVo;

@Repository
public class FlcRemarkDao implements IFlcRemarkDao{

	@Autowired
	private SessionFactory sessionFactory;
	
	public Long save(FlcRemarkVo flcRemarkVo ) throws Exception{
			FlcRemark flcRemark = new FlcRemark() ;
			BeanUtils.copyProperties(flcRemark, flcRemarkVo) ;
			Session session = sessionFactory.getCurrentSession();
			session.save(flcRemark) ;
			session.flush() ;
			return flcRemark.getId() ;
	}

}
