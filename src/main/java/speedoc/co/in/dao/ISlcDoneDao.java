package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.SlcDoneFileVo;

public interface ISlcDoneDao {

	List<SlcDoneFileVo> getSlcDoneMonthWiseList(FilterVo<SlcDoneFileVo> filterVo, int monthNumber, String year);
	Long getSlcDoneCount();
	void addSlcDoneDataFile(String path);
	SlcDoneFileVo addSlcDoneFileRecord(SlcDoneFileVo slcDoneFileVo);
	Boolean updateSlcFileId(Long slcFileId);
	Boolean updateSlcDoneFileByFIleId(Integer i, Integer j, Long slcFileId);
	Integer uploadSlcDoneFile();
	boolean clearAllCurrentAccessList();
}
