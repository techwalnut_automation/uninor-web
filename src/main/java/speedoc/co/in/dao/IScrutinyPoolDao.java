package speedoc.co.in.dao;

import java.util.List;

import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.SearchResultVo;
import speedoc.co.in.vo.StatusResultVo;
import speedoc.co.in.vo.UserVo;

public interface IScrutinyPoolDao {
	public CafStatusVo getDECompletedCafs(UserVo userVo) throws Exception;

	// public ScrutinyPoolVo save1(ScrutinyPoolVo scrutinyPoolVo ) throws
	// BatchUpdateException;
	public boolean delete(Long id);

	public List<SearchResultVo> getSlcSearchDataBySpeedocId(String speedocId);

	public List<SearchResultVo> getSlcSearchDataByCafNumber(String cafNumber);

	public List<SearchResultVo> getSlcSearchDataByMobileNumber(
			String mobileNumber);

	public boolean deleteFromSlcPoolTbl(Long cafTblId);

	public List<StatusResultVo> getStatusList(StatusResultVo statusResultVo);

	public CafVo getCafData(Long cafTblId);

	public boolean cancelCaf(Long cafTblId);

	public boolean cafNotDeComplete(Long id);

	public IndexingDataVO getElectroData(String poiref);
}
