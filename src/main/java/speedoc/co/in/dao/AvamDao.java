package speedoc.co.in.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import speedoc.co.in.domain.Avam;
import speedoc.co.in.domain.AvamImport;
import speedoc.co.in.domain.CafStatus;
import speedoc.co.in.domain.PosCpos;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.AvamImportVo;
import speedoc.co.in.vo.AvamVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.PosCposVo;

@Repository
public class AvamDao implements IAvamDao {

	private static Logger log = Logger.getLogger(AvamDao.class.getName());

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public AvamVo addNewAvamNumber(AvamVo avamVo) {
		Avam avam = new Avam();
		AvamVo savedAvamVo = null;
		try {
			Utils.copyProperties(avam, avamVo);
			Session session = sessionFactory.getCurrentSession();
			session.save(avam);
			if (avam != null) {
				savedAvamVo = new AvamVo();
				Utils.copyProperties(savedAvamVo, avam);
			}
		} catch (Exception e) {
			log.error("Erron in addNewAvamNumber method "+e.getMessage());
			return null;
		}
		return savedAvamVo;
	}

	@Override
	public AvamVo getAvamVoBySpeedocId(String speedocId) {
		AvamVo avamVo = null;
		try {
			Avam avam = (Avam) sessionFactory.getCurrentSession()
					.createCriteria(Avam.class)
					.add(Restrictions.eq("speedocId", speedocId))
					.uniqueResult();
			if (avam != null) {
				avamVo = new AvamVo();
				Utils.copyProperties(avamVo, avam);
			}
		} catch (Exception e) {
			log.error("Erron in getAvamVoBySpeedocId method ", e);
			return null;
		}
		return avamVo;
	}

	@Override
	public AvamVo getAvamVoByCafTblId(String cafTblId) {
		AvamVo avamVo = null;
		try {
			Avam avam = (Avam) sessionFactory.getCurrentSession()
					.createCriteria(Avam.class)
					.add(Restrictions.eq("cafTblId", cafTblId)).uniqueResult();
			if (avam != null) {
				avamVo = new AvamVo();
				Utils.copyProperties(avamVo, avam);
			}
		} catch (Exception e) {
			log.error("Erron in getAvamVoByCafTblId method ", e);
		}
		return avamVo;
	}

	@Override
	public boolean updateAvamTblByAvamId(CafVo cafVo, Long avamTblId)
			throws Exception {
		log.info("inside updateAvamTblByAvamId :: avamTblId :: " + avamTblId);
		String hql = "UPDATE Avam set cafTblId = :cafTblId"
				+ " WHERE id = :id";
		Query query = sessionFactory.getCurrentSession().createQuery(hql);
		query.setParameter("cafTblId", cafVo.getId());
		query.setParameter("id", avamTblId);
		int result = query.executeUpdate();
		log.info("updateAvamTblByAvamId Rows affected: " + result);
		return true;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override public List<CafStatusVo> getAvamFailAndFLCCompleteRecords() {
		log.info("inside get getAvamFailAndFLCCompleteRecords.");
		String hql = " from CafStatus where latestInd IS true AND "
				+ "endDate IS null AND (statusId ="
				+ StatusCodeEnum.AVAM_FAIL.getStatusCode() + " OR statusId="
				+ StatusCodeEnum.FLC_ACCEPT.getStatusCode() + ")";
		try {
			Query query = sessionFactory.getCurrentSession().createQuery(hql);
			List list = query.list();
			if (list != null) {
				return convertToCafStatucVoList(list);
			}
			return null;
		} catch (Exception e) {
			log.error("Error in getAvamFailAndFLCCompleteRecords :: " + e);
		}
		return null;
	}

	public static List<CafStatusVo> convertToCafStatucVoList(
			List<CafStatus> handsetList) throws Exception {
		ArrayList<CafStatusVo> cafStatusVos = new ArrayList<CafStatusVo>();
		for (CafStatus cafStatus : handsetList) {
			CafStatusVo cafStatusVo = convertToCafStatusVo(cafStatus);
			cafStatusVos.add(cafStatusVo);
		}
		return cafStatusVos;
	}

	private static CafStatusVo convertToCafStatusVo(CafStatus cafStatus)
			throws Exception {
		CafStatusVo cafStatusVo = new CafStatusVo();
		Utils.copyProperties(cafStatusVo, cafStatus);
		return cafStatusVo;
	}

	@SuppressWarnings("unused")
	private List<AvamVo> convertToAvamVo(List<Object> list) throws Exception {
		Iterator<Object> it = list.iterator();
		List<AvamVo> avamVoList = new ArrayList<AvamVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			AvamVo avamVo = new AvamVo();
			avamVo.setId(((BigInteger) object[0]).longValue());
			avamVo.setSpeedocId((String) object[1]);
			avamVo.setMobileNumber((String) object[2]);
			avamVo.setDtrCode((String) object[3]);
			avamVo.setCpId((Long) object[4]);
			avamVoList.add(avamVo);
		}
		return avamVoList;
	}

	@Override
	public AvamImportVo addAvamImportRecord(AvamImportVo avamImportVo) {
		AvamImport avamImport = new AvamImport();
		try {
			Utils.copyProperties(avamImport, avamImportVo);
			Session session = sessionFactory.getCurrentSession();
			session.save(avamImport);
			if (avamImport != null) {
				AvamImportVo avamImportVo2 = new AvamImportVo();
				Utils.copyProperties(avamImportVo2, avamImport);
				return avamImportVo2;
			}
		} catch (Exception e) {
			log.error("addAvamImportRecord :: AvamDao :: " + e);
		}
		return avamImportVo;
	}

	@Override
	public Long getAvamCountBySpeedocId(String speedocId) {
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(Avam.class)
					.add(Restrictions.eq("speedocId", speedocId))
					.setProjection(Projections.rowCount()).uniqueResult();
		} catch (Exception e) {
			log.error("getAvamCountBySpeedocId :: avamDao :: " + e.getMessage(),e);
		}
		return count;
	}

	@Override
	public Boolean updateAvamImportByImportId(Integer recordInserted,Integer duplicateRecord, Long importId) {
		try {
			String hql = "UPDATE AvamImport SET recordInserted=?, duplicateRecord=?, status=? "
					+ "WHERE id=?";
			Session session = sessionFactory.getCurrentSession();
			session.createQuery(hql).setInteger(0, recordInserted)
					.setInteger(1, duplicateRecord).setString(2, "COMPLETE")
					.setLong(3, importId).executeUpdate();
			return true;
		} catch (Exception e) {
			log.error("updateAvamImportByImportId :: AvamDao ::" + e);
			return false;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AvamImportVo> getAvamMonthWiseList(
			FilterVo<AvamImportVo> filterVo, Integer month, String year) {

		String sql = "SELECT avam_import_tbl.id, avam_import_tbl.file_name,avam_import_tbl.total_record, "
				+ "avam_import_tbl.record_inserted, avam_import_tbl.duplicate_record, avam_import_tbl.status, "
				+ "avam_import_tbl.created_date,user_tbl.username FROM avam_import_tbl "
				+ "JOIN user_tbl on avam_import_tbl.created_by = user_tbl.id "
				+ "WHERE EXTRACT(month FROM avam_import_tbl.created_date)  = "
				+ month
				+ " "
				+ "AND EXTRACT(year FROM avam_import_tbl.created_date) = "
				+ year + " " + "ORDER BY avam_import_tbl.created_date DESC";
		try {
			List<Object> list = sessionFactory.getCurrentSession()
					.createSQLQuery(sql).list();
			return convertToAvamImportVoList(list);
		} catch (Exception e) {
			log.error(e);
		}
		return null;
	}

	public List<AvamImportVo> convertToAvamImportVoList(List<Object> list)
			throws Exception {
		Iterator<Object> it = list.iterator();
		List<AvamImportVo> avamVoList = new ArrayList<AvamImportVo>();
		while (it.hasNext()) {
			Object[] object = (Object[]) it.next();
			AvamImportVo avamImportVo = new AvamImportVo();
			// avamImportVo.setId((Long) object[0]);
			avamImportVo.setFileName((String) object[1]);
			avamImportVo.setTotalRecord((Integer) object[2]);
			avamImportVo.setRecordInserted((Integer) object[3]);
			avamImportVo.setDuplicateRecord((Integer) object[4]);
			avamImportVo.setStatus((String) object[5]);
			avamImportVo.setCreatedDate((Timestamp) object[6]);
			avamImportVo.setUsername((String) object[7]);
			avamVoList.add(avamImportVo);
		}
		return avamVoList;
	}

	@Override
	public Long getAvamCount() {
		Long count = null;
		try {
			count = (Long) sessionFactory.getCurrentSession()
					.createCriteria(AvamImport.class)
					.setProjection(Projections.rowCount()).uniqueResult();
		} catch (Exception e) {
			log.error("Error in getAvamCount of class AvamDao" + e.getMessage(),e);
		}
		return count;
	}

	@Override
	public PosCposVo addNewPosTbl(PosCposVo posCposVo) {
		PosCpos posCpos = new PosCpos();
		PosCposVo savedPosCposVo = null;
		try {
			Utils.copyProperties(posCpos, posCposVo);
			Session session = sessionFactory.getCurrentSession();
			session.save(posCpos);
			if (posCpos != null) {
				savedPosCposVo = new PosCposVo();
				Utils.copyProperties(savedPosCposVo, posCpos);
			}
		} catch (Exception e) {
			log.error("Erron in addNewPosTbl method " + e.getMessage(), e);
			return null;
		}
		return savedPosCposVo;
	}
}
