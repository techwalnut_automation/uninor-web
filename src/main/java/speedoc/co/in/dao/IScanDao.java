package speedoc.co.in.dao;

import speedoc.co.in.vo.AvamVo;
import speedoc.co.in.vo.CafVo;

public interface IScanDao {
	boolean updateCafTblByAvamData(CafVo cafVo, AvamVo avamVo);
	boolean checkFileIdExistsInCaf(String fileId, String speedocId);
}
