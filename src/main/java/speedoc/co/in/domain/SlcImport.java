package speedoc.co.in.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "slc_import_tbl")
public class SlcImport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4564876032555310851L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "slc_import_tbl_seq")
	@SequenceGenerator(name ="slc_import_tbl_seq" , sequenceName = "slc_import_tbl_seq" , allocationSize = 1)
        private Long id;
	
	@Column(name="file_name")
	private String fileName;
	
	@Column(name="total_record")
	private Integer totalRecord;
	
	@Column(name="record_updated")
	private Integer recordUpdated;
	
	@Column(name="duplicate_record")
	private Integer duplicateRecord;
	
	@Column(name="status")
	private String status;
	
	
    @Column(name = "created_date", insertable = false, updatable = false)
	private Timestamp createdDate;
	
	@Column(name="created_by")
	private Long createdBy;

	public SlcImport() {
	}

	public SlcImport(Long id, String status, Timestamp createdDate) {
		this.id = id;
		this.status = status;
		this.createdDate = createdDate;
	}

	public SlcImport(Long id, String fileName, Integer totalRecord,
			Integer recordUpdated, Integer duplicateRecord, String status,
			Timestamp createdDate, Long createdBy) {
		this.id = id;
		this.fileName = fileName;
		this.totalRecord = totalRecord;
		this.recordUpdated = recordUpdated;
		this.duplicateRecord = duplicateRecord;
		this.status = status;
		this.createdDate = createdDate;
		this.createdBy = createdBy;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Integer getTotalRecord() {
		return totalRecord;
	}

	public void setTotalRecord(Integer totalRecord) {
		this.totalRecord = totalRecord;
	}

	public Integer getRecordUpdated() {
		return recordUpdated;
	}

	public void setRecordUpdated(Integer recordUpdated) {
		this.recordUpdated = recordUpdated;
	}


	public Integer getDuplicateRecord() {
		return duplicateRecord;
	}

	public void setDuplicateRecord(Integer duplicateRecord) {
		this.duplicateRecord = duplicateRecord;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

}
