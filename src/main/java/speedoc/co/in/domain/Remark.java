package speedoc.co.in.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name="remark_tbl")
public class Remark implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 741096284765797174L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "remark_tbl_seq")
	@SequenceGenerator(name ="remark_tbl_seq" , sequenceName = "remark_tbl_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="reject_reason")
	private String rejectReason;

	@Column(name="reject_type")
	private String rejectType ;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	

	public String getRejectType() {
		return rejectType;
	}

	public void setRejectType(String rejectType) {
		this.rejectType = rejectType;
	}

	@Override
	public String toString() {
		return "Remark [id=" + id + ", rejectReason=" + rejectReason
				+ ", rejectType=" + rejectType + "]";
	}

}
