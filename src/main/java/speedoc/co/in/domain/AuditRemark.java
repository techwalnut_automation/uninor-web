package speedoc.co.in.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="audit_remark_tbl")
public class AuditRemark implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1059678919082941803L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "audit_remark_tbl_seq")
	@SequenceGenerator(name ="audit_remark_tbl_seq" , sequenceName = "audit_remark_tbl_seq" , allocationSize = 1)
	private Long id;
	
	@Column(name="remark_id")
	private Long remarkId;
	

	@Column(name="caf_tbl_id")
	private Long cafTblId;
	
	@Column(name="user_id")
	private Long userId;
	
	
	@Column(name="created_date", insertable = false, updatable = false)
	private Timestamp createdDate;

	@Column(name ="caf_status_id")
	private Long statusId ;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRemarkId() {
		return remarkId;
	}

	public void setRemarkId(Long remarkId) {
		this.remarkId = remarkId;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	@Override
	public String toString() {
		return "AuditRemark [id=" + id + ", remarkId=" + remarkId
				+ ", cafTblId=" + cafTblId + ", userId=" + userId
				+ ", createdDate=" + createdDate + ", statusId=" + statusId
				+ "]";
	}

	
	
}
