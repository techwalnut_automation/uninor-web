package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "slc_pool_tbl")
public class ScrutinyPool {

	@Id
//	@GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "scrutiny_pool_tbl_seq")
//	@SequenceGenerator(name = "scrutiny_pool_tbl_seq" ,sequenceName ="scrutiny_pool_tbl_seq" ,allocationSize = 1 )
	@GenericGenerator(name="generator", strategy="increment")
	@GeneratedValue(generator="generator")
	private Long id;

	@Column(name = "caf_tbl_id")
	private Long cafTblId;

	@Column(name = "start_date")
	private Timestamp startDate;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCafTblId() {
		return cafTblId;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	@Override
	public String toString() {
		return "ScrutinyPool [id=" + id + ", cafTblId=" + cafTblId
				+ ", startDate=" + startDate + "]";
	}
	
}
