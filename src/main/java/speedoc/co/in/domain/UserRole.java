package speedoc.co.in.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


@Entity
@Table(name = "user_role_tbl")
public class UserRole {

	public UserRole(){
		super();
		id = null ;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE , generator =  "user_role_tbl_seq")
	@SequenceGenerator(name = "user_role_tbl_seq" , sequenceName = "user_role_tbl_seq" , allocationSize = 1)
	private Long id ;
	
	@Column(name = "role_id")
	private Long roleId;
	
	@Column(name = "user_id")
	private Long userId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "UserRole [id=" + id + ", roleId=" + roleId + ", userId="
				+ userId + "]";
	}
	
	
	
	
}
