package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="slc_done_data_tbl")
public class SlcDoneData {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="slc_done_data_tbl_seq")
	@SequenceGenerator(name="slc_done_data_tbl_seq", sequenceName="slc_done_data_tbl_seq", allocationSize=1)
	private Long id;
	
	@Column(name="speedoc_id")
	private String speedocId;
	
	@Column(name="mobile_number")
	private String mobileNumber;
	
	@Column(name="created_date", insertable = false, updatable = false)
	private Timestamp createdDate;
	
	@Column(name="status")
	private Integer status;
	
	@Column(name="slc_file_id")
	private Long slcFileId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSlcFileId() {
		return slcFileId;
	}

	public void setSlcFileId(Long slcFileId) {
		this.slcFileId = slcFileId;
	}

	public String getSpeedocId() {
		return speedocId;
	}

	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "SlcDoneData [id=" + id + ", speedocId=" + speedocId
				+ ", mobileNumber=" + mobileNumber + ", createdDate="
				+ createdDate + ", status=" + status + ", slcFileId="
				+ slcFileId + "]";
	}
}
