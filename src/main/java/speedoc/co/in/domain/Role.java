package speedoc.co.in.domain;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="roles_tbl")
public class Role {

	public Role(){
	  super();
	  id = null ;
	}
	
	@Id
	private Long id;
	
	@Column(name="role_type")
	private String roleType;;
	
	@Column(name = "role_name")
	private String roleName;
	
	@Column(name="created_date")
	private Timestamp createdDate;
	
	@Column(name="modified_date")
	private Timestamp modifiedDate;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", roleType=" + roleType + ", roleName="
				+ roleName + ", createdDate=" + createdDate + ", modifiedDate="
				+ modifiedDate + "]";
	}

	
}
