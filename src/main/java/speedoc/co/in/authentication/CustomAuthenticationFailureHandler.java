package speedoc.co.in.authentication;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;


public class CustomAuthenticationFailureHandler extends
		SimpleUrlAuthenticationFailureHandler {
	protected static Logger logger = Logger
			.getLogger("CustomAuthenticationFailureHandler");

	@Override
	public void onAuthenticationFailure(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		logger.info("onAuthenticationFailure() called : Authentication failure");
		logger.debug("Authentication failure");


		super.onAuthenticationFailure(request, response, exception);

	}

}
