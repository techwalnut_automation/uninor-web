package speedoc.co.in.authentication;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

@SuppressWarnings("deprecation")
public class CustomAuthenticationEntryPoint extends
		LoginUrlAuthenticationEntryPoint {

	protected static Logger logger = Logger.getLogger("CustomAuthenticationEntryPoint");

	@Override
	public void commence(HttpServletRequest request,
			HttpServletResponse response, AuthenticationException authException)
			throws IOException, ServletException {

		logger.debug("Authentication required");
		logger.info("CustomAuthenticationEntryPoint : commence() : Authentication required");
		super.commence(request, response, authException);
	}
}
