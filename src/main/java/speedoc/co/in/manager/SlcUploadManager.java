package speedoc.co.in.manager;

import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.ISlcUploadDao;
import speedoc.co.in.dao.IUserDao;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.SlcDataVo;
import speedoc.co.in.vo.SlcImportVo;
import speedoc.co.in.vo.UserVo;

import com.csvreader.CsvReader;

@Component
@Transactional
public class SlcUploadManager {
	
	private static Logger logger=Logger.getLogger(SlcUploadManager.class);
		
	@Autowired
	private ISlcUploadDao slcUploadDao;
	
	@Autowired
	SessionService sessionService;
	
	@Autowired
	private IUserDao userDao;
		
	public boolean readSlcExcelData(String slcFilePath){
		logger.info("loading file =  " + slcFilePath);
		try {
			CsvReader reader = new CsvReader(slcFilePath,',');		
			int numLines=0;			
				while(reader.readRecord()){
					numLines++;
				}
			logger.info("numLines :: "+numLines);
			Long slcImportId = addSlcExcelImportId(slcFilePath,numLines-1);
			if (slcImportId != 0 && slcImportId != null) {
				 slcUploadDao.addSlcDataFile(slcFilePath);
				 slcUploadDao.updateSlcImortId(slcImportId);
				 slcUploadDao.updateSlcImportByImportId(0, 0, slcImportId);
			}
		} catch (Exception e) {
			logger.error("readSlcExcelData :: "+e.getMessage(),e);
		}
		return true;
	}
	
	@SuppressWarnings("unused")
	private SlcDataVo addNewSlcDataNumber(SlcDataVo slcDataVo) {
		SlcDataVo savedSlcDataVo = slcUploadDao.addNewSlcDataNumber(slcDataVo);
		return savedSlcDataVo;
	}

	private Long addSlcExcelImportId(String slcFilePath, int totalRecords) {
		SlcImportVo slcImportVo = new SlcImportVo();
		slcImportVo.setCreatedBy((long) 2);
		slcImportVo.setFileName(FilenameUtils.getName(slcFilePath));
		slcImportVo.setStatus("INPROCESS");
		slcImportVo.setTotalRecord(totalRecords);
		SlcImportVo insertedSlcImportRecordVo = slcUploadDao.addSlcImportRecord(slcImportVo);
		return insertedSlcImportRecordVo.getId();
	}

	public List<SlcImportVo> getSlcList(FilterVo<SlcImportVo> filterVo) {
			return slcUploadDao.getSlcList(filterVo);
	}

	public Long getSlcCount() {
		return slcUploadDao.getSlcCount();
	}
	
	public UserVo getUserLogin() {
		User user = (User) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();
		logger.info("user.getUsername() :: " + user.getUsername());
		UserVo userVo = userDao.findByUniqueUserName(user.getUsername());
		logger.info("userVo :: " + userVo);
		return userVo;
	}

	public Integer uploadSlcFile() {
		try{
			logger.info("inside procedure function uploadSlcFile");
			Integer uploadSlcFile = slcUploadDao.uploadSlcFile();
			logger.info("uploadSlcFile :: "+uploadSlcFile);
			return uploadSlcFile;
		}catch (Exception e) {
			logger.error(""+e.getMessage(),e);
			return null;
		}
	}
}
