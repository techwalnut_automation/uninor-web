package speedoc.co.in.manager;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.IAuditDao;
import speedoc.co.in.dao.IAuditRemarkDao;
import speedoc.co.in.dao.ICafDao;
import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.dao.ICaoDao;
import speedoc.co.in.dao.IRemarkDao;
import speedoc.co.in.dao.IScrutinyPoolDao;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.vo.AuditRemarkVo;
import speedoc.co.in.vo.CafAuditVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.CaoVo;
import speedoc.co.in.vo.IndexingDataVO;
import speedoc.co.in.vo.PosCposVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

@Component
@Transactional
public class AuditManager {

	@Autowired
	private IAuditDao auditDao;

	@Autowired
	private ICafDao cafDao;

	@Autowired
	private ICaoDao caoDao;

	@Autowired
	private ICafStatusDao cafStatusDao;

	@Autowired
	private IAuditRemarkDao auditRemarkDao;
	
	@Autowired
	private IRemarkDao remarkDao;
	
	@Autowired
	private IScrutinyPoolDao scrutinyPoolDao;

	@Value("${ACTIVATION_OFFICER_FILE_PATH}")
	private String caoPath;

	@Value("${TEMP_DATA_PATH}")
	private String cafTempPath;

	private static Logger logger = Logger.getLogger(AuditManager.class);

	public CafAuditVo getSlcRejectedCafs(UserVo userVo) {
		CafAuditVo cafAuditVo = null;
		List<CafAuditVo> cafAuditVos =null;
		try {
			// Get SLC Rejected caf from caf_status tbl
			CafStatusVo cafStatusVo = auditDao.getSlcRejectedCafs(userVo); // fetch with limit 1
			logger.info("SLC Rejected cafStatusVo :: " + cafStatusVo);
			if (cafStatusVo != null) {
					// Get details from caf_tbl
					CafVo cafVo = cafDao.findCafById(cafStatusVo.getCafTblId());
					logger.info("cafVo = " + cafVo);
					PosCposVo posCposVo =cafDao.getPosDetail(cafVo.getSpeedocId());
					cafAuditVo = new CafAuditVo();
					if(posCposVo != null){
						cafVo.setRtrCode(posCposVo.getRtrCode());
						cafVo.setRtrName(posCposVo.getRtrName());
					}
					IndexingDataVO electroDataVo=scrutinyPoolDao.getElectroData(cafVo.getPoiref());
					if(electroDataVo != null){
						cafVo.setVoterId(electroDataVo.getVoterId());
						cafVo.setPollingStation(electroDataVo.getPollingStation());
						cafVo.setVoterName(cafVo.getFirstName());
					}
					BeanUtils.copyProperties(cafAuditVo, cafVo);
					cafAuditVo.setCafStatusId(cafStatusVo.getId());
					
					cafAuditVos = auditDao.findSlcRejectData(cafStatusVo.getCafTblId());
					for (CafAuditVo cafAuditVo2 : cafAuditVos) {
						cafAuditVo.setRejectReason(cafAuditVo2.getRejectReason());
						cafAuditVo.setUsername(cafAuditVo2.getUsername());
						cafAuditVo.setCreatedDate(cafAuditVo2.getCreatedDate());
					}
					return cafAuditVo;
				}
		} catch (Exception e) {
			logger.error("Error in getSlcRejectedCafs() of AuditManager :: "+ e.getMessage(), e);
		}
		return cafAuditVo;
	}

	public void getCafFile(String fileName, OutputStream out) {
		fileName = caoPath + File.separator + fileName;

		try {
			FileUtils.copyFile(new File(fileName), out);
		} catch (IOException e1) {
			logger.error(e1.getMessage(), e1);
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
	}

	public CaoVo getCaoImage(String caoCode) {
		return caoDao.getCaoImageByCaoCode(caoCode);
	}

	public Boolean rejectAudit(String auditremak, CafAuditVo cafAuditVo,
			UserVo userVo) {
		try {
			// Update CafStatus tbl (Setting end date and latestInd to false and current accessing user id to null)
			boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false, cafAuditVo.getCafStatusId());
			logger.info("Audit Rows Updated  in updateCafStatus ::"+poolResult);
			// Add status
			logger.info("rejectAudit cafAuditVo.getId() = " + cafAuditVo.getId());
			CafStatusVo cafStatusVo = new CafStatusVo();
			cafStatusVo.setCafTblId(cafAuditVo.getId());
			cafStatusVo.setLatestInd(true);
			cafStatusVo
					.setStatusId(StatusCodeEnum.AUDIT_REJECT.getStatusCode());
			cafStatusVo.setCreatedBy(userVo.getId());
			cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
			logger.info("Saved rejectSlc Status Rejected with id ::" + cafStatusVo.getId());

			String[] rejectReasonArr = auditremak.split(",");
			// Save remarks in Remark Tbl
			for (int i = 0; i < rejectReasonArr.length; i++) {
				AuditRemarkVo auditRemarkVo = new AuditRemarkVo();
				auditRemarkVo.setCafTblId(cafAuditVo.getId());
				auditRemarkVo.setRemarkId(Long.parseLong(rejectReasonArr[i].trim()));
				auditRemarkVo.setUserId(userVo.getId());
				auditRemarkVo.setStatusId(cafStatusVo.getId());
				Long id = auditRemarkDao.save(auditRemarkVo);
				logger.info("Saved rejectAudit with id ::" + id);
			}

			// Delete the images
			String[] imageNames = cafAuditVo.getImages1().split(",");
			if (imageNames.length > 0) {
				for (int i = 0; i < imageNames.length; i++) {
					if (!imageNames[i].equals("")) {
						deleteFile(imageNames[i]);
					}
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("Error in rejectAudit() of AuditManager :: " + e.getMessage(), e);
			return false;
		}
	}

	public void deleteFile(String fileName) throws Exception {
		File file = new File(cafTempPath + File.separator + fileName);
		if (file.exists()) {
			file.delete();
			logger.info("File :" + fileName + "Deleted");
		} else {
			logger.info("AuditManager Unable to delete File. File not found ::" + fileName);
		}
	}

	public Boolean save(CafAuditVo cafAuditVo, UserVo userVo) {
		try {
			// Update CafStatus tbl (Setting end date and latestInd to false)
			boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false, cafAuditVo.getCafStatusId());
			logger.info("Audit Rows Updated  in updateCafStatus ::"+poolResult);
			// Insert status to Audit accepted
			CafStatusVo cafStatusVo = new CafStatusVo();
			cafStatusVo.setCafTblId(cafAuditVo.getId());
			cafStatusVo.setLatestInd(true);
			cafStatusVo.setStatusId(StatusCodeEnum.AUDIT_COMPLETE.getStatusCode());
			cafStatusVo.setCreatedBy(userVo.getId());
			cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
			logger.info("Saved Status Audit Accepted  with id ::" + cafStatusVo.getId());
			
			// Delete the images
			String[] imageNames = cafAuditVo.getImages1().split(",");
			if (imageNames.length > 0) {
				for (int i = 0; i < imageNames.length; i++) {
					if (!imageNames[i].equals("")) {
						deleteFile(imageNames[i]);
					}
				}
			}
			return true;
		} catch (Exception e) {
			logger.error("Error in save() of AuditManager :: " + e.getMessage(),e);
			return false;
		}
	}

	public List<RemarkVo> getAuditRejectReasons() {
		return remarkDao.getAuditRejectReasons();
	}
	
	public void deleteAuditPool(){		
		auditDao.deleteAuditPool();			
	}

	public CafAuditVo getCafData(Long cafTblId) {
		CafAuditVo cafAuditVo = null;
		List<CafAuditVo> cafAuditVos =null;
		try{						 
			CafStatusVo cafStatusVo = auditDao.getCafStatusDataByCafTblId(cafTblId);
			if(cafStatusVo != null){
					// Get details from caf_tbl
					CafVo cafVo = cafDao.findCafById(cafStatusVo.getCafTblId());
					logger.info("cafVo = " + cafVo);
					cafAuditVo = new CafAuditVo();
					BeanUtils.copyProperties(cafAuditVo, cafVo);
					cafAuditVo.setCafStatusId(cafStatusVo.getId());
					cafAuditVos = auditDao.findSlcRejectData(cafStatusVo.getCafTblId());
					for (CafAuditVo cafAuditVo2 : cafAuditVos) {
						cafAuditVo.setRejectReason(cafAuditVo2.getRejectReason());
						cafAuditVo.setUsername(cafAuditVo2.getUsername());
						cafAuditVo.setCreatedDate(cafAuditVo2.getCreatedDate());
					}
					return cafAuditVo;
				}
		}catch(Exception e){
			cafAuditVo = null;
		}		
		return cafAuditVo;
	}
}
