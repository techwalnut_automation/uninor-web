package speedoc.co.in.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import speedoc.co.in.dao.IVendorDao;
import speedoc.co.in.vo.VendorUserVo;
import speedoc.co.in.vo.VendorVo;

@Component
public class VendorManager {
	
	@Autowired
	IVendorDao vendorDao;
	
	public VendorVo addVendor(VendorVo vendorVo){
		return vendorDao.addVendor(vendorVo);
	}
	
	public VendorUserVo addVendorUser(VendorUserVo vendorUserVo) throws Exception{
		return vendorDao.addVendorUser(vendorUserVo);		
	}
	
	public VendorVo getVendorByAdminUserId(Long adminId){
		return vendorDao.getVendorByAdminUserId(adminId);
	}
	
}
