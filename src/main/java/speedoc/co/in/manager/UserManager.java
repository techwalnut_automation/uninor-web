package speedoc.co.in.manager;

import java.security.MessageDigest;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import speedoc.co.in.dao.IRolesDao;
import speedoc.co.in.dao.IUserDao;
import speedoc.co.in.dao.IVendorDao;
import speedoc.co.in.mail.MailMail;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.RoleVo;
import speedoc.co.in.vo.UserVo;
import speedoc.co.in.vo.VendorScanWorkstationVo;
import speedoc.co.in.vo.VendorUserVo;
import speedoc.co.in.vo.VendorVo;

@Component
public class UserManager {

	private static Logger logger = Logger.getLogger(UserManager.class);
	
	@Autowired
	protected IUserDao userDao;

	@Autowired
	private IRolesDao roleDao;

	@Autowired
	private SessionService sessionService;
	
	@Autowired
	IVendorDao vendorDao;
	
	@Autowired
	private MailMail mail;
	
	@Value("${FILE_URL}")
	private String fileUrl;
	
	private TransactionTemplate txTemplate;

	@Autowired
	public void setTransactionManager(PlatformTransactionManager txManager) {
		this.txTemplate = new TransactionTemplate(txManager);
		this.txTemplate.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED);
	}

	public List<UserVo> getAllUser() {

		return userDao.getAllUsers();
	}

	public UserVo findById(Long userId) {
		return userDao.findById(userId);
	}

	public UserVo findByUserName(String userName) throws Exception {

		return userDao.findByUsername(userName);
	}

	public UserVo findByUniqueUserName(String username) {
		return userDao.findByUniqueUserName(username);
	}
	
	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public void saveOrUpdate(final UserVo userVo,final String userType) throws Exception {
		
		txTemplate.execute(new TransactionCallbackWithoutResult() {

			@Override
			protected void doInTransactionWithoutResult(TransactionStatus status) {
				try {
					Long loginUserId = sessionService.getUserVo().getId();
					if(loginUserId != null && loginUserId != 0){
						userVo.setCreatedBy(loginUserId);
						userVo.setActive(0);
						String pswd = userVo.getPwd();				
						if (userVo.getId() == null) {
							userVo.setPwd(toMD5(userVo.getPwd()));
						}
						
						UserVo uservo = userDao.saveOrUpdate(userVo, userType);
						
						if(uservo.getId() != null && uservo.getId() != 0){
							
							VendorVo vendorVo = vendorDao.getVendorDataByUserId(loginUserId);
							if ( vendorVo.getId() != null &&  vendorVo.getId() != 0) {
								Long vendorId = vendorVo.getId();
								VendorUserVo savedVendorUser = addToVendorUser(uservo.getId() , vendorId);
								
								if(savedVendorUser.getId() != null && savedVendorUser.getId() != 0){
									
									if (userType.equalsIgnoreCase("Scan User")) {
										uservo.setPwd(pswd);
										VendorScanWorkstationVo vendorScanWorkstationVo = addUserToVendorScanWorkStation(uservo,vendorId);
										if(vendorScanWorkstationVo.getId() != null && vendorScanWorkstationVo.getId() != 0){
											sendMail(vendorScanWorkstationVo.getRegKey(), uservo);
										}else{
											throw new Exception("vendorScanWorkstationVo.getId() is Null or 0");
										}	
									}							
								}else{
									throw new Exception("savedVendorUser id is Null or 0");
								}
							}else{
								throw new Exception("vendorId is Null or 0");
							}										
						}else{
							throw new Exception("userId is Null or 0");
						}
					}else{
						throw new Exception("loginUserId is Null or 0");
					}			
				} catch (Exception e) {
					logger.error("Unable to create User \n" + e);
					status.setRollbackOnly();
				}
			}
		});
		
	}
	
	private VendorUserVo addToVendorUser(Long userId, Long vendorId) throws Exception {
		VendorUserVo vendorUserVo = new VendorUserVo();
		vendorUserVo.setVendorId(vendorId);
		vendorUserVo.setUserId(userId);
		return vendorDao.addVendorUser(vendorUserVo);
	}

	private VendorScanWorkstationVo addUserToVendorScanWorkStation(UserVo uservo,Long vendorId) throws Exception {
		
		VendorScanWorkstationVo vendorScanWorkstationVo = new VendorScanWorkstationVo();
		vendorScanWorkstationVo.setRegKey(toMD5(uservo.getId()+ uservo.getEmailAddress()));			
		vendorScanWorkstationVo.setMacId(null);
		vendorScanWorkstationVo.setVendorId(vendorId);
		vendorScanWorkstationVo.setUserId(uservo.getId());
		return vendorDao.addVendorScanWorkStation(vendorScanWorkstationVo);
	
	}

	public void sendMail(String regKey, UserVo userVo) throws Exception {
		String url = fileUrl + regKey + "&uvId=" + userVo.getId();
		mail.sendEmail(userVo, url);
	}

	public RoleVo getAllRolesNameByUserId(Long userId) {
		return roleDao.getAllRolesNameByUserId(userId);
	}

	public List<UserVo> getUsersByRowsCount(FilterVo<UserVo> filterVo) {
		return userDao.getUsersByRowsCount(filterVo);
	}

	public Long getUsersCount() {
		return userDao.getUsersCount();
	}

	public static String toMD5(String stringVal) throws Exception {
		
			byte[] stringValInBytes = stringVal.getBytes();
			MessageDigest algorithm = MessageDigest.getInstance("MD5");
			algorithm.reset();
			algorithm.update(stringValInBytes);
			byte messageDigest[] = algorithm.digest();

			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				if ((0xff & messageDigest[i]) < 0x10) {
					hexString.append("0"
							+ Integer.toHexString((0xFF & messageDigest[i])));
				} else {
					hexString.append(Integer
							.toHexString(0xFF & messageDigest[i]));
				}
			}
			return hexString + "";
	}

	public boolean getExistingUserName(String username) {
		return userDao.getExistingUserName(username);
	}

	public List<UserVo> getUserList(FilterVo<UserVo> filterVo) {
		return userDao.getUserList(filterVo);
	}


}
