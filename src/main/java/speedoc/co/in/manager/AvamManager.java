package speedoc.co.in.manager;

import java.io.File;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import speedoc.co.in.dao.IActivationOfficerDao;
import speedoc.co.in.dao.IAvamDao;
import speedoc.co.in.dao.ICafDao;
import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.dao.IScanDao;
import speedoc.co.in.dao.IUserDao;
import speedoc.co.in.service.SessionService;
import speedoc.co.in.util.AvamExcelUtil;
import speedoc.co.in.util.CAFUtils;
import speedoc.co.in.util.MonthEnum;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.util.Utils;
import speedoc.co.in.vo.AvamImportVo;
import speedoc.co.in.vo.AvamVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.FilterVo;
import speedoc.co.in.vo.PosCposVo;

@Component
@Transactional
public class AvamManager {

	protected static Logger logger = Logger.getLogger(AvamManager.class);

	@Autowired
	IAvamDao avamDao;

	@Autowired
	ICafDao cafDao;

	@Autowired
	IScanDao scanDao;

	@Autowired
	ICafStatusDao cafStatusDao;
	
	@Autowired
	IUserDao userDao;
	
	@Autowired
	SessionService sessionService;
	
	@Autowired
	IActivationOfficerDao activationOfficerDao;

	@Value("${CAF_DATA_PATH}")
	private String cafDataPath;

	@Value("${AVAM_USER_NAME}")
	private String avamUserName;
		
	@Value("${AVAM_ERROR_DATA_PATH}")
	private String avamErrorDataPath;

	private TransactionTemplate txTemplate;

	@Autowired
	public void setTransactionManager(PlatformTransactionManager txManager) {
		this.txTemplate = new TransactionTemplate(txManager);
		this.txTemplate
				.setPropagationBehavior(DefaultTransactionDefinition.PROPAGATION_REQUIRED);
	}

	public AvamVo addNewAvamNumber(AvamVo avamVo) {
		AvamVo savedAvamVo = avamDao.addNewAvamNumber(avamVo);
		return savedAvamVo;
	}

	public AvamVo getAvamBySpeedocId(String speedocId) {
		AvamVo avamVo = avamDao.getAvamVoBySpeedocId(speedocId);
		return avamVo;
	}

	public boolean updateAvamTblByAvamId(CafVo cafVo, Long avamTblId) throws Exception {
		return avamDao.updateAvamTblByAvamId(cafVo, avamTblId);
	}

	// Schedular for Avam_Fail to Avam_Success
	public void convertAvamFailToAvamSuccess() {
		AvamFailToSuccessTransactionCallbackHandler handler = null;
		List<CafStatusVo> avamFailAndFLCCompleteRecords = avamDao.getAvamFailAndFLCCompleteRecords();
		logger.info( " Avam Fail Record Size :: " + avamFailAndFLCCompleteRecords.size());
		for (CafStatusVo cafStatusVo : avamFailAndFLCCompleteRecords) {
			CafVo cafVo = null;
			AvamVo avamVo = null;
			try {
				cafVo = cafDao.findCafById(cafStatusVo.getCafTblId());
				avamVo = avamDao.getAvamVoBySpeedocId(cafVo.getSpeedocId());
				if(avamVo != null){
					handler = new AvamFailToSuccessTransactionCallbackHandler(cafVo, cafStatusVo, avamVo);
					txTemplate.execute(handler);
				}
			} catch (Exception e) {
				logger.error("convertAvamFailToAvamSuccess :: "+e.getMessage(),e);
			}
		}
	}

	private String getFileLocation(CafVo cafVo, AvamVo avamVo) throws Exception {
		String fileName = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
		String locationDate = dateFormat.format(cafVo.getScannedDate().getTime());
		String speedocId = StringUtils.trimToNull(avamVo.getSpeedocId());
		if (speedocId != null) {
			fileName = speedocId + ".pdf";
		} else {
			logger.error("File Location Not Decided Properly. :: cafVO " + cafVo.toString());
			throw new Exception();
		}
		String newCafFileLocation = CAFUtils.checkAndGetCafFileLocation(cafDataPath, locationDate, FilenameUtils.getBaseName(fileName));
		newCafFileLocation = locationDate + File.separator + newCafFileLocation;
		logger.info(" newCafFileLocation :: " + newCafFileLocation);
		return newCafFileLocation;
	}

	class AvamFailToSuccessTransactionCallbackHandler extends TransactionCallbackWithoutResult {
		private AvamVo avamVo = null;
		private CafVo cafVo = null;
		private CafStatusVo cafStatusVo = null;

		public AvamFailToSuccessTransactionCallbackHandler(CafVo cafVo, CafStatusVo cafStatusVo, AvamVo avamVo) {
			super();
			this.avamVo = avamVo;
			this.cafVo = cafVo;
			this.cafStatusVo = cafStatusVo;
		}

		@Override
		protected void doInTransactionWithoutResult(TransactionStatus status) {
			try {
				cafStatusDao.updateCafStatusLatestToOld(false,cafStatusVo.getId());
				cafStatusDao.saveCafStatus(Utils.getCafStatusVoForAvamStatus(StatusCodeEnum.AVAM_SUCCESS.getStatusCode(), cafVo));
				updateAvamTblByAvamId(cafVo, avamVo.getId());

				if(cafStatusVo.getStatusId() == 3001){
					String oldCafFileLocation = cafDataPath + File.separator+ cafVo.getCafFileLocation();
					String newCafFileLocation = getFileLocation(cafVo, avamVo);
					cafVo.setCafFileLocation(newCafFileLocation);
					CAFUtils.moveFile(oldCafFileLocation, cafDataPath+ File.separator + newCafFileLocation);
				}
				cafVo.setModifiedDate(new Timestamp(new Date().getTime()));
				scanDao.updateCafTblByAvamData(cafVo, avamVo);
			} catch (Exception e) {
				status.setRollbackOnly();
				logger.error("doInTransactionWithoutResult :: "+e.getMessage());
			}
		}
	}

	public boolean readExcelData(String avamFilePath) {
		logger.info("loading file =  " + avamFilePath);
		ArrayList<AvamVo> excelAvamVoList = new ArrayList<AvamVo>();
		try {
			XSSFSheet sheet0 = AvamExcelUtil.getSheetFromExcel(avamFilePath);
			logger.info("sheet0.getPhysicalNumberOfRows() :: "+ sheet0.getPhysicalNumberOfRows());
			for (int i = 1; i < sheet0.getPhysicalNumberOfRows(); i++) {
				AvamVo avamVoFromExcel = AvamExcelUtil.readSheetRowWise(sheet0.getRow(i));
				if(avamVoFromExcel != null){
					logger.info("avamVoFromExcel.getCaoCode() :: "+avamVoFromExcel.getCaoCode()+"avamVoFromExcel.getActivationDate() :: "+avamVoFromExcel.getActivationDate());
					Boolean isAlreadyExists=activationOfficerDao.isCaoCodeAlreadyExist(avamVoFromExcel.getCaoCode(),0); 
					if(!isAlreadyExists){
						avamVoFromExcel=null;
					}
				}				
				if (avamVoFromExcel == null) {
					String errorFileName = FilenameUtils.getBaseName(avamFilePath) + "_error.xlsx";
					File f = new File(avamErrorDataPath + File.separator+ errorFileName);
					if (f.exists()) {
						logger.info(" inside if :: error file exists.");
						AvamExcelUtil.addRoWToErrorFile(sheet0.getRow(i),f.getPath());
					} else {
						logger.error(" inside else :: error file not exists.. now creating file");
						String excelErrorFilePath = AvamExcelUtil.createErrorFile(f.getPath(),sheet0.getRow(0));
						AvamExcelUtil.addRoWToErrorFile(sheet0.getRow(i),excelErrorFilePath);
					}
				} else {
					excelAvamVoList.add(avamVoFromExcel);
				}
			}
			logger.info("excelAvamVoList.size() :: " + excelAvamVoList.size());
			if (excelAvamVoList.size() > 0) {
				Integer recordInserted = 0;
				Integer duplicateRecord = 0;
				Long avamImportId = addAvamExcelImportId(avamFilePath,sheet0.getPhysicalNumberOfRows() - 1);
				if (avamImportId != 0 && avamImportId != null) {
					for (int i = 0; i < excelAvamVoList.size(); i++) {
						AvamVo avamVo = (AvamVo) excelAvamVoList.get(i);
						Long count = avamDao.getAvamCountBySpeedocId(avamVo.getSpeedocId());
						if (count == 0) {						
							avamVo.setImportId(avamImportId);
							avamVo.setCreatedBy(userDao.findByUniqueUserName(avamUserName.trim()).getId());
							avamVo.setActivationDate(avamVo.getActivationDate());
							avamVo.setCafTrackingStatus(avamVo.getCafTrackingStatus());
							/**
							 * It insert in avam table
							 */
							AvamVo insertedAvamVo = addNewAvamNumber(avamVo);
							PosCposVo posCposVo =new PosCposVo();
							posCposVo.setSpeedocId(avamVo.getSpeedocId());
							posCposVo.setMobileNumber(avamVo.getMobileNumber());
							posCposVo.setCafNumber(avamVo.getCafNumber());
							posCposVo.setRtrCode(avamVo.getPosCode());
							posCposVo.setRtrName(avamVo.getPosName());
							PosCposVo addNewPosTbl = addNewPosTbl(posCposVo);
							if (insertedAvamVo.getId() != null && addNewPosTbl.getId() != null) {
								recordInserted = recordInserted + 1;
								logger.info("recoredInserted :: "+ recordInserted);
							} else {
								logger.error("record not inserted.:: ");
							}
						} else {
							duplicateRecord = duplicateRecord + 1;
							logger.info(" duplicateRecord :: "+ duplicateRecord);
						}
					}
				} else {
					logger.info("avamImportId = null or 0 ");
				}
				avamDao.updateAvamImportByImportId(recordInserted,duplicateRecord, avamImportId);
			}
		} catch (Exception e) {
			logger.error("readExcelData :: "+e.getMessage());
		}
		return true;
	}
	
	public PosCposVo addNewPosTbl(PosCposVo posCposVo) {
		PosCposVo posCposVo2 = avamDao.addNewPosTbl(posCposVo);
		return posCposVo2;
	}

	private Long addAvamExcelImportId(String avamFilePath, int totalRecords) throws Exception {
		AvamImportVo avamImportVo = new AvamImportVo();
		avamImportVo.setCreatedBy(userDao.findByUniqueUserName(avamUserName.trim()).getId());
		avamImportVo.setFileName(FilenameUtils.getName(avamFilePath));
		avamImportVo.setStatus("INPROCESS");
		avamImportVo.setTotalRecord(totalRecords);
		AvamImportVo insertedAvamImportRecordVo = avamDao.addAvamImportRecord(avamImportVo);
		return insertedAvamImportRecordVo.getId();
	}

	public List<AvamImportVo> getAvamMonthWiseList(
			FilterVo<AvamImportVo> filterVo, String month) {
		// month MM yy
		if (month != null && !month.equals("")) {
			String[] monthArr = month.split(" ");
			String monthName = monthArr[0];
			logger.info("monthName = " + monthName);
			String year = monthArr[1];
			logger.info("year = " + year);
			int monthNumber = getMonthNumber(monthName);
			return avamDao.getAvamMonthWiseList(filterVo, monthNumber,year);
		}
		return null;
	}
	
	public static int getMonthNumber(String monthName) {
		int monthNum = 0;

		if (monthName == null) {
			return monthNum;
		}

		switch (MonthEnum.valueOf(monthName)) {
		case January:
			monthNum = 1;
			break;
		case February:
			monthNum = 2;
			break;
		case March:
			monthNum = 3;
			break;
		case April:
			monthNum = 4;
			break;
		case May:
			monthNum = 5;
			break;
		case June:
			monthNum = 6;
			break;
		case July:
			monthNum = 7;
			break;
		case August:
			monthNum = 8;
			break;
		case September:
			monthNum = 9;
			break;
		case October:
			monthNum = 10;
			break;
		case November:
			monthNum = 11;
			break;
		case December:
			monthNum = 12;
			break;
		default:
			monthNum = 0;
			break;
		}
		return monthNum;
	}

	public Long getAvamCount() {
		return avamDao.getAvamCount();
	}
}
