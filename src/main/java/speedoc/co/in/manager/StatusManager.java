package speedoc.co.in.manager;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.IStatusDao;
import speedoc.co.in.vo.FtpReportVo;
import speedoc.co.in.vo.StatusViewerVo;

@Component
@Transactional
public class StatusManager {
	
	@Value("${FTP_FILE_PATH}")
	private String ftpFilePath;
	
	@Value("${FTP_POST_URL}")
	private String ftpPostUrl;

	@Autowired
	IStatusDao statusDao;
	private static Logger logger=Logger.getLogger(StatusManager.class);

	public List<StatusViewerVo> getCountbyAdmin(String fromdate, String todate,
			String username) {
		logger.info(" fromdate :: " + fromdate + " todate :: " + todate);
		return statusDao.getCountbyAdmin(fromdate, todate, username);
	}

	public File getFtpReport(Timestamp fromDate, Timestamp toDate) {
		File file = null;
		BufferedWriter output = null;
		try {					
			List<FtpReportVo> ftpReportVos = statusDao.getFtpReport(fromDate,toDate);
				
				if(ftpReportVos != null && ftpReportVos.size() > 0){
					logger.info("ftpReportVos.size() :: "+ftpReportVos.size());		
					String filePath = ftpFilePath + File.separator + "FTP_UPLOAD_FILE.txt";
//					String filePath = ftpFilePath + File.separator + "FTP_UPLOAD_FILE.bat";
					file = new File(filePath);
					
					output = new BufferedWriter(new FileWriter(file));
					for (FtpReportVo ftpReportVo : ftpReportVos) {										
						String subSpeedocId = ftpReportVo.getSpeedocId().substring(9);
						logger.info("subSpeedocId :: "+subSpeedocId);
						if(subSpeedocId != null){
							output.write("wget -O " + ftpReportVo.getMobileNumber() + "_CAF_POA_POI" + subSpeedocId + ".pdf " + ftpPostUrl + ftpReportVo.getCafFileLocation());
//							output.write(ftpPostUrl + ftpReportVo.getCafFileLocation());
							output.newLine();
						}else{
							output.write("wget -O " + ftpReportVo.getMobileNumber() + "_CAF_POA_POI" + ".pdf " + ftpPostUrl + ftpReportVo.getCafFileLocation());
//							output.write(ftpPostUrl + ftpReportVo.getCafFileLocation());
							output.newLine();
						}		
					}	
					output.close();	
				}														
		} catch (IOException e) {
			logger.error("Error in getFtpReport StatusManager :: "+e.getMessage(),e);
		}
		return file;
	}

	public File getIndexingData(String fromDate, String toDate) {
		File filePath = null;
		try{
			String indexingPath="/tmp"+File.separator + "INDEXING_DATA_"+fromDate+"_"+toDate+".csv";
			filePath = new File(ftpFilePath + File.separator + "INDEXING_DATA_"+fromDate+"_"+toDate+".csv");
			
			if(!filePath.exists()){
				logger.info("file not exists.");
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				Date dateStr = formatter.parse(fromDate);
				String endDate = toDate + " 23:59:00";
				SimpleDateFormat formatter1 = new SimpleDateFormat(
						"dd/MM/yyyy hh:mm:ss");
				Date dateEnd = formatter1.parse(endDate);

				Timestamp timestamp = new Timestamp(dateStr.getTime());
				Timestamp timestamp2 = new Timestamp(dateEnd.getTime());
				
				statusDao.getIndexingData(timestamp,timestamp2,indexingPath);		
				FileUtils.moveFile(new File(indexingPath), filePath);
			}else{
				logger.info("file exists.");
			}
		}catch (Exception e) {
			logger.error("Error in getIndexingData :: "+e.getMessage(),e);
		}
		return filePath;
	}
	
	public File getScruitinyRejectData(String fromDate, String toDate) {
		File filePath = null;
		String indexingPath= null;
		try{
			String newFromDate = fromDate.replace("/","_");
			String newT0Date = toDate.replace("/","_");
			
			indexingPath="/tmp"+File.separator + "SCRUTINY_REJECT_"+newFromDate+"_"+newT0Date+".csv";
			filePath = new File(ftpFilePath + File.separator + "SCRUTINY_REJECT_"+newFromDate+"_"+newT0Date+".csv");
			
			if(!filePath.exists()){
				logger.info("file not exists.");
				SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
				Date dateStr = formatter.parse(fromDate);
				String endDate = toDate + " 23:59:00";
				SimpleDateFormat formatter1 = new SimpleDateFormat(
						"dd/MM/yyyy hh:mm:ss");
				Date dateEnd = formatter1.parse(endDate);

				Timestamp timestamp = new Timestamp(dateStr.getTime());
				Timestamp timestamp2 = new Timestamp(dateEnd.getTime());
				
				statusDao.getScruitinyRejectData(timestamp,timestamp2,indexingPath);		
				FileUtils.moveFile(new File(indexingPath), filePath);
			}else{
				logger.info("file exists.");
			}
		}catch (Exception e) {
			logger.error("Error in getScruitinyRejectData :: "+e.getMessage(),e);
		}
		return filePath;
	}
}
