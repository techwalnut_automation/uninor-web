package speedoc.co.in.manager;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import java.sql.BatchUpdateException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import speedoc.co.in.dao.ICafDao;
import speedoc.co.in.dao.ICafStatusDao;
import speedoc.co.in.dao.IFlcPoolDao;
import speedoc.co.in.dao.IFlcRemarkDao;
import speedoc.co.in.dao.IRemarkDao;
import speedoc.co.in.util.StatusCodeEnum;
import speedoc.co.in.vo.CafFlcVo;
import speedoc.co.in.vo.CafStatusVo;
import speedoc.co.in.vo.CafVo;
import speedoc.co.in.vo.FlcAcceptVo;
import speedoc.co.in.vo.FlcRemarkVo;
import speedoc.co.in.vo.RemarkVo;
import speedoc.co.in.vo.UserVo;

@Component
@Transactional
public class FlcManager {
	/*@Autowired
	SessionFactory sessionFactory;*/

	@Autowired
	private IFlcPoolDao flcPoolDao;

	@Autowired
	private ICafDao cafDao;

	@Autowired
	private ICafStatusDao cafStatusDao;

	@Autowired
	private IRemarkDao remarkDao;

	@Autowired
	private IFlcRemarkDao flcRemarkDao;

	@Value("${CAF_DATA_PATH}")
	private String cafDataPath;

	@Value("${TEMP_DATA_PATH}")
	private String cafTempPath;

	private Logger log = Logger.getLogger(FlcManager.class);

	public void deleteFile(String fileName) throws Exception {
		File file = new File(cafTempPath + File.separator + fileName);
		if (file.exists()) {
			file.delete();
			log.info("File :" + fileName + "Deleted");
		} else {
			log.info("Unable to delete File. File not found ::" + fileName);
		}
	}

	public void getCafFile(String fileName, OutputStream out) {
		fileName = cafTempPath + File.separator + fileName;
		// fileName = "D:\\data01\\appdata\\speedoc\\uninor\\temp-files"+
		// File.separator + fileName;
		try {
			FileUtils.copyFile(new File(fileName), out);
		} catch (SocketException e1) {
			log.error(e1.getMessage(), e1.getCause());
		} catch (IOException e1) {
			log.error(e1.getMessage(), e1);
		} finally {
			try {
				out.close();
				out.flush();
			} catch (Exception e) {
				log.error(e.getMessage(), e);
			}
		}
	}

	public CafFlcVo getUnknownCaf(UserVo userVo) {
		CafFlcVo cafFlcVo = null;
		try {
			// Get unknown caf from caf_status tbl
			CafStatusVo cafStatusVo = flcPoolDao.getUnknownCaf(userVo); // fetch
																		// with
																		// limit
																		// 1
			log.info("cafStatusVo=" + cafStatusVo);
			CafVo cafVo = null;
			if (cafStatusVo != null) {
				// Get details from caf_tbl
				cafVo = cafDao.findCafById(cafStatusVo.getCafTblId());
				log.info("cafVo = " + cafVo);
				if (cafVo != null) {
					cafFlcVo = new CafFlcVo();
					cafFlcVo.setCafId(cafStatusVo.getCafTblId());
					cafFlcVo.setFileLocation(cafVo.getCafFileLocation());
					cafFlcVo.setCurrentStatus(StatusCodeEnum.UNKNOWN.toString());
					cafFlcVo.setCafStatusId(cafStatusVo.getId());
					cafFlcVo.setPages(cafVo.getPages());
					log.info("cafFlcVo :: " + cafFlcVo);
					return cafFlcVo;
				}
			}
			return cafFlcVo;
		} catch (BatchUpdateException e) {
			return this.getUnknownCaf(userVo);
		} catch (Exception e) {
			log.error(
					"Error in getUnknownCafs() of FlcPoolManager :: "
							+ e.getMessage(), e);
			return cafFlcVo;
		}
	}

	public Boolean save(CafFlcVo cafFlcVo, UserVo userVo) {
		try {
			
			/* 
			 * @author : Kishor Mendhe
			 * @Date : 02-08-2015
			 * 
			 *  added procedure in checkOnholdAndSpeedocIdExist()
			 *	
			 *	starts
			 */
			
			FlcAcceptVo flcAcceptVo = cafDao.checkOnholdAndSpeedocIdExist(cafFlcVo.getSpeedocId(),cafFlcVo.getCafId(),userVo.getId());

			log.info(" FlcManager :: updatedResult : "+flcAcceptVo.getUpdatedResult());
			
			if (flcAcceptVo.getUpdatedResult().equalsIgnoreCase("UPDATED")) {

				log.info("Inside if updatedResult == UPDATED ");

				// Update CafStatus tbl (Setting end date and latestInd to false)
				
				boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false, cafFlcVo.getCafStatusId());
				log.info("Flc Rows Updated  in updateCafStatus ::" + poolResult);
				// Insert status to Flc accepted
				CafStatusVo cafStatusVo = new CafStatusVo();
				cafStatusVo.setCafTblId(cafFlcVo.getCafId());
				cafStatusVo.setLatestInd(true);
				cafStatusVo.setStatusId(StatusCodeEnum.FLC_ACCEPT.getStatusCode());
				cafStatusVo.setCreatedBy(userVo.getId());
				log.info("userVo.getId() :: " + userVo.getId());
				
				if (!cafStatusDao.alreadyFLCComplete(cafStatusVo)) {
					cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
					log.info("Saved Status Accepted with id ::"+ cafStatusVo.getId());
				}
				// Delete the images
				String[] imageNames = cafFlcVo.getImages().split(",");
				if (imageNames.length > 0) {
					for (int i = 0; i < imageNames.length; i++) {
						if (!imageNames[i].equals("")) {
							deleteFile(imageNames[i]);
						}
					}
				}
				return true;
			}
			
			/*
			 * Kishor ends
			 * 
			 * */
			
			
			/*if(cafFlcVo.getCafId() != null){
				 CafVo cafVo1 = cafDao.findOnhold(cafFlcVo.getCafId());
				 Integer onhold = cafVo1.getOnhold();
				 log.info("cafVo1 :: "+cafVo1);
				 if(onhold == null){
					 onhold = 0;
				 }
				 if(onhold <= 5){
					 
					 
					 Boolean bool = flcPoolDao.findSpeedocIdExistInAvam(cafFlcVo.getSpeedocId());
					 if(bool == true){
						 //check already exist speedoc id
						 CafVo cafVo = cafDao.findSpeedocIdAlreadyExists(cafFlcVo.getSpeedocId());
						 log.info("List of cafVo :: "+cafVo);
						 // Update caf_tbl			 
						 int result = 0;
						 int flag = 0;	
						 Integer subFlag = 0;
						 if(cafVo != null){
							 log.info("inside if statement");						 
							 if(cafVo.getSpeedocId().equals(cafFlcVo.getSpeedocId())){
								 String speedocId = cafVo.getSpeedocId().substring(0, 9);
								 log.info(" if speedocId :: "+speedocId);
								 subFlag=null;
								 log.info(" if subFlag :: "+subFlag);
							 }else{
								 String subString = cafVo.getSpeedocId().substring(11);
								 log.info("else subString :: "+subString);
								 String speedocId = cafVo.getSpeedocId().substring(0, 9);
								 log.info("else speedocId :: "+speedocId);
								 subFlag=Integer.parseInt(subString);
								 log.info("else subFlag :: "+subFlag);
							 }					
							 if(subFlag == null){	
								 log.info("if flag :: "+flag);
								 flag++;
								 result = cafDao.updateSpeedocIdInCafById(cafFlcVo.getSpeedocId()+"_r"+flag, cafFlcVo.getCafId(),userVo);
			 				 }else{
			 					log.info("else subFlag :: "+subFlag);
								 subFlag++;
								 result = cafDao.updateSpeedocIdInCafById(cafFlcVo.getSpeedocId()+"_r"+subFlag, cafFlcVo.getCafId(),userVo);
							 }				 								
						 }else{
							 log.info("inside else statement");
							 result = cafDao.updateSpeedocIdInCafById(cafFlcVo.getSpeedocId(), cafFlcVo.getCafId(),userVo);
						 }				
						 log.info("result="+result);
						 if(result > 0){
							// Update CafStatus tbl (Setting end date and latestInd to false)
								
							boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false, cafFlcVo.getCafStatusId());
							log.info("Flc Rows Updated  in updateCafStatus ::"+poolResult);
							// Insert status to Flc accepted
							CafStatusVo cafStatusVo = new CafStatusVo();
							cafStatusVo.setCafTblId(cafFlcVo.getCafId());
							cafStatusVo.setLatestInd(true);
							cafStatusVo.setStatusId(StatusCodeEnum.FLC_ACCEPT.getStatusCode());
							log.info("userVo.getId() :: "+userVo.getId());
							cafStatusVo.setCreatedBy(userVo.getId()) ;
							
							if(!cafStatusDao.alreadyFLCComplete(cafStatusVo)){
								cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
								log.info("Saved Status Accepted with id ::"+cafStatusVo.getId());
							}
							// Delete the images 
							String[] imageNames = cafFlcVo.getImages().split(",");
							if(imageNames.length > 0){
								for (int i = 0; i < imageNames.length; i++) {
									if(!imageNames[i].equals("")){
										deleteFile(imageNames[i]);
									}
								}
							}
							return true ;
						 }
					 }			
				   }
				 }*/		
		}catch(Exception e){
			log.error("Error in save() of FlcPoolManager :: "+e.getMessage(),e);
			return false ;
		}
		return null;
	}

	public Boolean rejectFlc(String rejectReason, CafFlcVo cafFlcVo,
			UserVo userVo) {
		try {
			// Update CafStatus tbl (Setting end date and latestInd to false)
			boolean poolResult = cafStatusDao.updateCafStatusLatestToOld(false,
					cafFlcVo.getCafStatusId());
			log.info("flc Rows Updated  in updateCafStatus ::" + poolResult);
			// Add status
			log.info("cafFlcVo.id = " + cafFlcVo.getCafId());
			CafStatusVo cafStatusVo = new CafStatusVo();
			cafStatusVo.setCafTblId(cafFlcVo.getCafId());
			cafStatusVo.setLatestInd(true);
			cafStatusVo.setStatusId(StatusCodeEnum.FLC_REJECT.getStatusCode());
			cafStatusVo.setCreatedBy(userVo.getId());
			cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
			log.info("Saved Status Rejected with id ::" + cafStatusVo.getId());

			String[] rejectReasonArr = rejectReason.split(",");
			// Save remarks in Remark Tbl
			for (int i = 0; i < rejectReasonArr.length; i++) {
				FlcRemarkVo flcRemarkVo = new FlcRemarkVo();
				flcRemarkVo.setCafTblId(cafFlcVo.getCafId());
				flcRemarkVo.setRemarkId(Long.parseLong(rejectReasonArr[i]
						.trim()));
				flcRemarkVo.setUserId(userVo.getId());
				flcRemarkVo.setStatusId(cafStatusVo.getId());
				Long id = flcRemarkDao.save(flcRemarkVo);
				log.info("Saved rejectFlc FlcRemarkVo with id ::" + id);
			}
			// Delete the images
			String[] imageNames = cafFlcVo.getImages().split(",");
			if (imageNames.length > 0) {
				for (int i = 0; i < imageNames.length; i++) {
					if (!imageNames[i].equals("")) {
						deleteFile(imageNames[i]);
					}
				}
			}
			return true;
		} catch (Exception e) {
			log.error(
					"Error in rejectFlc() of FlcPoolManager :: "
							+ e.getMessage(), e);
			return false;
		}
	}

	public Boolean skipFlc(CafFlcVo cafFlcVo, UserVo userVo) {
		try {
			CafVo cafVo = cafDao.getOnholdByCafId(cafFlcVo.getCafId());
			log.info("skipFlc cafVo :: " + cafVo);
			Integer onhold = cafVo.getOnhold();
			if (onhold == null) {
				onhold = 0;
			}
			log.info("onhold :: " + onhold);
			// update caf_tbl onhold
			if (onhold == 0 || onhold == null) {
				Boolean bool = cafDao.updateOnhold(cafFlcVo.getCafId(),
						onhold + 1);
				if (bool == true) {
					// update old status currently accesss id and Add new status
					@SuppressWarnings("unused")
					boolean bool1 = cafStatusDao.updateCurrentAccessUserId(
							cafFlcVo.getCafId(),
							StatusCodeEnum.UNKNOWN.getStatusCode());
					log.info(" if cafFlcVo.id = " + cafFlcVo.getCafId());
					CafStatusVo cafStatusVo = new CafStatusVo();
					cafStatusVo.setCafTblId(cafFlcVo.getCafId());
					cafStatusVo.setLatestInd(true);
					cafStatusVo.setStatusId(StatusCodeEnum.FLC_SKIP
							.getStatusCode());
					cafStatusVo.setCreatedBy(userVo.getId());
					cafStatusVo = cafStatusDao.saveCafStatus(cafStatusVo);
					log.info("If Saved Status Skiped with id ::"
							+ cafStatusVo.getId());
					return true;
				}
			} else {
				if (cafVo.getOnhold() < 5) {
					@SuppressWarnings("unused")
					boolean bool1 = cafStatusDao.updateCurrentAccessUserId1(
							cafFlcVo.getCafId(),
							StatusCodeEnum.FLC_SKIP.getStatusCode());
					Boolean bool = cafDao.updateOnhold(cafFlcVo.getCafId(),
							onhold + 1);
					if (bool == true) {
						return true;
					}
				} else {
					boolean poolResult = cafStatusDao
							.updateCafStatusLatestToOld(false,
									cafFlcVo.getCafStatusId());
					log.info("skipFlc :: Rows Updated  in updateCafStatus for rejectFlc() ::"
							+ poolResult);
					String[] imageNames1 = cafFlcVo.getImages().split(",");
					if (imageNames1.length > 0) {
						for (int j = 0; j < imageNames1.length; j++) {
							if (!imageNames1[j].equals("")) {
								deleteFile(imageNames1[j]);
							}
						}
					}
					return false;
				}
			}
		} catch (Exception e) {
			log.error(
					"Error in skipFlc() of FlcPoolManager :: " + e.getMessage(),
					e);
		}
		return null;
	}

	private void compressImage(File input) throws IOException {
		log.info("Inside compressImage");
		// File input = new File("digital_image_processing.jpg");
		BufferedImage image = ImageIO.read(input);

		File compressedImageFile = new File(input.getPath());
		OutputStream os = new FileOutputStream(compressedImageFile);
		Iterator<ImageWriter> writers = ImageIO
				.getImageWritersByFormatName("jpg");
		ImageWriter writer = (ImageWriter) writers.next();

		ImageOutputStream ios = ImageIO.createImageOutputStream(os);
		writer.setOutput(ios);

		ImageWriteParam param = writer.getDefaultWriteParam();
		param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
		param.setCompressionQuality(0.2f);
		writer.write(null, new IIOImage(image, null, null), param);
		os.close();
		ios.close();
		writer.dispose();
	}

	@SuppressWarnings("rawtypes")
	public String getCafImageNames(String cafFileName, boolean renameImage) {
		PDDocument document = null;
		String imageNames = "";
		// String cafFileName="29052014\\103500004.pdf";
		try {
			String tempJpgFilePath = cafTempPath + File.separator + cafFileName;
			String pdfPath = cafDataPath + File.separator + cafFileName;
			// String tempJpgFilePath =
			// "D:\\data01\\appdata\\speedoc\\uninor\\temp-files"+File.separator
			// + cafFileName;;
			// String pdfPath =
			// "D:\\data01\\appdata\\speedoc\\uninor\\caf-files"+File.separator
			// + cafFileName;
			document = PDDocument.load(pdfPath);

			log.info("After Loading");

			List pages = document.getDocumentCatalog().getAllPages();
			int imageCounter = 0;
			Iterator iter = pages.iterator();
			while (iter.hasNext()) {
				PDPage page = (PDPage) iter.next();
				PDResources resources = page.getResources();

				Map images = resources.getImages();

				if (images != null) {
					Iterator imageIter = images.keySet().iterator();
					while (imageIter.hasNext()) {
						String key = (String) imageIter.next();
						PDXObjectImage image = (PDXObjectImage) images.get(key);

						String cafFileNameNew = cafFileName.replaceAll(".pdf","");
						imageNames = imageNames + ";" + cafFileNameNew + "_"+ imageCounter + ".jpg";
						String name = tempJpgFilePath + "_" + imageCounter;
						name = name.replaceAll(".pdf", "");
						String folderForTemp = cafTempPath + File.separator
								+ cafFileName.split("/")[0];
						// String folderForTemp =
						// "D:\\data01\\appdata\\speedoc\\uninor\\temp-files"+File.separator
						// + cafFileName.split("/")[0];
						File tempFile = new File(tempJpgFilePath);

						File file = new File(tempFile.getParent());
						if (!file.exists()) {
							boolean success = (new File(tempFile.getParent()))
									.mkdir();
							if (success) {
								log.info("Directory: " + folderForTemp
										+ " created");
							} else {
								log.info("Directory: " + folderForTemp
										+ " creation failed");
							}
						}
						image.write2file(name);
						try {
							compressImage(new File(name + ".jpg"));
						} catch (Exception e) {
							log.info("error : " + e.getMessage(), e);
						}
						imageCounter++;
					}
				}
				if (renameImage) {
					break;
				}
			}
			document.close();
		} catch (Exception eX) {
			log.error("FlcManager :: getCafImageNames :: " + eX.getMessage(),
					eX);
			return null;
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (IOException e) {
					log.error(
							"FlcManager :: getCafImageNames :: "
									+ e.getMessage(), e);
				}
			}
		}
		if (imageNames == "") {
			imageNames = null;
		}
		imageNames = imageNames.replaceAll(".pdf", "");
		log.info("imageNames ::::::: " + imageNames);
		return imageNames;
	}

	public List<RemarkVo> getRejectReasons() {
		return remarkDao.getFlcRejectReasons();
	}

	public List<RemarkVo> getSlcRejectReasons() {
		return remarkDao.getSlcRejectReasons();
	}

	public void deleteFlcPool() {
		flcPoolDao.deleteFlcPool();
	}

	public boolean checkSpeedocIdExist(String speedocId) {
		return flcPoolDao.findSpeedocIdExistInAvam(speedocId);
	}

	public List<RemarkVo> getRejectReasons(String header) {
		return remarkDao.getRejectReasons(header);
	}

	public Boolean updateCafStatus(UserVo userVo) {
		try {
			Boolean updateCafStatus = cafStatusDao.updateCafStatus(userVo);
			return updateCafStatus;
		} catch (Exception e) {
			log.error(
					"Error in updateCafStatus() of FlcManager :: "
							+ e.getMessage(), e);
			return false;
		}
	}
}
