package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class IndexingDataVO implements Serializable{

	private static final long serialVersionUID = -6916167413795970633L;
	private Long id;
	private String voterId;
	private String name;
	private Integer age;
	private String fatherName;
	private String state;
	private String district;
	private String pollingStation;
	private String pollingStationNo;
	private String assemblyConstituency;
	private String parlimentryConstituency;
	private Timestamp createdDate;
	private String electroDataStatus;
	private Long electroId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getVoterId() {
		return voterId;
	}
	public void setVoterId(String voterId) {
		this.voterId = voterId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getPollingStation() {
		return pollingStation;
	}
	public void setPollingStation(String pollingStation) {
		this.pollingStation = pollingStation;
	}
	
	public String getPollingStationNo() {
		return pollingStationNo;
	}
	public void setPollingStationNo(String pollingStationNo) {
		this.pollingStationNo = pollingStationNo;
	}
	public String getAssemblyConstituency() {
		return assemblyConstituency;
	}
	public void setAssemblyConstituency(String assemblyConstituency) {
		this.assemblyConstituency = assemblyConstituency;
	}
	public String getParlimentryConstituency() {
		return parlimentryConstituency;
	}
	public void setParlimentryConstituency(String parlimentryConstituency) {
		this.parlimentryConstituency = parlimentryConstituency;
	}
	
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public String getElectroDataStatus() {
		return electroDataStatus;
		
	}
	public void setElectroDataStatus(String electroDataStatus) {
		this.electroDataStatus = electroDataStatus;
	}
	
	
	public Long getElectroId() {
		return electroId;
	}
	public void setElectroId(Long electroId) {
		this.electroId = electroId;
	}
	@Override
	public String toString() {
		return "IndexingDataVO [id=" + id + ", voterId=" + voterId + ", name="
				+ name + ", age=" + age + ", fatherName=" + fatherName
				+ ", state=" + state + ", district=" + district
				+ ", pollingStation=" + pollingStation + ", pollingStationNo="
				+ pollingStationNo + ", assemblyConstituency="
				+ assemblyConstituency + ", parlimentryConstituency="
				+ parlimentryConstituency + ", createdDate=" + createdDate
				+ ", electroDataStatus=" + electroDataStatus + ", electroId="
				+ electroId + "]";
	}
	
}
