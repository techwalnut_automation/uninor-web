package speedoc.co.in.vo;

import java.sql.Timestamp;


public class AvamVo {
	private Long id;
	private Long cafTblId;
	private String speedocId;
	private String mobileNumber;
	private String cafNumber;
	private String connectionType;
	private String distributorName;
	private String dtrCode;
	private Long cpId;
	private Timestamp activationDate;
	private Long importId;
	private Timestamp createdDate;
	private Long createdBy;
	private String caoName;
	private String caoCode;
	private String posCode;
	private String posName;
	private String cafTrackingStatus;
	
	public String getCafTrackingStatus() {
		return cafTrackingStatus;
	}
	public void setCafTrackingStatus(String cafTrackingStatus) {
		this.cafTrackingStatus = cafTrackingStatus;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public String getConnectionType() {
		return connectionType;
	}
	public void setConnectionType(String connectionType) {
		this.connectionType = connectionType;
	}
	public String getDtrCode() {
		return dtrCode;
	}
	public void setDtrCode(String dtrCode) {
		this.dtrCode = dtrCode;
	}
	public Long getCpId() {
		return cpId;
	}
	public void setCpId(Long cpId) {
		this.cpId = cpId;
	}
	public Timestamp getActivationDate() {
		return activationDate;
	}
	public void setActivationDate(Timestamp activationDate) {
		this.activationDate = activationDate;
	}
	public Long getImportId() {
		return importId;
	}
	public void setImportId(Long importId) {
		this.importId = importId;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public String getCaoName() {
		return caoName;
	}
	public void setCaoName(String caoName) {
		this.caoName = caoName;
	}
	public String getCaoCode() {
		return caoCode;
	}
	public void setCaoCode(String caoCode) {
		this.caoCode = caoCode;
	}
	
	public String getPosCode() {
		return posCode;
	}
	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}
	public String getPosName() {
		return posName;
	}
	public void setPosName(String posName) {
		this.posName = posName;
	}
	
	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}
	public String getDistributorName() {
		return distributorName;
	}
	@Override
	public String toString() {
		return "AvamVo [id=" + id + ", cafTblId=" + cafTblId + ", speedocId="
				+ speedocId + ", mobileNumber=" + mobileNumber + ", cafNumber="
				+ cafNumber + ", connectionType=" + connectionType
				+ ", distributorName=" + distributorName + ", dtrCode="
				+ dtrCode + ", cpId=" + cpId + ", activationDate="
				+ activationDate + ", importId=" + importId + ", createdDate="
				+ createdDate + ", createdBy=" + createdBy + ", caoName="
				+ caoName + ", caoCode=" + caoCode + ", posCode=" + posCode
				+ ", posName=" + posName + ", cafTrackingStatus="
				+ cafTrackingStatus + "]";
	}	
	
}