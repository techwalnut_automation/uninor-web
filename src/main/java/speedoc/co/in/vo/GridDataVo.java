package speedoc.co.in.vo;

import java.io.Serializable;

public class GridDataVo<T> implements Serializable{

	private static final long serialVersionUID = 2281361341378956150L;
	private Integer maxPageNumber;
	private Long maxRowCount;
	private T result;
	private Boolean hasMorePages;
	private String records;
	
	public Integer getMaxPageNumber() {
		return maxPageNumber;
	}
	public void setMaxPageNumber(Integer maxPageNumber) {
		this.maxPageNumber = maxPageNumber;
	}
	
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	public Boolean getHasMorePages() {
		return hasMorePages;
	}
	public void setHasMorePages(Boolean hasMorePages) {
		this.hasMorePages = hasMorePages;
	}
	public Long getMaxRowCount() {
		return maxRowCount;
	}
	public void setMaxRowCount(Long maxRowCount) {
		this.maxRowCount = maxRowCount;
	}
	public String getRecords() {
		return records;
	}
	public void setRecords(String records) {
		this.records = records;
	}
	
	@Override
	public String toString() {
		return "GridDataVo [maxPageNumber=" + maxPageNumber + ", maxRowCount="
				+ maxRowCount + ", result=" + result + ", hasMorePages="
				+ hasMorePages + ", records=" + records + "]";
	}
	
	
	
}
