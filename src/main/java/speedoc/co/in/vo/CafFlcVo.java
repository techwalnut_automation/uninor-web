package speedoc.co.in.vo;

import java.io.Serializable;

public class CafFlcVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4683715384090555641L;
	private Long cafId ;
	private String speedocId ;
	private String userId ;
	private Long cafStatusId ;
	private Long flcPoolId ;
	private String currentStatus ;
	private String fileLocation ;
	private int  pages; 
	private String remark ;
	private String images ;
	private String flcStatus;
	
	public Long getCafId() {
		return cafId;
	}
	public void setCafId(Long cafId) {
		this.cafId = cafId;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Long getCafStatusId() {
		return cafStatusId;
	}
	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}
	public Long getFlcPoolId() {
		return flcPoolId;
	}
	public void setFlcPoolId(Long flcPoolId) {
		this.flcPoolId = flcPoolId;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getFileLocation() {
		return fileLocation;
	}
	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}
	public int getPages() {
		return pages;
	}
	public void setPages(int pages) {
		this.pages = pages;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	public String getImages() {
		return images;
	}
	public void setImages(String images) {
		this.images = images;
	}
	public String getFlcStatus() {
		return flcStatus;
	}
	public void setFlcStatus(String flcStatus) {
		this.flcStatus = flcStatus;
	}
	@Override
	public String toString() {
		return "CafFlcVo [cafId=" + cafId + ", speedocId=" + speedocId
				+ ", userId=" + userId + ", cafStatusId=" + cafStatusId
				+ ", flcPoolId=" + flcPoolId + ", currentStatus="
				+ currentStatus + ", fileLocation=" + fileLocation + ", pages="
				+ pages + ", remark=" + remark + ", images=" + images
				+ ", flcStatus=" + flcStatus + "]";
	}
}
