package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class TempPoolVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5908388104053076493L;
	
	private Long id;
	private Long cafTblId;
	private Long cafStatusId;
	private Timestamp createdDate;
	private Long poolId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public Long getCafStatusId() {
		return cafStatusId;
	}
	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Long getPoolId() {
		return poolId;
	}
	public void setPoolId(Long poolId) {
		this.poolId = poolId;
	}
	
	@Override
	public String toString() {
		return "TempPoolVo [id=" + id + ", cafTblId=" + cafTblId
				+ ", cafStatusId=" + cafStatusId + ", createdDate="
				+ createdDate + ", poolId=" + poolId + "]";
	}
}
