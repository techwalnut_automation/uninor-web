
package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class UserVo implements Serializable{
	
	private static final long serialVersionUID = -2153492009281401784L;
	private Long id;
	private String firstName;
	private String middleName ;
	private String lastName;
	private String pwd;
	private String mobileNumber ;
	private String address ;
	private Boolean enabled;
	private Boolean accountExpired ;
	private Boolean accountLocked ;
	private Timestamp passwordChangedDate ;
	private String username ;
	private String city;
	private String state;
	private String status;
	private String description;
	private Timestamp createdDate ;
	private Timestamp modifiedDate ;
	private Integer active; 
	private Long createdBy ;	
	private List<RoleVo>  roleList;
	private String emailAddress;
	private String userType;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getPasswordChangedDate() {
		return passwordChangedDate;
	}
	public void setPasswordChangedDate(Timestamp passwordChangedDate) {
		this.passwordChangedDate = passwordChangedDate;
	}
	public Boolean getAccountLocked() {
		return accountLocked;
	}
	public void setAccountLocked(Boolean accountLocked) {
		this.accountLocked = accountLocked;
	}
	public Boolean getAccountExpired() {
		return accountExpired;
	}
	public void setAccountExpired(Boolean accountExpired) {
		this.accountExpired = accountExpired;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public List<RoleVo> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<RoleVo> roleList) {
		this.roleList = roleList;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Integer getActive() {
		return active;
	}
	public void setActive(Integer active) {
		this.active = active;
	}	
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	
	@Override
	public String toString() {
		return "UserVo [id=" + id + ", firstName=" + firstName
				+ ", middleName=" + middleName + ", lastName=" + lastName
				+ ", pwd=" + pwd + ", mobileNumber=" + mobileNumber
				+ ", address=" + address + ", enabled=" + enabled
				+ ", accountExpired=" + accountExpired + ", accountLocked="
				+ accountLocked + ", passwordChangedDate="
				+ passwordChangedDate + ", username=" + username + ", city="
				+ city + ", state=" + state + ", status=" + status
				+ ", description=" + description + ", createdDate="
				+ createdDate + ", modifiedDate=" + modifiedDate + ", active="
				+ active + ", createdBy=" + createdBy + ", roleList="
				+ roleList + ", emailAddress=" + emailAddress + ", userType="
				+ userType + "]";
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserType() {
		return userType;
	}

}
