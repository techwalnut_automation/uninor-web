package speedoc.co.in.vo;

import java.util.List;

public class ScrutinyRecordJSONResponseVo {
	
	public String username;
	public String summaryId;
	public List<ScrutinyCaptureVo> scrutinyRecords;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSummaryId() {
		return summaryId;
	}
	public void setSummaryId(String summaryId) {
		this.summaryId = summaryId;
	}
	public List<ScrutinyCaptureVo> getScrutinyRecords() {
		return scrutinyRecords;
	}
	public void setScrutinyRecords(List<ScrutinyCaptureVo> scrutinyRecords) {
		this.scrutinyRecords = scrutinyRecords;
	}
	
	@Override
	public String toString() {
		return "ScrutinyRecordJSONResponseVO [username=" + username
				+ ", summaryId=" + summaryId + ", scrutinyRecords="
				+ scrutinyRecords + "]";
	}
	
	
	
}
