package speedoc.co.in.vo;

import java.sql.Timestamp;

public class IndexingVo {
	private Long id;
	private Long cafTblId;
	private String cafNumber;
	private String mobileNumber;
	private String cafTrackingStatus;
	private String imsiNumber;
	private String upcCode;
	private String previousServiceProviderName;
	private String statusOfSubscriber;
	private String existingNumber;
	private String firstName;
	private String middleName;
	private String lastName;
	private String fatherFirstName;
	private String fatherMiddleName;
	private String fatherLastName;
	private String gender;
	private String dobText;
	private Timestamp dob;
	private String nationality;
	private String panGirNumber;
	private String uidNumber;
	private String localAddress;
	private String localAddressLandmark;
	private String localCity;
	private String localDistrict;
	private String localState;
	private String localPincode;
	private String permanentAddress;
	private String permanentAddressLandmark;
	private String permanentCity;
	private String permanentDistrict;
	private String permanentState;
	private String permanentPincode;
	private String existingConnection;
	private String tarrifPlanApplied;
	private String emailAddress;
	private String valueAddedServiceApplied;
	private String alternateMobileNumber;
	private String subscriberProfession;
	private String poi;
	private String poiref;
	private String poiIssueDateText;
	private Timestamp poiIssueDate;
	private String poiIssuePlace;
	private String poiIssuingAuthority;
	private String poa;
	private String poaref;
	private String poaIssueDateText;
	private Timestamp poaIssueDate;
	private String poaIssuePlace;
	private String poaIssuingAuthority;
	private String passportNumber;
	private String visaValidTillText;
	private Timestamp visaValidTill;
	private String typeOfVisa;
	private String visaNumber;
	private String localReferenceName;
	private String localReferenceAddress;
	private String localReferenceCity;
	private String localReferencePincode;
	private String localReferenceContactNumber;
	private String callingPartyNumber;
	private String friendsAndFamily;
	private String gprs;
	private String wap;
	private String mms;
	private String callerBackRingTone;
	private Integer status;
	private Integer searchCount;
	private Timestamp createdDate;
	private Integer electroChittiStatus;

	public Integer getSearchCount() {
		return searchCount;
	}

	public void setSearchCount(Integer searchCount) {
		this.searchCount = searchCount;
	}

	public String getDobText() {
		return dobText;
	}

	public void setDobText(String dobText) {
		this.dobText = dobText;
	}

	public Timestamp getDob() {
		return dob;
	}

	public void setDob(Timestamp dob) {
		this.dob = dob;
	}

	public String getPoiIssueDateText() {
		return poiIssueDateText;
	}

	public void setPoiIssueDateText(String poiIssueDateText) {
		this.poiIssueDateText = poiIssueDateText;
	}

	public Timestamp getPoiIssueDate() {
		return poiIssueDate;
	}

	public void setPoiIssueDate(Timestamp poiIssueDate) {
		this.poiIssueDate = poiIssueDate;
	}

	public String getPoaIssueDateText() {
		return poaIssueDateText;
	}

	public void setPoaIssueDateText(String poaIssueDateText) {
		this.poaIssueDateText = poaIssueDateText;
	}

	public Timestamp getPoaIssueDate() {
		return poaIssueDate;
	}

	public void setPoaIssueDate(Timestamp poaIssueDate) {
		this.poaIssueDate = poaIssueDate;
	}

	public String getVisaValidTillText() {
		return visaValidTillText;
	}

	public void setVisaValidTillText(String visaValidTillText) {
		this.visaValidTillText = visaValidTillText;
	}

	public Timestamp getVisaValidTill() {
		return visaValidTill;
	}

	public void setVisaValidTill(Timestamp visaValidTill) {
		this.visaValidTill = visaValidTill;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getCafNumber() {
		return cafNumber;
	}



	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCafTrackingStatus() {
		return cafTrackingStatus;
	}

	public void setCafTrackingStatus(String cafTrackingStatus) {
		this.cafTrackingStatus = cafTrackingStatus;
	}

	public String getImsiNumber() {
		return imsiNumber;
	}

	public void setImsiNumber(String imsiNumber) {
		this.imsiNumber = imsiNumber;
	}

	public String getUpcCode() {
		return upcCode;
	}

	public void setUpcCode(String upcCode) {
		this.upcCode = upcCode;
	}

	public String getPreviousServiceProviderName() {
		return previousServiceProviderName;
	}

	public void setPreviousServiceProviderName(String previousServiceProviderName) {
		this.previousServiceProviderName = previousServiceProviderName;
	}

	public String getStatusOfSubscriber() {
		return statusOfSubscriber;
	}

	public void setStatusOfSubscriber(String statusOfSubscriber) {
		this.statusOfSubscriber = statusOfSubscriber;
	}

	public String getExistingNumber() {
		return existingNumber;
	}

	public void setExistingNumber(String existingNumber) {
		this.existingNumber = existingNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFatherFirstName() {
		return fatherFirstName;
	}

	public void setFatherFirstName(String fatherFirstName) {
		this.fatherFirstName = fatherFirstName;
	}

	public String getFatherMiddleName() {
		return fatherMiddleName;
	}

	public void setFatherMiddleName(String fatherMiddleName) {
		this.fatherMiddleName = fatherMiddleName;
	}

	public String getFatherLastName() {
		return fatherLastName;
	}

	public void setFatherLastName(String fatherLastName) {
		this.fatherLastName = fatherLastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getPanGirNumber() {
		return panGirNumber;
	}

	public void setPanGirNumber(String panGirNumber) {
		this.panGirNumber = panGirNumber;
	}

	public String getUidNumber() {
		return uidNumber;
	}

	public void setUidNumber(String uidNumber) {
		this.uidNumber = uidNumber;
	}

	public String getLocalAddress() {
		return localAddress;
	}

	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	public String getLocalAddressLandmark() {
		return localAddressLandmark;
	}

	public void setLocalAddressLandmark(String localAddressLandmark) {
		this.localAddressLandmark = localAddressLandmark;
	}

	public String getLocalCity() {
		return localCity;
	}

	public void setLocalCity(String localCity) {
		this.localCity = localCity;
	}

	public String getLocalDistrict() {
		return localDistrict;
	}

	public void setLocalDistrict(String localDistrict) {
		this.localDistrict = localDistrict;
	}

	public String getLocalState() {
		return localState;
	}

	public void setLocalState(String localState) {
		this.localState = localState;
	}

	public String getLocalPincode() {
		return localPincode;
	}

	public void setLocalPincode(String localPincode) {
		this.localPincode = localPincode;
	}

	public String getPermanentAddress() {
		return permanentAddress;
	}

	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	public String getPermanentAddressLandmark() {
		return permanentAddressLandmark;
	}

	public void setPermanentAddressLandmark(String permanentAddressLandmark) {
		this.permanentAddressLandmark = permanentAddressLandmark;
	}

	public String getPermanentCity() {
		return permanentCity;
	}

	public void setPermanentCity(String permanentCity) {
		this.permanentCity = permanentCity;
	}

	public String getPermanentDistrict() {
		return permanentDistrict;
	}

	public void setPermanentDistrict(String permanentDistrict) {
		this.permanentDistrict = permanentDistrict;
	}

	public String getPermanentState() {
		return permanentState;
	}

	public void setPermanentState(String permanentState) {
		this.permanentState = permanentState;
	}

	public String getPermanentPincode() {
		return permanentPincode;
	}

	public void setPermanentPincode(String permanentPincode) {
		this.permanentPincode = permanentPincode;
	}

	public String getExistingConnection() {
		return existingConnection;
	}

	public void setExistingConnection(String existingConnection) {
		this.existingConnection = existingConnection;
	}

	public String getTarrifPlanApplied() {
		return tarrifPlanApplied;
	}

	public void setTarrifPlanApplied(String tarrifPlanApplied) {
		this.tarrifPlanApplied = tarrifPlanApplied;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getValueAddedServiceApplied() {
		return valueAddedServiceApplied;
	}

	public void setValueAddedServiceApplied(String valueAddedServiceApplied) {
		this.valueAddedServiceApplied = valueAddedServiceApplied;
	}

	public String getAlternateMobileNumber() {
		return alternateMobileNumber;
	}

	public void setAlternateMobileNumber(String alternateMobileNumber) {
		this.alternateMobileNumber = alternateMobileNumber;
	}

	public String getSubscriberProfession() {
		return subscriberProfession;
	}

	public void setSubscriberProfession(String subscriberProfession) {
		this.subscriberProfession = subscriberProfession;
	}

	public String getPoi() {
		return poi;
	}

	public void setPoi(String poi) {
		this.poi = poi;
	}

	public String getPoiref() {
		return poiref;
	}

	public void setPoiref(String poiref) {
		this.poiref = poiref;
	}

	public String getPoiIssuePlace() {
		return poiIssuePlace;
	}

	public void setPoiIssuePlace(String poiIssuePlace) {
		this.poiIssuePlace = poiIssuePlace;
	}

	public String getPoiIssuingAuthority() {
		return poiIssuingAuthority;
	}

	public void setPoiIssuingAuthority(String poiIssuingAuthority) {
		this.poiIssuingAuthority = poiIssuingAuthority;
	}

	public String getPoa() {
		return poa;
	}

	public void setPoa(String poa) {
		this.poa = poa;
	}

	public String getPoaref() {
		return poaref;
	}

	public void setPoaref(String poaref) {
		this.poaref = poaref;
	}

	public String getPoaIssuePlace() {
		return poaIssuePlace;
	}

	public void setPoaIssuePlace(String poaIssuePlace) {
		this.poaIssuePlace = poaIssuePlace;
	}

	public String getPoaIssuingAuthority() {
		return poaIssuingAuthority;
	}

	public void setPoaIssuingAuthority(String poaIssuingAuthority) {
		this.poaIssuingAuthority = poaIssuingAuthority;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) {
		this.passportNumber = passportNumber;
	}

	public String getTypeOfVisa() {
		return typeOfVisa;
	}

	public void setTypeOfVisa(String typeOfVisa) {
		this.typeOfVisa = typeOfVisa;
	}

	public String getVisaNumber() {
		return visaNumber;
	}

	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
	}

	public String getLocalReferenceName() {
		return localReferenceName;
	}

	public void setLocalReferenceName(String localReferenceName) {
		this.localReferenceName = localReferenceName;
	}

	public String getLocalReferenceAddress() {
		return localReferenceAddress;
	}

	public void setLocalReferenceAddress(String localReferenceAddress) {
		this.localReferenceAddress = localReferenceAddress;
	}

	public String getLocalReferenceCity() {
		return localReferenceCity;
	}

	public void setLocalReferenceCity(String localReferenceCity) {
		this.localReferenceCity = localReferenceCity;
	}

	public String getLocalReferencePincode() {
		return localReferencePincode;
	}

	public void setLocalReferencePincode(String localReferencePincode) {
		this.localReferencePincode = localReferencePincode;
	}

	public String getLocalReferenceContactNumber() {
		return localReferenceContactNumber;
	}

	public void setLocalReferenceContactNumber(
			String localReferenceContactNumber) {
		this.localReferenceContactNumber = localReferenceContactNumber;
	}

	public String getCallingPartyNumber() {
		return callingPartyNumber;
	}

	public void setCallingPartyNumber(String callingPartyNumber) {
		this.callingPartyNumber = callingPartyNumber;
	}

	public String getFriendsAndFamily() {
		return friendsAndFamily;
	}

	public void setFriendsAndFamily(String friendsAndFamily) {
		this.friendsAndFamily = friendsAndFamily;
	}

	public String getGprs() {
		return gprs;
	}

	public void setGprs(String gprs) {
		this.gprs = gprs;
	}

	public String getWap() {
		return wap;
	}

	public void setWap(String wap) {
		this.wap = wap;
	}

	public String getMms() {
		return mms;
	}

	public void setMms(String mms) {
		this.mms = mms;
	}

	public String getCallerBackRingTone() {
		return callerBackRingTone;
	}

	public void setCallerBackRingTone(String callerBackRingTone) {
		this.callerBackRingTone = callerBackRingTone;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}

	public Long getCafTblId() {
		return cafTblId;
	}
	
	public Integer getElectroChittiStatus() {
		return electroChittiStatus;
	}

	public void setElectroChittiStatus(Integer electroChittiStatus) {
		this.electroChittiStatus = electroChittiStatus;
	}

	@Override
	public String toString() {
		return "IndexingVo [id=" + id + ", cafTblId=" + cafTblId
				+ ", cafNumber=" + cafNumber + ", mobileNumber=" + mobileNumber
				+ ", cafTrackingStatus=" + cafTrackingStatus + ", imsiNumber="
				+ imsiNumber + ", upcCode=" + upcCode
				+ ", previousServiceProviderName="
				+ previousServiceProviderName + ", statusOfSubscriber="
				+ statusOfSubscriber + ", existingNumber=" + existingNumber
				+ ", firstName=" + firstName + ", middleName=" + middleName
				+ ", lastName=" + lastName + ", fatherFirstName="
				+ fatherFirstName + ", fatherMiddleName=" + fatherMiddleName
				+ ", fatherLastName=" + fatherLastName + ", gender=" + gender
				+ ", dobText=" + dobText + ", dob=" + dob + ", nationality="
				+ nationality + ", panGirNumber=" + panGirNumber
				+ ", uidNumber=" + uidNumber + ", localAddress=" + localAddress
				+ ", localAddressLandmark=" + localAddressLandmark
				+ ", localCity=" + localCity + ", localDistrict="
				+ localDistrict + ", localState=" + localState
				+ ", localPincode=" + localPincode + ", permanentAddress="
				+ permanentAddress + ", permanentAddressLandmark="
				+ permanentAddressLandmark + ", permanentCity=" + permanentCity
				+ ", permanentDistrict=" + permanentDistrict
				+ ", permanentState=" + permanentState + ", permanentPincode="
				+ permanentPincode + ", existingConnection="
				+ existingConnection + ", tarrifPlanApplied="
				+ tarrifPlanApplied + ", emailAddress=" + emailAddress
				+ ", valueAddedServiceApplied=" + valueAddedServiceApplied
				+ ", alternateMobileNumber=" + alternateMobileNumber
				+ ", subscriberProfession=" + subscriberProfession + ", poi="
				+ poi + ", poiref=" + poiref + ", poiIssueDateText="
				+ poiIssueDateText + ", poiIssueDate=" + poiIssueDate
				+ ", poiIssuePlace=" + poiIssuePlace + ", poiIssuingAuthority="
				+ poiIssuingAuthority + ", poa=" + poa + ", poaref=" + poaref
				+ ", poaIssueDateText=" + poaIssueDateText + ", poaIssueDate="
				+ poaIssueDate + ", poaIssuePlace=" + poaIssuePlace
				+ ", poaIssuingAuthority=" + poaIssuingAuthority
				+ ", passportNumber=" + passportNumber + ", visaValidTillText="
				+ visaValidTillText + ", visaValidTill=" + visaValidTill
				+ ", typeOfVisa=" + typeOfVisa + ", visaNumber=" + visaNumber
				+ ", localReferenceName=" + localReferenceName
				+ ", localReferenceAddress=" + localReferenceAddress
				+ ", localReferenceCity=" + localReferenceCity
				+ ", localReferencePincode=" + localReferencePincode
				+ ", localReferenceContactNumber="
				+ localReferenceContactNumber + ", callingPartyNumber="
				+ callingPartyNumber + ", friendsAndFamily=" + friendsAndFamily
				+ ", gprs=" + gprs + ", wap=" + wap + ", mms=" + mms
				+ ", callerBackRingTone=" + callerBackRingTone + ", status="
				+ status + ", searchCount=" + searchCount + ", createdDate="
				+ createdDate + ", electroChittiStatus=" + electroChittiStatus
				+ "]";
	}
	
	
}