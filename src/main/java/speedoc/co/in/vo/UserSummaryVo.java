package speedoc.co.in.vo;

import java.sql.Timestamp;



public class UserSummaryVo {

	private Integer userId;
	private String userName;
	private Integer summaryId;
	private Integer indexUserId;
	private Integer scrutinyCount;
	private Timestamp createdDate;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public Integer getSummaryId() {
		return summaryId;
	}
	public void setSummaryId(Integer summaryId) {
		this.summaryId = summaryId;
	}
	public Integer getIndexUserId() {
		return indexUserId;
	}
	public void setIndexUserId(Integer indexUserId) {
		this.indexUserId = indexUserId;
	}
	public Integer getScrutinyCount() {
		return scrutinyCount;
	}
	public void setScrutinyCount(Integer scrutinyCount) {
		this.scrutinyCount = scrutinyCount;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public String toString() {
		return "UserSummaryVO [userId=" + userId + ", userName=" + userName
				+ ", summaryId=" + summaryId + ", indexUserId=" + indexUserId
				+ ", scrutinyCount=" + scrutinyCount + ", createdDate="
				+ createdDate + "]";
	}
	
	
	
}
