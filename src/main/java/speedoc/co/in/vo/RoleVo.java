package speedoc.co.in.vo;


import java.io.Serializable;

//mapping of role_tbl
public class RoleVo implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7704142184571805812L;
	private Long id;
	private String roleType;
	private String roleName;
	
	
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getRoleType() {
		return roleType;
	}
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}
	
	 
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "RoleVo [id=" + id + ", roleType=" + roleType + ", roleName="
				+ roleName + "]";
	}
	
	
	
	
	
	
}
