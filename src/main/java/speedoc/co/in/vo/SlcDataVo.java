package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class SlcDataVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6662514323135134100L;
	private Long id;
	private Long slcImportId;
	private String speedocId;
	private String mobileNumber;
	private String cafNumber;
	private Timestamp createdDate;
	private Long createdBy;
	private Integer status;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getSlcImportId() {
		return slcImportId;
	}
	public void setSlcImportId(Long slcImportId) {
		this.slcImportId = slcImportId;
	}
	public String getSpeedocId() {
		return speedocId;
	}
	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}
	public String getMobileNumber() {
		return mobileNumber;
	}
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	public String getCafNumber() {
		return cafNumber;
	}
	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	@Override
	public String toString() {
		return "SlcDataVo [id=" + id + ", slcImportId=" + slcImportId
				+ ", speedocId=" + speedocId + ", mobileNumber=" + mobileNumber
				+ ", cafNumber=" + cafNumber + ", createdDate=" + createdDate
				+ ", createdBy=" + createdBy + ", status=" + status + "]";
	}	
}
