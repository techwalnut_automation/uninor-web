package speedoc.co.in.vo;

public class ScanClientInfoVO {

    private String macAddress;
    private UserVo userVO;
    private String regKey;
    private String postUrl;

        
    

	public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    public UserVo getUserVO() {
        return userVO;
    }

    public void setUserVO(UserVo userVO) {
        this.userVO = userVO;
    }



    public String getRegKey() {
        return regKey;
    }

    public void setRegKey(String regKey) {
        this.regKey = regKey;
    }

    public String getPostUrl() {
        return postUrl;
    }

    public void setPostUrl(String postUrl) {
        this.postUrl = postUrl;
    }

	@Override
	public String toString() {
		return "ScanClientInfoVO [macAddress=" + macAddress + ", userVO="
				+ userVO + ", regKey=" + regKey + ", postUrl=" + postUrl + "]";
	}

	
}
