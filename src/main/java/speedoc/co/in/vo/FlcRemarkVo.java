package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;

public class FlcRemarkVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1381158793861292687L;

	private Long remarkId;
	private Long id;
	private Long cafTblId;
	private Long userId;
	private Long statusId ;
	private Timestamp createdDate;
	public Long getRemarkId() {
		return remarkId;
	}
	public void setRemarkId(Long remarkId) {
		this.remarkId = remarkId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getCafTblId() {
		return cafTblId;
	}
	public void setCafTblId(Long cafTblId) {
		this.cafTblId = cafTblId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Long getStatusId() {
		return statusId;
	}
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}
	@Override
	public String toString() {
		return "FlcRemarkVo [remarkId=" + remarkId + ", id=" + id
				+ ", cafTblId=" + cafTblId + ", userId=" + userId
				+ ", statusId=" + statusId + ", createdDate=" + createdDate
				+ "]";
	}
	
}
