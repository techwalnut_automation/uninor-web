package speedoc.co.in.vo;

public class FlcAcceptVo {
	private String updatedResult;

	public String getUpdatedResult() {
		return updatedResult;
	}

	public void setUpdatedResult(String updatedResult) {
		this.updatedResult = updatedResult;
	}

	@Override
	public String toString() {
		return "FlcAcceptVo [updatedResult=" + updatedResult + "]";
	}
}
