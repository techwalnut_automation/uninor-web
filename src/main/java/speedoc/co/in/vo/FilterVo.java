package speedoc.co.in.vo;

import java.io.Serializable;

public class FilterVo<T> implements Serializable{

	private static final long serialVersionUID = 3189059076428757945L;
	private Integer pageNumber;
	private Integer rowsPerPage;
	private String allRecords;
	private T result;
	
	public T getResult() {
		return result;
	}
	public void setResult(T result) {
		this.result = result;
	}
	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Integer getRowsPerPage() {
		return rowsPerPage;
	}
	public void setRowsPerPage(Integer rowsPerPage) {
		this.rowsPerPage = rowsPerPage;
	}
	public String getAllRecords() {
		return allRecords;
	}
	public void setAllRecords(String allRecords) {
		this.allRecords = allRecords;
	}
	@Override
	public String toString() {
		return "FilterVo [pageNumber=" + pageNumber + ", rowsPerPage="
				+ rowsPerPage + ", allRecords=" + allRecords + ", result="
				+ result + "]";
	}
	
	
}
