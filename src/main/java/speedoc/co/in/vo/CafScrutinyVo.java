package speedoc.co.in.vo;

import java.io.Serializable;

public class CafScrutinyVo extends CafVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long cafStatusId ;
	private String sessionId;
	private String images1 ;
	
	public Long getCafStatusId() {
		return cafStatusId;
	}

	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}

	public String getImages1() {
		return images1;
	}

	public void setImages1(String images1) {
		this.images1 = images1;
	}
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String toString() {
		return "CafScrutinyVo [cafStatusId=" + cafStatusId + ", sessionId="
				+ sessionId + ", images1=" + images1 + "]";
	}

	
}
