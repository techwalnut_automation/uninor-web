package speedoc.co.in.vo;

public class VendorUserVo {
	private Long id;
	private Long vendorId;
	private Long userId;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getVendorId() {
		return vendorId;
	}
	public void setVendorId(Long vendorId) {
		this.vendorId = vendorId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	@Override
	public String toString() {
		return "VendorUserVO [id=" + id + ", vendorId=" + vendorId
				+ ", userId=" + userId + "]";
	}
	
	
}
