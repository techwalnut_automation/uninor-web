package speedoc.co.in.vo;

public class ClientInfoVO {
	
	private String scanClientInfoVO;

	public void setScanClientInfoVO(String scanClientInfoVO) {
		this.scanClientInfoVO = scanClientInfoVO;
	}

	public String getScanClientInfoVO() {
		return scanClientInfoVO;
	}

	@Override
	public String toString() {
		return "ClientInfoVO [scanClientInfoVO=" + scanClientInfoVO + "]";
	}

}
