package speedoc.co.in.vo;

import java.io.Serializable;

public class CaoVo implements Serializable{

	private static final long serialVersionUID = -3720515204430599701L;
	private Long id;
	private String caoName;
	private String caoCode;
	private String imageName;
	private int status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCaoName() {
		return caoName;
	}
	public void setCaoName(String caoName) {
		this.caoName = caoName;
	}
	public String getCaoCode() {
		return caoCode;
	}
	public void setCaoCode(String caoCode) {
		this.caoCode = caoCode;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "CaoVo [id=" + id + ", caoName=" + caoName + ", caoCode="
				+ caoCode + ", imageName=" + imageName + ", status=" + status
				+ "]";
	}
	
	
}
