package speedoc.co.in.vo;

import java.io.Serializable;
import java.math.BigDecimal;

public class StatusViewerVo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6921348135427603597L;
	private BigDecimal id;
	private BigDecimal count;
	private String username;
	private BigDecimal deCompleteCount;
	private BigDecimal flcAcceptCount;
	private BigDecimal flcRejectCount;
	private BigDecimal scrutinyCompleteCount;
	private BigDecimal scrutinyRejectCount;
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	public BigDecimal getCount() {
		return count;
	}
	public void setCount(BigDecimal count) {
		this.count = count;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public BigDecimal getDeCompleteCount() {
		return deCompleteCount;
	}
	public void setDeCompleteCount(BigDecimal deCompleteCount) {
		this.deCompleteCount = deCompleteCount;
	}
	public BigDecimal getFlcAcceptCount() {
		return flcAcceptCount;
	}
	public void setFlcAcceptCount(BigDecimal flcAcceptCount) {
		this.flcAcceptCount = flcAcceptCount;
	}
	public BigDecimal getFlcRejectCount() {
		return flcRejectCount;
	}
	public void setFlcRejectCount(BigDecimal flcRejectCount) {
		this.flcRejectCount = flcRejectCount;
	}
	public BigDecimal getScrutinyCompleteCount() {
		return scrutinyCompleteCount;
	}
	public void setScrutinyCompleteCount(BigDecimal scrutinyCompleteCount) {
		this.scrutinyCompleteCount = scrutinyCompleteCount;
	}
	
	
	public BigDecimal getScrutinyRejectCount() {
		return scrutinyRejectCount;
	}
	public void setScrutinyRejectCount(BigDecimal scrutinyRejectCount) {
		this.scrutinyRejectCount = scrutinyRejectCount;
	}
	@Override
	public String toString() {
		return "StatusViewerVo [id=" + id + ", count=" + count + ", username="
				+ username + ", deCompleteCount=" + deCompleteCount
				+ ", flcAcceptCount=" + flcAcceptCount + ", flcRejectCount="
				+ flcRejectCount + ", scrutinyCompleteCount="
				+ scrutinyCompleteCount + ", scrutinyRejectCount="
				+ scrutinyRejectCount + "]";
	}
		
	


		
}
