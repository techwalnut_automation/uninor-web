package speedoc.co.in.vo;

import java.io.Serializable;

public class PosCposVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4610461900158584641L;
	private Long id;
	private String mobileNumber;
	private String cafNumber;
	private String speedocId;
	private String rtrCode;
	private String rtrName;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCafNumber() {
		return cafNumber;
	}

	public void setCafNumber(String cafNumber) {
		this.cafNumber = cafNumber;
	}

	public String getSpeedocId() {
		return speedocId;
	}

	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}

	public String getRtrCode() {
		return rtrCode;
	}

	public void setRtrCode(String rtrCode) {
		this.rtrCode = rtrCode;
	}

	public String getRtrName() {
		return rtrName;
	}

	public void setRtrName(String rtrName) {
		this.rtrName = rtrName;
	}

	@Override
	public String toString() {
		return "PosCposVo [id=" + id + ", mobileNumber=" + mobileNumber
				+ ", cafNumber=" + cafNumber + ", speedocId=" + speedocId
				+ ", rtrCode=" + rtrCode + ", rtrName=" + rtrName + "]";
	}

	
}
