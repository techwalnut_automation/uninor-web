package speedoc.co.in.vo;

import java.sql.Timestamp;

public class CafAuditVo extends CafVo{
	
	private static final long serialVersionUID = 1L; 
	private Long cafStatusId ;
	private String images1 ;
	private Long cafId ;
	private String speedocId ;
	private String userId ;
	private Long auditPoolId ;
	private String currentStatus ;
	private String fileLocation ;
	private int pages; 
	private String remark ;
	private String auditStatus;
	private String username;
	private Timestamp createdDate;
	private String rejectReason;
	
	public Long getCafStatusId() {
		return cafStatusId;
	}

	public void setCafStatusId(Long cafStatusId) {
		this.cafStatusId = cafStatusId;
	}

	public String getImages1() {
		return images1;
	}

	public void setImages1(String images1) {
		this.images1 = images1;
	}

	public Long getCafId() {
		return cafId;
	}

	public void setCafId(Long cafId) {
		this.cafId = cafId;
	}

	public String getSpeedocId() {
		return speedocId;
	}

	public void setSpeedocId(String speedocId) {
		this.speedocId = speedocId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getAuditPoolId() {
		return auditPoolId;
	}

	public void setAuditPoolId(Long auditPoolId) {
		this.auditPoolId = auditPoolId;
	}

	public String getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

//	public int getPages() {
//		return pages;
//	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAuditStatus() {
		return auditStatus;
	}

	public void setAuditStatus(String auditStatus) {
		this.auditStatus = auditStatus;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getRejectReason() {
		return rejectReason;
	}

	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}

	@Override
	public String toString() {
		return "CafAuditVo [cafStatusId=" + cafStatusId + ", images1="
				+ images1 + ", cafId=" + cafId + ", speedocId=" + speedocId
				+ ", userId=" + userId + ", auditPoolId=" + auditPoolId
				+ ", currentStatus=" + currentStatus + ", fileLocation="
				+ fileLocation + ", pages=" + pages + ", remark=" + remark
				+ ", auditStatus=" + auditStatus + ", username=" + username
				+ ", createdDate=" + createdDate + ", rejectReason="
				+ rejectReason + "]";
	}
}
