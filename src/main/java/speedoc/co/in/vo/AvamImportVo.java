package speedoc.co.in.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class AvamImportVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2127688099942801625L;
	private Long id;
	private String fileName;
	private Integer totalRecord;
	private Integer recordInserted;
	private Integer duplicateRecord;
	private String status;
	private Timestamp createdDate;
	private Long createdBy;
	private String username;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Integer getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Integer totalRecord) {
		this.totalRecord = totalRecord;
	}
	public Integer getRecordInserted() {
		return recordInserted;
	}
	public void setRecordInserted(Integer recordInserted) {
		this.recordInserted = recordInserted;
	}
	public Integer getDuplicateRecord() {
		return duplicateRecord;
	}
	public void setDuplicateRecord(Integer duplicateRecord) {
		this.duplicateRecord = duplicateRecord;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Long getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getCreatedDateString(){
		if (getCreatedDate() != null) {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
			return sdf.format(getCreatedDate());
		}
		return "";
	}
	@Override
	public String toString() {
		return "AvamImportVo [id=" + id + ", fileName=" + fileName
				+ ", totalRecord=" + totalRecord + ", recordInserted="
				+ recordInserted + ", duplicateRecord=" + duplicateRecord
				+ ", status=" + status + ", createdDate=" + createdDate
				+ ", createdBy=" + createdBy + ", username=" + username + "]";
	}
 
	
}
