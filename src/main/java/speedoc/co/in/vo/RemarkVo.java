package speedoc.co.in.vo;

import java.io.Serializable;

public class RemarkVo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2456536024517321644L;
	private Long id ;
	private String rejectReason ;
	private String rejectType ;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRejectReason() {
		return rejectReason;
	}
	public void setRejectReason(String rejectReason) {
		this.rejectReason = rejectReason;
	}
	public String getRejectType() {
		return rejectType;
	}
	public void setRejectType(String rejectType) {
		this.rejectType = rejectType;
	}
	@Override
	public String toString() {
		return "RemarkVo [id=" + id + ", rejectReason=" + rejectReason
				+ ", rejectType=" + rejectType + "]";
	}
	
}
