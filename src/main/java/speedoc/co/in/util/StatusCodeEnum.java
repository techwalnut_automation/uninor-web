package speedoc.co.in.util;

public enum StatusCodeEnum {
	
	    AVAM_FAIL(2001),
	    AVAM_SUCCESS(2002),
	    UNKNOWN(2003),
	    
	 	SCAN_COMPLETE(1001),
	 	SCAN_DUPLICATE(1002),
	 
	    FLC_ACCEPT(3001),
	    FLC_REJECT(3002),
	    FLC_SKIP(3003),
	    
	    DE_COMPLETE(4001),
	    DE_REJECT(4002),
	  
	    SCRUTINY_COMPLETE(5001),
	    SCRUTINY_REJECT(5002),
	    SLA_ALREADY_DONE(5003),
	        
	    NEW(6001),
	    FOUND(6002),
	    INDEXING_COMPLETE(6003),
	    
	    AUDIT_COMPLETE(7001),
	    AUDIT_REJECT(7002);
	    
	 	private int statusCode;

	    private StatusCodeEnum(int statusCode) {
	        this.statusCode = statusCode;
	    }

	    public int getStatusCode() {
	        return statusCode;
	    }


}
