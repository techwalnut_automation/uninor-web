package speedoc.co.in.util;

import java.io.Serializable;

/**
 * A POJO representing a response property. 
 * 
 */
public class ServiceResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private T response;
	private String message;
	private boolean isSuccessful;
	public ServiceResponse() {
		super();
	}
	public ServiceResponse(T response) {
		super();
		this.response = response;
	}
	public ServiceResponse(T response,String message,
			boolean isSuccessful) {
		super();
		this.response = response;
		this.message = message;
		this.isSuccessful = isSuccessful;
	}
	public T getResponse() {
		return response;
	}
	public void setResponse(T response) {
		this.response = response;
	}
	public boolean isSuccessful() {
		return isSuccessful;
	}
	public void setSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "ServiceResponse [response=" + response + ", message=" + message
				+ ", isSuccessful=" + isSuccessful + "]";
	}
	
	
	
	
}