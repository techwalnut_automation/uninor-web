package speedoc.co.in.util;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import speedoc.co.in.vo.CafVo;

public class CAFUtils {

	private static Logger log = Logger.getLogger(CAFUtils.class.getName());

	@SuppressWarnings("deprecation")
	public static CafVo getCafVoFromPdf(String tempFilePath) {

		PDDocumentInformation info = null;
		PDDocument document = null;
		CafVo cafVo = null;
		File f = new File(tempFilePath);
		try {
			cafVo = new CafVo();
			document = PDDocument.load(f);
			info = document.getDocumentInformation();
			cafVo.setSpeedocId(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.SPEEDOC_ID
							.toString())));
			cafVo.setCafNumber(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.CAF_NUMBER
							.toString())));
			cafVo.setUnknownNumber(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.UNKNOWN_NUMBER
							.toString())));
			cafVo.setMobileNumber(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.MOBILE_NUMBER
							.toString())));
			cafVo.setSimNumber(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.SIM_NUMBER
							.toString())));
			// if (CafAttributeEnum.CREATED_BY.toString() != null) {
			cafVo.setCreatedBy(Long.parseLong(info
					.getCustomMetadataValue(CafAttributeEnum.CREATED_BY
							.toString())));
			cafVo.setModifiedBy(Long.parseLong(info
					.getCustomMetadataValue(CafAttributeEnum.CREATED_BY
							.toString())));
			// }
			cafVo.setPages(document.getPageCount());
			cafVo.setFileId(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.FILE_ID.toString())));
			cafVo.setReport(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.CAF_REPORT
							.toString())));
			cafVo.setConnectionType(StringUtils.trimToNull(info
					.getCustomMetadataValue(CafAttributeEnum.CAF_CONNECTION_TYPE
							.toString())));
			cafVo.setScannedDate(new Timestamp(info.getCreationDate().getTime()
					.getTime()));
			cafVo.setCreatedDate(new Timestamp(new Date().getTime()));

			document.close();

		} catch (Exception e) {
			log.error("Error in getCafVoFromPdf :: " + e.getMessage(), e);
			cafVo = null;
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return cafVo;
	}

	public static void deleteCAFFile(String filePath) {

		log.debug("File to delete :: " + filePath);
		File file = new File(filePath);
		FileUtils.deleteQuietly(file);

	}

	public static void moveFile(String srcFilePath, String destFilePath)
			throws Exception {
		File srcFile = new File(srcFilePath);
		File destFile = new File(destFilePath);
		// FileUtils.moveFile(srcFile, destFile);
		FileUtils.copyFile(srcFile, destFile);
		log.info("moving file from :: " + srcFilePath + "    " + destFilePath);
	}

	public static void moveFile1(String srcFilePath, String destFilePath)
			throws Exception {
		File srcFile = new File(srcFilePath);
		File destFile = new File(destFilePath);
		FileUtils.moveFile(srcFile, destFile);
		log.info("moving file from :: " + srcFilePath + "    " + destFilePath);
	}

	public static void createFoldersIfNotExists(String dateNowFolder,
			String cafDataPath) {
		File cafDateFolder = new File(cafDataPath + File.separator
				+ dateNowFolder);
		if (!cafDateFolder.exists()) {
			cafDateFolder.mkdir();
			cafDateFolder.setExecutable(true);
		}
	}

	public static String createFileIfExistsTemp(String tempDatePath,
			String fileNameWithoutExtension) {
		log.info("createFileIfExistsTemp ::  new temp tempDatePath = "
				+ tempDatePath + "  ::  fileNameWithoutExtension :: "
				+ fileNameWithoutExtension);
		int flag = 0;
		String tempFileName = fileNameWithoutExtension;
		do {
			flag++;

			File filePath = new File(tempDatePath + File.separator
					+ tempFileName + ".pdf");

			if (filePath.exists()) {

				tempFileName = fileNameWithoutExtension + "-r" + flag;

			} else {
				flag = 0;
			}

		} while (flag != 0);
		tempFileName = tempFileName + ".pdf";
		log.info("createFileIfExistsTemp ::  new temp FileName = "
				+ tempFileName);
		return tempFileName;
	}

	public static String checkAndGetCafFileLocation(String cafDatePath,
			String dateName, String fileNameWithoutExtension) {
		log.info(" check And GetCafFileLocation  cafDatePath  = " + cafDatePath
				+ "  ::  fileNameWithoutExtension :: "
				+ fileNameWithoutExtension);
		int flag = 0;
		String tempFileName = fileNameWithoutExtension;
		do {
			flag++;

			File filePath = new File(cafDatePath + File.separator + dateName
					+ File.separator + tempFileName + ".pdf");

			if (filePath.exists()) {

				tempFileName = fileNameWithoutExtension + "-r" + flag;

			} else {
				flag = 0;
			}

		} while (flag != 0);
		tempFileName = tempFileName + ".pdf";
		log.info(" new caf FileName = " + tempFileName);
		return tempFileName;
	}

}
