package com.vista.test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class ImageFileWrite {
	public static void main(String args[]){
		String string="D:\\backup"+File.separator+"image.jpg";
		BufferedImage image = null;
        try {
 
            URL url = new URL("http://www.mkyong.com/image/mypic.jpg");
            image = ImageIO.read(url);
 
            ImageIO.write(image, "jpg",new File("C:\\out.jpg"));
            ImageIO.write(image, "gif",new File("C:\\out.gif"));
            ImageIO.write(image, "png",new File("C:\\out.png"));
 
        } catch (IOException e) {
        	e.printStackTrace();
        }
        System.out.println("Done");
	}
}
